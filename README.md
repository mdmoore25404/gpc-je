# gpc-je a super simple platofrm agnostic parallel distributed job queue over HTTP

## HTTP API

The HTTP API is implemented in Python Flask and is extremely simple to use.

The default storage backend is MongoDB however that is not a requirement. You may create a child class of `GpcJobEngineBase` and use that instead of `GpcJobEngineMongo`

Below are examples using CURL to perform typical job interactions. Output is JSON format. User POST/PUT uses form data.

### Add a job

<table>
    <tr><th>Parameter</th><th>Type</th><th>Default</th><th>Possible Values</tr>
    <tr><td>payload</td><td>string</td><td>REQUIRED</td><td>Any arbitrary string value you want. 
    <p>JSON is recommended</p>
    </td></tr>
    <tr><td>friendlyName</td><td>string</td><td>'' (optional)</td><td>Optional Name to make it easier to search for results.</td></tr>
    <tr><td>jobFamily</td><td>string</td><td>'' (optional)</td><td>Optional job family to categorize/group</td></tr>
    <tr><td>priority</td><td>int</td><td>1 (optional)</td><td>optional job priority. higher values worked on first</td></tr>
    <tr><td>parentId</td><td>string</td><td>'' (optional)</td><td>optional parent Job ID. If you're using a parent-children setup child jobs should specify the parent job's _id here.</td></tr>
</table>

Success result code is `201`

    michaelmoore@nuc1:~$  curl -X POST http://nuc1:5000/jobs -F payload=foobar -F friendlyName=barfoo -F priority=2
    {"_id": "7bf6aaf0-c372-49b0-94ed-3e94791237d7"}

The ID of the newly added job is returned. This ID is a python uuid.uuid4()

### Add multiple jobs

You can also upload a file (.txt or .json) with an array of JSON objects that will be added.

Example JSON file (`jobs.json`):

    [
        {
            "payload": "testjob1",
            "friendlyName": "Test Job 1",
            "jobFamily": "test"
        },
        {
            "payload": "testjob2",
            "friendlyName": "Test Job 2",
            "jobFamily": "test"
        },
        {
            "payload": "testjob3",
            "friendlyName": "Test Job 3",
            "jobFamily": "test"
        },
        {
            "payload": "testjob4",
            "friendlyName": "Test Job 4",
            "jobFamily": "test"
        },
        {
            "payload": "testjob5",
            "friendlyName": "Test Job 5",
            "jobFamily": "test",
            "priority" : 2
        }
    ]

Submitting `jobs.json`:

    GSSLA17050714:~ mdmoore2$ curl -X POST http://localhost/jobs -F file=@jobs.json
    ["adeb8e5b-f995-4af3-b0a4-1fae7089a11a", "363edbe5-8cc4-4fdd-99d3-906cde7bfd18", "e4dc50b6-ca14-455d-9de4-0514c73e4730", "9ff878cd-24d0-4519-8292-cbf398ccf6b3", "5b053951-15ea-4efe-9398-50f8efd4f5cb"]

Successful result code is `201`.

A list of IDs is returned.

Searching for the added "Test Job 4"

    GSSLA17050714:~ mdmoore2$ curl "http:/localhost/jobs?name=Test%20Job%204"
    [{"_id": "bd7edb15-63e9-449d-80f0-148227ad251c", "createdOn": "2019-06-27 20:22:33.649938", "startTime": null, "endTime": null, "payload": "testjob4", "friendlyName": "Test Job 4", "workerid": "", "result": "", "runTimeSec": null, "status": "pending", "progress": 0.0, "jobFamily": "test", "priority": 2}]
    GSSLA17050714:~ mdmoore2$

### Query jobs

<table>
    <tr><th>Parameter</th><th>Type</th><th>Default</th><th>Possible Values</tr>
    <tr><td>_id</td><td>string</td><td>''</td><td>job _id</td></tr>
    <tr><td>limit</td><td>int</td><td>1000</td><td>all positive integers</td></tr>
    <tr><td>name</td><td>string</td><td>''</td><td>Jobs with this friendlyName</td></tr>
    <tr><td>status</td><td>string</td><td>''</td><td> 
        <ul> 
            <li>not provided (all jobs)</li>
            <li>pending</li>
            <li>running</li>
            <li>completed</li>
            <li>failed</li>
        </ul>
    </td></tr>
    <tr><td>family</td><td>string</td><td>''</td><td>Your job family. To query multiple families at once submit multiple family=x parameters in GET querystring.</td></tr>
    <tr><td>payload.owner</td><td>string</td><td>''</td><td>string matching the payload.owner field. To utilize this submit your payload with an owner key such as: <p>
    "owner": "UNIQUE_ID"</p></td></tr>
</table>

#### Examples

All jobs

Result code is `200` if no error. An empty JSON array `[]` is returned if no results match.

    michaelmoore@nuc1:~$ curl http://localhost:5000/jobs

    [{"endTime": null, "createdOn": "2019-05-08 03:20:33.845176", "priority": 1, "startTime": "2019-05-08 03:26:36.512563", "workerIdentity": "nuc1 25484", "_id": "bb46dd3b-1f1f-4f85-bb4b-3e9ecabb5f2a", "payload": "foobar"}, {"endTime": null, "createdOn": "2019-05-08 03:23:30.257745", "priority": 1, "startTime": "2019-05-08 03:26:38.377662", "workerIdentity": "nuc1 25484", "_id": "a052d493-06de-41c5-b5cf-cb4c4b3b700f", "payload": "foobar"}, {"endTime": null, "createdOn": "2019-05-08 03:23:30.842092", "priority": 1, "startTime": "2019-05-08 03:26:38.954795", "workerIdentity": "nuc1 25484", "_id": "860d6da7-fcb5-4a01-965b-33f9f6eda24c", "payload": "foobar"}, {"endTime": null, "createdOn": "2019-05-08 03:23:31.348290", "priority": 1, "startTime": "2019-05-08 03:26:39.518097", "workerIdentity": "nuc1 25484", "_id": "d1e8e4a1-c257-4fb5-80e1-089b8ef0babb", "payload": "foobar"}, {"endTime": null, "createdOn": "2019-05-08 03:23:31.828599", "priority": 1, "startTime": "2019-05-08 03:26:40.012711", "workerIdentity": "nuc1 25484", "_id": "1c490c7e-238c-42cb-b301-b44d11cd0813", "payload": "foobar"}, {"endTime": null, "createdOn": "2019-05-08 03:29:56.855853", "priority": 1, "startTime": null, "_id": "0842355f-a4ac-4bcf-82e8-095a81710187", "payload": "foobar"}]

Limit of 2

    michaelmoore@nuc1:~$ curl http://localhost:5000/jobs?limit=2
    [{"endTime": null, "createdOn": "2019-05-08 03:20:33.845176", "priority": 1, "startTime": "2019-05-08 03:26:36.512563", "workerIdentity": "nuc1 25484", "_id": "bb46dd3b-1f1f-4f85-bb4b-3e9ecabb5f2a", "payload": "foobar"}, {"endTime": null, "createdOn": "2019-05-08 03:23:30.257745", "priority": 1, "startTime": "2019-05-08 03:26:38.377662", "workerIdentity": "nuc1 25484", "_id": "a052d493-06de-41c5-b5cf-cb4c4b3b700f", "payload": "foobar"}]

Running jobs

    michaelmoore@nuc1:~$ curl 'http://localhost:5000/jobs?status=running'
    [{"endTime": null, "createdOn": "2019-05-08 03:20:33.845176", "priority": 1, "startTime": "2019-05-08 03:26:36.512563", "workerIdentity": "nuc1 25484", "_id": "bb46dd3b-1f1f-4f85-bb4b-3e9ecabb5f2a", "payload": "foobar"}, {"endTime": null, "createdOn": "2019-05-08 03:23:30.257745", "priority": 1, "startTime": "2019-05-08 03:26:38.377662", "workerIdentity": "nuc1 25484", "_id": "a052d493-06de-41c5-b5cf-cb4c4b3b700f", "payload": "foobar"}]

Filter by job family (optional field)

    curl "http://localhost:30000/jobs?family=primes&limit=2"
    [{"_id": "73c590ad-96be-4c6a-9beb-e07edb0b2338", "createdOn": "2019-05-27 03:41:33.943732", "startTime": null, "endTime": null, "payload": "{\"n\": 1}", "friendlyName": "IsPrime 1", "workerid": "", "result": "", "runTimeSec": null, "status": "pending", "progress": 0.0, "jobFamily": "primes"}, {"_id": "a3058503-7897-4b1a-9090-178cebb52316", "createdOn": "2019-05-27 03:41:33.949820", "startTime": null, "endTime": null, "payload": "{\"n\": 2}", "friendlyName": "IsPrime 2", "workerid": "", "result": "", "runTimeSec": null, "status": "pending", "progress": 0.0, "jobFamily": "primes"}]

Get children of a particular job

    curl localhost:8008/jobs?parentId=54a2d4b9-4197-4cfb-ad3d-81395b6da55d
    [{"_id": "b3d346db-f1f0-4f5b-9c14-e1f6529450b1", "createdOn": "2021-01-28 03:46:34.318086", "startTime": "2021-01-28 03:46:34.586156", "endTime": null, "payload": {"jobType": "child", ..... } } ]

### Check out a job to work on (atomically)

Success result code is `200`.
If there are no pending jobs the result code is `404`

    michaelmoore@nuc1:~$ curl http://localhost:5000/jobs/checkout_one
    {"endTime": null, "createdOn": "2019-05-08 03:29:56.855853", "priority": 1, "startTime": "2019-05-08 04:11:08.403573", "workerIdentity": "nuc1 7397", "_id": "0842355f-a4ac-4bcf-82e8-095a81710187", "payload": "foobar"}

The job is marked as in progress and will not be given to any other parallel workers

You can also specify a `workerid` string to uniquely identify the worker that claimed the job.

You can also provide a `family=<jobFamily>` string if you want only certain families, otherwise the default ('') is used.

### Report result of job

Success result code is `201`.
Specifying `family` is not required because this is by unique ID.

    michaelmoore@nuc1:~$ curl -X PUT http://nuc1:5000/jobs/9dcce517-bf33-4801-9d23-3aea3e889761 -F result=barfoo
    {}

### Report that job failed

Success result code is `201`.
Specifying `family` is not required because this is by unique ID

    michaelmoore@nuc1:~$ curl -X PUT http://nuc1:5000/jobs/9dcce517-bf33-4801-9d23-3aea3e889761 -F error=whoops
    {}

### Return a job

Success result code is `201`.
The use for this feature is in the event that your worker client is unable to process the request, but it's not a fault of the job's definition or payload.  For example your worker client has a dependency that is not available so you want to put the job back in the queue.

    michaelmoore@nuc1:~$ curl -X PUT http://nuc1:5000/jobs/return/9dcce517-bf33-4801-9d23-3aea3e889761
    {}

### Optionally provide a job progress update

You may wish to do this for particularly long running jobs where you can calculate `progress`. The format for `progress` is a decimal. e.g. 1.0 for 100%, 0.5 for 50%. Specifying `family` is not required because this is by unique id

    michaelmoore@nuc1:~$ curl -X PUT http://nuc1:5000/jobs/9dcce517-bf33-4801-9d23-3aea3e889761 -F progress=0.5
    {}

### Optionally add a history/log event

    michaelmoore@nuc1:~$ curl localhost:8008/jobs/ca16ad7a-f964-4fce-97ae-35eda13f1bdb -F log="example log message" -X PUT
    {}
    michaelmoore@nuc1:~$ curl localhost:8008/jobs/ca16ad7a-f964-4fce-97ae-35eda13f1bdb
    {"_id": "ca16ad7a-f964-4fce-97ae-35eda13f1bdb", "createdOn": "2020-06-24 21:56:50.398714", "startTime": null, "endTime": null, "payload": {"n": 1}, "friendlyName": "IsPrime 1", "workerid": "", "result": "", "runTimeSec": null, "status": "pending", "progress": 0.0, "history": [{"timestamp": "2020-06-24 21:57:24.584956", "message": "example log message"}], "jobFamily": "primes", "priority": 1}

The job's `history` field contains a JSON `array` of objects. Each object has a property for `timestamp` which is automatically generated and stored in UTC timezone. The `message` field is the user provided log event text. Sort order should be in chronological order but is not guarantted. UIs that display `history` should ensure sort order.

### delete a job

Success result code is `204` specifying `family` is not required because this is by unique id

    michaelmoore@nuc1:~$ curl -X DELETE http://nuc1:5000/jobs/a052d493-06de-41c5-b5cf-cb4c4b3b700f

#### delete all jobs

Success result code is `204`

    michaelmoore@nuc1:~$ curl -X DELETE http://nuc1:5000/jobs

#### delete all jobs in a family

Success result code is `204`

    michaelmoore@nuc1:~$ curl -X DELETE http://nuc1:5000/jobs?family=test

#### Get the count of jobs by status

    michaelmoore@nuc1:~/projects/gpc-je-python-client/examples$ curl http://nuc1:30000/jobs/count
    {"running": 0, "pending": 0, "completed": 100, "total": 100}

You can also use `?family=<jobFamily>` to restrict the count. By default returns for family=''

#### Update Job Priority

Use the `priority` property to define priority of a job. `priority` is an integer, higher values are returned first from the `checkout_job` call.

    $ curl localhost:8008/jobs/240e6429-e64b-4b14-9899-c5b1e4935529
    {"_id": "240e6429-e64b-4b14-9899-c5b1e4935529", "createdOn": "2020-05-24 01:55:08.644211", "startTime": null, "endTime": null, "payload": "foo", "friendlyName": "", "workerid": "", "result": "", "runTimeSec": null, "status": "pending", "progress": 0.0, "jobFamily": "", "priority": 1}
    $ curl localhost:8008/jobs/240e6429-e64b-4b14-9899-c5b1e4935529 -F priority=4 -X PUT
    {}
    $ curl localhost:8008/jobs/240e6429-e64b-4b14-9899-c5b1e4935529
    {"_id": "240e6429-e64b-4b14-9899-c5b1e4935529", "createdOn": "2020-05-24 01:55:08.644211", "startTime": null, "endTime": null, "payload": "foo", "friendlyName": "", "workerid": "", "result": "", "runTimeSec": null, "status": "pending", "progress": 0.0, "jobFamily": "", "priority": 4}

## Anatomy of a JobEngine Job record (JSON)

    {
    "_id": "a6dea40e-7b2a-426c-b575-539a2c8fab9e", // unique id python uuid4
    "createdOn": "2019-05-17 04:58:41.032866", // timestamp of job creation
    "startTime": "2019-05-17 04:58:41.513451", // timestamp of job checkout
    "endTime": "2019-05-17 04:58:43.524945", // timestamp of job completionm
    "payload": {"n": 1}, // parameters provided when creating job
    "friendlyName": "IsPrime 1", // optional friendly name provided when creating the job
    "workerid": "2", // optional worker identification when checking out a job
    "result": {"isPrime": false}", // result of job provided when completing job
    "runTimeSec": 2.011494, //total runtime in second (endTime - startTime), calculated when job is marked completed
    "status": "completed", //easy filter for job status. pending|running|completed, updated at creation, checkout, completion,
    "jobFamily" : "primes", //optional string to group jobs. if not provided default is empty string ""
    "priority": 1, //priority. higher values get worked first
    "history" : [], //optional history. 
    "progress" : 0.5 // optionally provided job progress estimate. format is decimal, 
    }

## Job Families API

Job Families are a way to group jobs that should be handled by a particular type of client.

They are configured via the /jobs/families API endpoint

### List job families

    MichaelsMBP1483:~ michaelmoore$ curl localhost:8008/jobs/families
    [{"_id": "foo", "pause": true}, {"_id": "primes", "pause": false}]

### Set a job family property

    MichaelsMBP1483:~ michaelmoore$ curl localhost:8008/jobs/families/foo -F pause=false -X PUT
    {}

### View a specific family

    MichaelsMBP1483:~ michaelmoore$ curl localhost:8008/jobs/families/foo
    {"_id": "foo", "pause": false}

### Pausing a job family

Set the "pause" property to "true" - compliant clients will wait and try again later.

## Job Workers

The API keeps track of job workers (clients) when they provide `workerid`. This allows checking the /jobs/workers API endpoint to detect if a worker has gone dark or otherwise misbehaves.

The API sets the `updatedTime` property and `status` when a worker:

* Attempts to checkout a job (checkout)
* Marks a job completed (job complete)
* Marks a job failed (job failed)
* Provides a job progress (progress X.X)
* TODO: when worker gracefully shuts down (termianted)

### View job workers for a family

To see all omit the `family` parameter.

    MichaelsMBP1483:~ michaelmoore$ curl localhost:8008/jobs/workers?family=primes
    [{"_id": "91e0d333-50b7-40e9-bfce-d1cc3151a7d6", "jobFamily": "primes", "name": "ubu18-0", "status": "checkout", "updatedTime": "2019-07-11 00:43:06.003974"}, {"_id": "e1d35773-5649-47fd-a555-a25579103a55", "jobFamily": "primes", "name": "ubu18-1", "status": "checkout", "updatedTime": "2019-07-11 00:43:05.976874"}, {"_id": "550f003d-18f0-4c0f-bcb6-16eb5a65bde0", "jobFamily": "primes", "name": "ubu18-2", "status": "checkout", "updatedTime": "2019-07-11 00:43:06.018767"}, {"_id": "360274fa-f185-494e-9ae6-77bbe3fe2f77", "jobFamily": "primes", "name": "ubu18-4", "status": "checkout", "updatedTime": "2019-07-11 00:43:05.989679"}, {"_id": "b10aead0-3f66-408e-a263-9dd86e77edef", "jobFamily": "primes", "name": "ubu18-3", "status": "checkout", "updatedTime": "2019-07-11 00:43:06.009915"}]

### Multi-Step jobs

If you have a scenario where you depend on the output of a previous job you can simply submit another job with the payload containing the inputs needed.

A functional example implementing the Fibonacchi sequence is located in [fib.py](clients/gpc-je-python/examples/fib/fib.py)

![Fibonacchi Example in Web UI](clients/gpc-je-python/examples/fib/fib-example.png)
