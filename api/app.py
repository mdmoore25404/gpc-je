#!/usr/bin/python3

from flask import Flask, g
from flask_restful import Resource, Api, reqparse, request
import jobengine_mongo
import os
import json
import flask_compress
import db_bootstrap

## Read environment for any changes to default options
_GPC_MONGO_HOST = os.getenv('GPC_MONGO_HOST', 'localhost') # read mongo host from environment
_GPC_MONGO_PORT = int(os.getenv( 'GPC_MONGO_PORT', 27017))
_GPC_MONGO_DB = os.getenv('GPC_MONGO_DB', 'jobs')
_GPC_MONGO_COLL = os.getenv('GPC_MONGO_COLL', 'queue')

parser = reqparse.RequestParser()
parser.add_argument('result', type=dict)

app = Flask(__name__)
api = Api(app)

je = jobengine_mongo.GpcJobEngineMongo(mongoHost= _GPC_MONGO_HOST,
                                    mongoPort = _GPC_MONGO_PORT,
                                    mongoDbName=_GPC_MONGO_DB,
                                    mongoDbCollection=_GPC_MONGO_COLL)


COMPRESS_MIMETYPES=['text/html', 'application/json'] # compress html and json
COMPRESS_LEVEL = 6
COMPRESS_MIN_SIZE = 500
flask_compress.Compress(app)
ALLOWED_EXTENSIONS = set(['txt', 'json'])


# https://dev.to/svencowart/multi-value-query-parameters-with-flask-3a92
def normalize_query_param(value):
    """
    Given a non-flattened query parameter value,
    and if the value is a list only containing 1 item,
    then the value is flattened.

    :param value: a value from a query parameter
    :return: a normalized query parameter value
    """
    return value if len(value) > 1 else value[0]

# https://dev.to/svencowart/multi-value-query-parameters-with-flask-3a92
def normalize_query(params):
    """
    Converts query parameters from only containing one value for each parameter,
    to include parameters with multiple values as lists.

    :param params: a flask query parameters data structure
    :return: a dict of normalized query parameters
    """
    params_non_flat = params.to_dict(flat=False)
    return {k: normalize_query_param(v) for k, v in params_non_flat.items()}



def is_json(myjson):
  try:
    json_object = json.loads(myjson)
  except ValueError as e:
    return False
  return True

def addJobsBeforeFieldOneObject(j): 
    if j == None:
        return j
           
    if j['status'] == "pending":
        j['jobsBefore'] = int(je.countJobsBefore(j['_id']))
    else:
        j['jobsBefore'] = 0
    return j

def addJobsBeforeField(jobsList):        
    for j in list(jobsList):
        j = addJobsBeforeFieldOneObject(j)
    return jobsList

def jsonOrString(s):
    if is_json(s):
        return json.loads(s)
    else:
        return s

def NoneToEmptyString(s):
    if s == None:
        return ""
    else:
        return s

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

class JobCount(Resource):
    def get(self):        
        #je = get_je()
        r = {}
        filters = {}
        args = normalize_query(request.args)
        familyList = args['family']
        #convert to single-index array
        if type(familyList) is str:
            familyList = [familyList]

        familyList = NoneToEmptyString(familyList)

        parentId = request.args.get("parentId")
        parentId = NoneToEmptyString(parentId)

        if familyList != "":
            filters['family'] = familyList

        if parentId != "":
            filters["parentId"] = parentId

        

        r['total'] = je.countJobs(filters)
        statuses = ['running', 'pending', 'completed', 'failed']
        for status in statuses:
            filters['status'] = status
            r[status] = je.countJobs(filters)

        return r


class JobList(Resource):   
    def get(self):        
        #je = get_je()

        
        status = request.args.get('status')        
        args = normalize_query(request.args)
        familyList = ""

        if 'family' in args:
            familyList = args['family']
        #convert to single-index array
        if type(familyList) is str:
            familyList = [familyList]
        
        limit = request.args.get('limit')
        name = request.args.get('name')
        parentId = request.args.get("parentId")
        _id = request.args.get('id')

        payloadOwner = request.args.get("payload.owner")

        status = NoneToEmptyString(status)        
        limit = NoneToEmptyString(limit)
        name = NoneToEmptyString(name)
        parentId = NoneToEmptyString(parentId)
        payloadOwner = NoneToEmptyString(payloadOwner)

        print(f"Payload owner search: '{payloadOwner}'")

        _id = NoneToEmptyString(_id)
        filters = {}
        if familyList != "":
            filters['family'] = familyList
        if status != "":
            filters['status'] = status
        if name != "":
            filters['name'] = name
        if parentId != "":
            filters['parentId'] = parentId
        
        if payloadOwner != "":
            filters['payload.owner'] = payloadOwner
            
        if _id != "":
            filters['_id'] = _id

        print(f"Limit is {limit}")

        if limit == "":
            limit = 1000
        else:
            limit = int(limit) # convert string to int

        jobsList = list( je.getJobs(filters, limit=limit))

        return addJobsBeforeField(jobsList)

    
    def post(self):           
       # je = get_je()     

        if "file" not in request.files:
            payload = request.form.get('payload')
            friendlyName = request.form.get('friendlyName')
            jobFamily = request.form.get('jobFamily')
            friendlyName = NoneToEmptyString(friendlyName)
            jobFamily = NoneToEmptyString(jobFamily)        
            priority = request.form.get('priority')

            parentId = request.form.get("parentId")
            parentId = NoneToEmptyString(parentId)
            
            if priority == None:
                priority = 1

            if payload == None:
                return "You must specify the payload parameter",400

            payload = jsonOrString(payload)

            result = {}
            result['_id'] = je.addJob(payload, friendlyName, jobFamily, priority=priority, parentId=parentId).inserted_id
            return result,201 
        else:
            # handling a file upload
            print("File upload")
            file = request.files['file']
            if file.filename == '':
                return "no selected file",400

            if file and allowed_file(file.filename):
                s = file.read()
                print(f"Got file: {s}")
                jobs = json.loads(s)                
                ids = []
                
                # iterate and check validity
                for job in jobs:                                        
                    if "payload" not in job:
                        return "Payload is a required field for a job", 400

                    if "friendlyName" not in job:                                                                
                        job["friendlyName"] = ""
                    if "jobFamily" not in job:                                            
                        job["jobFamily"] = ""
                    if "priority" not in job:
                        job["priority"] = 1
                    
                    if "parentId" not in job:
                        job["parentId"] = ""

                # didn't kick out an error so lets save
                for job in jobs:
                    id = je.addJob(job['payload'], job['friendlyName'], job['jobFamily'], priority=job['priority'], parentId=job['parentId']).inserted_id
                    ids.append(id)                    

                return ids,201
            else:
                return "Invalid file extension",400
                

    def delete(self):
       # je = get_je()
        print("in delete")        
        status = request.args.get('status')
        args = normalize_query(request.args)
        familyList = ""
        if 'family' in args:
            familyList = args['family']
        #convert to single-index array
        if type(familyList) is str:
            familyList = [familyList]

        status = NoneToEmptyString(status)
        familyList = NoneToEmptyString(familyList)
        name = request.args.get("name")
        name = NoneToEmptyString(name)

        payloadOwner = request.args.get("payload.owner")
        payloadOwner = NoneToEmptyString(payloadOwner)

        parentId = request.args.get("parentId")
        parentId = NoneToEmptyString(parentId)

        
        filters = {}

        if name != "":
            filters['name'] = name

        if status != "":
            filters['status'] = status
        
        if familyList != "":
            filters['family'] = familyList

        if parentId != "":
            filters['parentId'] = parentId
        
        if payloadOwner != "":
            filters['payload.owner'] = payloadOwner

        print(f"jobs delete with filters {filters}")
        je.deleteJobs(filters)
        return {},204
        

class Job(Resource):
    def get(self, _id):
       # je = get_je()
        j = je.getJobById(_id)
        j = addJobsBeforeFieldOneObject(j)
        if j == None:
            return {},404
        else:
            return j
    
    def put(self, _id):
      #  je = get_je()
        result = request.form.get('result')
        error = request.form.get('error')
        progress = request.form.get("progress")
        priority = request.form.get("priority")
        log = request.form.get("log")
        
        r = None

        if error != None:
            error = jsonOrString(error)
            r = je.failJob(_id, error)
        elif progress != None:            
            r = je.progressJob(_id, progress)
        elif result != None:
            result = jsonOrString(result)
            r = je.completeJob(_id, result)        
        elif priority != None:
            try:
                priority = int(priority)
            except ValueError:
                return {"error" : f"Priority value {priority} must be int or convert to int"}                        
            r = je.rePrioritizeJob(_id, priority)            
        elif log != None:
            r = je.addLog(_id, log)
        else:
            return { "error" : "must provide result, error, progress, or priority" },400

        # print(f"Modified count {r.modified_count}")
        # print(f"matched count {r.matched_count}" )

        if r == False:
            return {},404
        elif r.matched_count == 1: # if the update doesn't change anything
            return {},201
        else:
            return {},404 # 404 if job id isn't found
    
    def delete(self, _id):
      #  je = get_je()  
       
        je.deleteJob(_id)
        return {},204


class JobCheckout(Resource):
    def get(self):        
       # je = get_je()
        workerid = request.args.get('workerid')
        workerid = NoneToEmptyString(workerid)
        family = request.args.get('family')
        family = NoneToEmptyString(family)

        f = je.getJobFamily(family)
        if f != None and "pause" in f and str(f["pause"]).lower() == "true":
            return "PAUSED",204

        j = je.checkoutJob(workerid, family=family)
        
        if j == None:
            return {},204 #return no content
        else:            
            return j

class JobFamilies(Resource):
    def get(self):        
       # je = get_je()        
        j = list(je.getJobFamilies())
        
        if j == None:
            return {},204 #return no content
        else:
            return j
        
    def delete(self):
        family = request.args.get('family')    
        print(f"JobFamilies.delete family={family}")
        family = NoneToEmptyString(family)
        filters = {}
        if family != "":
            filters['family'] = family

        je.deleteJobFamilies(filters)

        return {},204

class JobFamily(Resource):
    def get(self, name):        
       # je = get_je()        
        j = je.getJobFamily(name)
        
        if j == None:
            return {},204 #return no content
        else:
            return j
    
    def put(self, name):
        f = je.getJobFamily(name)

        if f == None:
            return f"Family named {name} not defined",404
        
        if "name" in request.form.keys():
            return "Cannot specify name in this context", 400
        
        for fieldname in request.form.keys():
            value = jsonOrString( request.form.get(fieldname) )
            je.updateJobFamily(name, fieldname, value)
    
        return {},201

    def delete(self, name):
        rJobs = je.deleteJobs(filters= {"family" : name})
        r = je.deleteJobFamilies(filters = {"_id" : name})

        if r.deleted_count > 0:
            return {},204
        else:
            return {},404

class JobFamilyStats(Resource):
    def get(self, name):
        f = je.getJobFamily(name)

        if f == None:
            return f"Family named {name} not defined",404
        
        stats = je.getFamilyStats(name)

        return stats,200
    
class Workers(Resource):
    def get(self):        
       # je = get_je()       
        
        args = normalize_query(request.args)
        familyList = ""

        if 'family' in args:
            familyList = args['family']
        #convert to single-index array
        if type(familyList) is str:
            familyList = [familyList]
        

        filters = {}
        if familyList != "":
            filters['family'] = familyList
        
        j = list(je.getWorkers(filters))
        
        if j == None:
            return {},204 #return no content
        else:
            return j   

    def delete(self):
       # je = get_je()
        print("in delete")        
        family = request.args.get('family')    
        family = NoneToEmptyString(family)
        filters = {}
        if family != "":
            filters['family'] = family

        je.deleteWorkers(filters)
        return {},204

class JobsResubmit(Resource):
    def post(self):
        family = request.args.get('family')    
        family = NoneToEmptyString(family)
        if family == "":
            return "family is a required parameter to this request",400
        
        status = request.args.get('status')    
        status = NoneToEmptyString(status)
        if status == "":
            return "status is a required parameter to this request",400

        if status != "completed" and status != "failed":
            return "in this context status may only be completed or failed",400

        failedJobs = je.getJobs(
            filters = { "family" : family, "status" : status}
        )

        for fj in failedJobs:
            je.addJob(fj["payload"], fj["friendlyName"], fj["jobFamily"], fj["priority"], fj["parentId"])

        return {},201

class Worker(Resource):
    def get(self,_id):
        worker = je.getWorker(_id)
        return worker    

    def delete(self, _id):
        je.deleteWorker(_id)
        return {},204

class JobReturn(Resource):
    def put(self, _id):
        j = je.returnJob(_id)
        return {},201

api.add_resource(Job, '/jobs/<string:_id>')
api.add_resource(JobCount, '/jobs/count')
api.add_resource(JobList, '/jobs')
api.add_resource(JobCheckout, '/jobs/checkout_one')
api.add_resource(JobFamilies, '/jobs/families')
api.add_resource(JobFamily, '/jobs/families/<string:name>')
api.add_resource(JobFamilyStats, '/jobs/families/<string:name>/stats')
api.add_resource(Workers, '/jobs/workers')
api.add_resource(Worker, '/jobs/workers/<string:_id>')
api.add_resource(JobsResubmit, '/jobs/resubmit')
api.add_resource(JobReturn, '/jobs/return/<string:_id>')


if __name__ == '__main__':    
    db_bootstrap.db_bootstrap()
    app.run(host='0.0.0.0', port=8009)
