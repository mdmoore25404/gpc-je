#!/usr/bin/env python3

import os
import pymongo

## Read environment for any changes to default options
_GPC_MONGO_HOST = os.getenv('GPC_MONGO_HOST', 'localhost') # read mongo host from environment
_GPC_MONGO_PORT = int(os.getenv( 'GPC_MONGO_PORT', 27017))
_GPC_MONGO_DB = os.getenv('GPC_MONGO_DB', 'jobs')
_GPC_MONGO_COLL = os.getenv('GPC_MONGO_COLL', 'queue')

def indexExists(index_name, collection):
    return index_name in collection.index_information()

def db_bootstrap():        

    print("attempting to connect to mongodb at " + _GPC_MONGO_HOST)
    try:
        mongoConnection = pymongo.MongoClient(_GPC_MONGO_HOST, _GPC_MONGO_PORT, serverSelectionTimeoutMS=2000, minPoolSize=10, maxPoolSize=50, connect=False)                                                           
    except Exception as err:
        print(err)
        exit(1)      

    db = mongoConnection[_GPC_MONGO_DB]
    queue = db[_GPC_MONGO_COLL]   
    families = db[f"{_GPC_MONGO_COLL}_families"] # family  collection 
    workers = db[f"{_GPC_MONGO_COLL}_workers"] # workers  collection 

    #create indexes to improve performance            
    if not indexExists("startEndTime", queue):
        queue.create_index( [ ("startTime", pymongo.DESCENDING), ("endTime", pymongo.DESCENDING) ], name="startEndTime" )        
    if not indexExists("createdOn", queue):
        queue.create_index( [ ("createdOn", pymongo.DESCENDING ) ], name="createdOn" )
    if not indexExists("status", queue):            
        queue.create_index( [ ("status", pymongo.ASCENDING ) ], name="status" )
    if not indexExists("jobFamily", queue):
        queue.create_index( [ ("jobFamily", pymongo.ASCENDING ) ], name="jobFamily" )
    if not indexExists("friendlyName", queue):
        queue.create_index([("friendlyName", pymongo.ASCENDING)], name="friendlyName")
    if not indexExists("priority", queue):
        queue.create_index([("priority", pymongo.ASCENDING)], name="priority")

    if not indexExists("priorityCreatedOn", queue):
        queue.create_index( [ ("priority", pymongo.DESCENDING), ("createdOn", pymongo.ASCENDING) ], name="priorityCreatedOn")

    if not indexExists("parentIdJobFamily", queue):
        queue.create_index( [ ("parentId", pymongo.ASCENDING), ("jobFamily", pymongo.ASCENDING) ], name="parentIdJobFamily")

    if not indexExists("statusJobFammily", queue):
        queue.create_index( [("status", pymongo.ASCENDING), ("jobFamily", pymongo.ASCENDING) ], name="statusJobFamily")  

    #_worker indexes
    if not indexExists("nameFamily", workers):
        workers.create_index( [ ("name", pymongo.ASCENDING), ("jobFamily", pymongo.ASCENDING)], name="nameFamily", unique=True)
    
    if not indexExists("workers_jobFamily", workers):
        workers.create_index( [ ("jobFamily", pymongo.ASCENDING)] , name="workers_jobFamily")


    # payload.owner             
    if not indexExists("payloadOwner", queue):            
        print("creating payloadOwner index")
        queue.create_index( [ ("payload.owner", pymongo.ASCENDING ) ], name="payloadOwner" )

    #jobsAhead index
    if not indexExists("jobFamily_status_createdOn_priority", queue):
        print("creating index jobFamily_status_createdOn_priority")
        queue.create_index( 
            [ ( "jobFamily", pymongo.ASCENDING ), 
            ("status" , pymongo.ASCENDING), 
            ("createdOn", pymongo.ASCENDING), 
            ("priority" , pymongo.DESCENDING) ],
            name="jobFamily_status_createdOn_priority")


    # families indexes
    # 
    if not indexExists("family_id_pause", families):
        families.create_index( [  ("_id", pymongo.ASCENDING), ("pause", pymongo.ASCENDING) ], name="family_id_pause" )

if __name__ == '__main__':    
    db_bootstrap()