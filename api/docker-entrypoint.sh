#!/usr/bin/env bash

set -x

cd /app

python3 /app/db_bootstrap.py

uwsgi --ini /app/app.ini

