#### #!/usr/bin/python3

import threading
import time
import pymongo
import datetime
import os
import sys
import uuid
from abc import abstractmethod

class GpcJobEngineBase:
    """GPC Job Engine"""

    @abstractmethod
    def __init__(self):
        pass


    @abstractmethod
    def __del__(self):
        pass
        
        
    def log(self, s):
        """
        Log string
        :param s: string to log
        """
        sys.stdout.write("%s\n" % s)

    @abstractmethod
    def addJob(self, payload, friendlyName = '', jobFamily = '', priority=1):
        pass

    @abstractmethod
    def getJobById(self, _id):
        pass

    @abstractmethod
    def checkoutJob(self, workerid):
        pass
        
    @abstractmethod
    def getJobs(self, limit=1000, family=''):
        pass
        
    @abstractmethod
    def completeJob(self, _id, result):
        pass

    @abstractmethod
    def progressJob(self, _id, progress):
        pass
    
    @abstractmethod
    def getJobFamilies(self):
        pass

    @abstractmethod
    def failJob(self, _id, error):
        pass

    @abstractmethod
    def deleteJobs(self, filters = {}):
        pass

    @abstractmethod
    def deleteJob(self, _id):    
        pass


    def getJobFamily(self, name):
        pass

    def addJobFamily(self, name):
        pass

    def updateJobFamily(self, name, field, value):
        pass

    def updateWorker(self, name, family, status):
       pass

    def deleteJobFamilies(self, filters):
        pass

    def addWorker(self, name, family):
        pass

    def getWorkers(self, filters):
        pass

    def deleteWorkers(self, filters):
        pass

    def rePrioritizeJob(self, _id, priority):
        pass

    def returnJob(self, _id):
        pass

    def countJobsBefore(self, _id):
        pass

    pass