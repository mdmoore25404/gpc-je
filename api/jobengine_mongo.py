#### #!/usr/bin/python3

import threading
import time
import pymongo
import datetime
import os
import sys
import uuid
from jobengine import GpcJobEngineBase

class GpcJobEngineMongo(GpcJobEngineBase):
    """GPC Job Engine using MongoDB"""

    def __init__(self, mongoHost = "localhost", mongoPort = 27017, mongoDbName = "jobs", mongoDbCollection = "queue", **kwargs):
        """
        constructor
        :param mongoHost=localhost: 
        :param mongoPort=27017:
        :param mongoDbName=jobs: mongdb database name
        :param mongoDbCollection=queue: mongodb collection
        """        
        self.mongoHost = mongoHost
        self.mongoPort = mongoPort        
        self.pid = os.getpid()
        self.mongoDbName = mongoDbName
        self.mongoDbCollection = mongoDbCollection
        self.mongoConnection = None

        self.log(f"GpcJobEngineMongo settings mongoHost={self.mongoHost}, mongoPort={self.mongoPort}, monboDbName={self.mongoDbName}, mongoDbCollection={self.mongoDbCollection}")
        
        self.mongoIsConnected = False

        if(self.mongoConnection == None):
            print("attempting to connect to mongodb at " + self.mongoHost)
            try:
                self.mongoConnection = pymongo.MongoClient(self.mongoHost, self.mongoPort, serverSelectionTimeoutMS=2000, minPoolSize=10, maxPoolSize=50, connect=False)                                                           
            except Exception as err:
                print(err)
                exit(1)                
        else:
            print("Using cached connection")


    def __del__(self):
        self.mongoConnection.close()
        self.mongoIsConnected = False
       
    
    def connectToDb(self):        
        if not self.mongoIsConnected:         
            print("Connecting client")   
            self.db = self.mongoConnection[self.mongoDbName]
            self.queue = self.db[self.mongoDbCollection]   
            self.families = self.db[f"{self.mongoDbCollection}_families"] # family  collection 
            self.workers = self.db[f"{self.mongoDbCollection}_workers"] # workers  collection 
            self.mongoIsConnected = True

    
    def log(self, s):
        """
        Log string
        :param s: string to log
        """
        sys.stdout.write("%s\n" % s)

    def addJob(self, payload, friendlyName = '', jobFamily='', priority = 1, parentId = ""):
        """
        Add a job to queue
        
        :param payload: dict of job settings
        :param priority=1: int optional priority for job
        """        
        self.connectToDb()
        result = self.queue.insert_one(
        {             
            "_id" : str(uuid.uuid4()),
            "createdOn": str(datetime.datetime.utcnow()),
            "startTime" : None,
            "endTime": None,            
            "payload": payload,
            "friendlyName" : friendlyName,
            "workerid" : "",
            "result": "",
            "runTimeSec" : None,
            "status" : "pending",
            "lastLog" : "",
            "progress": 0.0,
            "history" : [],
            "jobFamily" : jobFamily,
            "parentId" : parentId,
            "priority" : int(priority) # ensure is int           
        })


        #add stub for job family if it doesn't exist
        if self.getJobFamily(jobFamily) == None:
            self.addJobFamily(jobFamily)

        return result

    def buildFilters(self, filters):        
        f = {}
        print(f"in buildFilters with filters={filters}")
        
        if 'family' in filters:
            if type(filters['family']) is list:
                ## if it's a 1 item array equal to [""] no value was passed so we don't filter at all
                if len(filters['family']) == 1 and filters['family'][0] == "":
                    pass
                else:
                    f['jobFamily'] = { "$in" : filters['family'] }
            else:
                if filters['family'] != "":
                    f['jobFamily'] = { "$in" : [ filters['family'] ] } #ensure a one index array is passed

        if 'name' in filters:
            f['friendlyName'] = filters['name']
        
        if 'status' in filters:
            f['status'] = filters['status']

        if '_id' in filters:
            f['_id'] = filters['_id']

        if 'parentId' in filters:
            f['parentId'] = filters['parentId']

        if 'payload.owner' in filters:
            f['payload.owner'] = filters['payload.owner']


        print(f"Built filter {f}")
        return f
   
    def getJobById(self, _id):
        self.connectToDb()
        return self.queue.find_one({ "_id" : _id})

    def checkoutJob(self, workerid, family = ''):
        self.connectToDb()
        if workerid != "":
            self.updateWorker(workerid, family, "checkout") #updateWorker will upsert if worker+family dont exist
        return self.queue.find_one_and_update({ "status" : "pending", "jobFamily" : family }, 
        { '$set' : { "status" : "running", "startTime" : str(datetime.datetime.utcnow()), "workerid" : str(workerid)} },
        return_document=pymongo.ReturnDocument.AFTER,
        sort = [('priority', pymongo.DESCENDING), ('createdOn', pymongo.ASCENDING)]
        )

    def getJobs(self, filters = {}, limit=None):
        self.connectToDb()
        if limit != None :
            return self.queue.find(self.buildFilters(filters)).limit(limit)
        else:
            return self.queue.find(self.buildFilters(filters))

    def countJobs(self, filters = {}, limit=None):
        self.connectToDb()                
        return self.queue.count_documents(self.buildFilters(filters))

    def returnJob(self, _id):
        self.connectToDb()
        self.addLog(_id, "Job returned by worker")
        return self.queue.update_one({ "_id" : _id } , {'$set' : { "status" : "pending", "workerid" : None, "startTime" : None  } } )

    def completeJob(self, _id, result):
        self.connectToDb()
        now = datetime.datetime.utcnow()
        j = self.getJobById(_id)
        if j != None:
            start = j['startTime']        
            #example format 2019-05-17 04:40:58.289111 
            startedTime = datetime.datetime.strptime(start, '%Y-%m-%d %H:%M:%S.%f')
            delta = now - startedTime
            runTimeSec = delta.total_seconds()
            self.log(runTimeSec)
            if j['jobFamily'] != "" and j['workerid'] != "":
                self.updateWorker(j['workerid'], j['jobFamily'], f"job complete")
            return self.queue.update_one({ "_id" : _id }, { '$set' : { "endTime" : str(now) , "result" : result, "runTimeSec" : runTimeSec, "status" : "completed", "progress" : 1.0 } } )
        else:
            return False

    def progressJob(self, _id, progress): 
        self.connectToDb()      
        j = self.getJobById(_id)
        if j != None and j['jobFamily'] != "" and j['workerid'] != "":
            self.updateWorker(j['workerid'], j['jobFamily'], f"progress {progress}")
        return self.queue.update_one({ "_id" : _id }, { '$set' : { "progress": float(progress) } } )

    def addLog(self, _id, log):
        self.connectToDb()
        now = str(datetime.datetime.utcnow())
        return self.queue.update_one({ "_id" : _id }, { '$set' : { "lastLog": log} , '$push' : { "history" : { "timestamp" : now, "message" : log } } } )

    def failJob(self, _id, error):
        self.connectToDb()
        now = datetime.datetime.utcnow()
        j = self.getJobById(_id)
        if j != None:
            start = j['startTime']        
            #example format 2019-05-17 04:40:58.289111 
            startedTime = datetime.datetime.strptime(start, '%Y-%m-%d %H:%M:%S.%f')
            delta = now - startedTime
            runTimeSec = delta.total_seconds()
            self.log(runTimeSec)        
            if j != None and j['jobFamily'] != "" and j['workerid'] != "":
                self.updateWorker(j['workerid'], j['jobFamily'], f"job failed")
            return self.queue.update_one({ "_id" : _id }, { '$set' : { "endTime" : str(now) , "error" : error, "runTimeSec" : runTimeSec, "status" : "failed" } } )
        else:
            return False

    def deleteJobs(self, filters):
        self.connectToDb()
        return self.queue.delete_many( self.buildFilters(filters) )

    def deleteJob(self, _id):
        self.connectToDb()
        return self.queue.delete_one({ "_id" : _id})

    def getJobFamilies(self):
        self.connectToDb()
        return self.families.find({}).sort("_id")
        #.sort(key = "jobFamily")

    def getJobFamily(self, name):
        self.connectToDb()
        #print(f"Looking for family named '{name}'")
        return self.families.find_one({ "_id" : name})

    def addJobFamily(self, name):
        self.connectToDb()
        return self.families.insert_one(
                { 
                    "_id" : name,
                    "pause" : False
                }
            )
    def updateJobFamily(self, name, field, value):
        self.connectToDb()
        return self.families.update_one(
            { "_id" : name},
            { "$set" : { field: value } }
        )

    def updateWorker(self, name, family, status):
        self.connectToDb()
        now = str(datetime.datetime.utcnow())
        if self.workers.find_one({ "name" : name, "jobFamily" : family}) == None:
            self.addWorker(name, family)

        return self.workers.update_one(
            { "name" : name, "jobFamily" : family},
            { "$set" : { "status": status, "updatedTime" : now } }        )

    def addWorker(self, name, family):
        self.connectToDb()
        now = str(datetime.datetime.utcnow())
        return self.workers.insert_one(
            { "_id" : str(uuid.uuid4()), 
            "jobFamily" : family, 
            "name" : name,
            "status": "created", 
            "updatedTime" : now } )
    
    def getWorkers(self, filters):
        self.connectToDb()
        return self.workers.find( self.buildFilters(filters))
    
    def getWorker(self, _id):
        self.connectToDb()
        return self.workers.find_one( { "_id" : _id})

    def deleteWorker(self, _id):
        self.connectToDb()
        return self.workers.find_one_and_delete({ "_id": _id})

    def deleteWorkers(self, filters):
        self.connectToDb()
        return self.workers.delete_many( self.buildFilters(filters) )

    def deleteJobFamilies(self, filters):
        self.connectToDb()
        return self.families.delete_many( self.buildFilters(filters))


    def getFamilyStats(self, family):
        self.connectToDb()

        totalspipeline = [
            { 
                "$match" : { "jobFamily" : family}
            },
            { 
                "$group" : 
                {
                "_id" : "$jobFamily", "avgRunTimeSec" : { "$avg" : "$runTimeSec"}, "totalRunTimeSec" : { "$sum" : "$runTimeSec"}
                }
            }
        ]

        workerspipeline = [
            { 
                "$match" : { "jobFamily" : family}
            },
            { 
                "$group" : 
                {
                "_id" : "$workerid", "avgRunTimeSec" : { "$avg" : "$runTimeSec"}, "totalRunTimeSec" : { "$sum" : "$runTimeSec"}
                }
            }
        ]
        
        stats = {
            "totals" : list(self.queue.aggregate(totalspipeline)),
            "workers" : list(self.queue.aggregate(workerspipeline))
        }

        return stats
    
    def rePrioritizeJob(self, _id, priority):
        self.connectToDb()
        j = self.getJobById(_id)        
        return self.queue.update_one({ "_id" : _id }, { '$set' : { "priority": int(priority) } } )

        
    def countJobsBefore(self, _id):
        self.connectToDb();
        #{ "jobFamily" : "psg-parent", "status" : { $in : ["pending", "running" ] }, "createdOn" : { $lt: "2021-04-02 04:43:09.561781" }, "priority" : { $gte: 1  } }
        j = self.getJobById(_id)
         
        # print("inCountJobsBefore")
        # print(f" {j['_id']} {j['createdOn']} {j['priority']} {j['status']}")

        filter = {
                "jobFamily" : j['jobFamily']  ,                 
                "$or" : [ 
                    { "status" : "running" },
                    {
                        "$and" : [
                            { "status" : "pending" },
                            { "priority" : { "$eq" : j['priority']  }   },
                            { "createdOn" : { "$lt": j['createdOn'] }   }
                        ] # end and
                    },
                    {
                        "$and" : [
                            { "status" : "pending" },
                           { "priority" : { "$gt" : j['priority'] } }                            
                        ] # end and
                    }
                ] #end or
            }

        # print(filter)

        # jobsBeforeList = list(self.queue.find(filter))
        
        # print(f"jobsBefore count: {len(jobsBeforeList)}")
        # for j in jobsBeforeList:
            # print(f" {j['_id']} {j['createdOn']} {j['priority']} {j['status']}")
        
        # print("")

        jobsBefore = self.queue.count_documents(
           filter
        )

# return self.queue.find_one_and_update({ "status" : "pending", "jobFamily" : family }, 
#         { '$set' : { "status" : "running", "startTime" : str(datetime.datetime.utcnow()), "workerid" : str(workerid)} },
#         return_document=pymongo.ReturnDocument.AFTER,
#         sort = [('priority', pymongo.DESCENDING), ('createdOn', pymongo.ASCENDING)]

        return jobsBefore

