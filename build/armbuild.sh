#!/bin/bash


docker build --pull --no-cache -t mdmoore25404/gpc-je-flask-arm:latest flask/
docker build --pull --no-cache -t mdmoore25404/gpc-je-nginx-arm:latest nginx/


docker images

docker push mdmoore25404/gpc-je-flask-arm:latest
docker push mdmoore25404/gpc-je-nginx-arm:latest

./triggerpipeline.sh

