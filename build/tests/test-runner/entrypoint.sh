#!/usr/bin/env bash

echo JE_TEST is: $JE_TEST

if [ -z "$JE_TEST" ]; then
    echo JE_TEST is empty. fail.
    exit 1
fi

if [ $JE_TEST = "cli" ]; then
    /app/cli/test.sh
fi

if [ $JE_TEST = "python" ]; then
    /app/gpc-je-python/tests/test.sh
fi