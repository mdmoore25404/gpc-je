# Job Engine CLI Client

The `jobengine` client is a CLI client that wraps the [Python Client](../gpc-je-python).

## System Requirements

* [JE Python Client](../gpc-je-python) 
* [Pandas](https://pypi.org/project/pandas/)
* [dicttoxml](https://pypi.org/project/dicttoxml/)

## Usage

`jobengine <command> <subcommand> [arguments]`

## Commands

Below are the commands within the client.

### jobs

Interact with multiple jobs at once.

#### jobs list

List jobs

`jobengine jobs list`

#### jobs add

Add jobs in bulk. Input file must be a valid array of objects. Example file: [jobs.json](jobs.json)

`jobengine jobs add --input <input.json>`

`--input` may be provided multiple times and each file is sent as a separate POST to the API. 

Output is a Python `dict` with keys for each `--input` containing a `list` of the added item(s), converted to `json` or `xml`.

*Note: `table` output is overriden to `json` for this command.*

#### jobs delete

Delete jobs. Use filters (see additional options below)

`jobengine jobs delete [--family <family>] [--status <status>] `

#### jobs purge

Remove ALL Jobs, ignoring any filters

`jobengine jobs purge`

### job

Interact with a single job

#### job add

Add a new job.

`jobengine job add <payload> [--friendlyName 'a friendly job name'] [--family jobFamily] [--priority <int>]`

#### job show

View a job's full record

`jobengine job show <_id>`

If a result is found by ID the result is a JSON object.

Or using friendlyName

`jobengine job show <friendlyName>` 

Implemented as a first check for `_id` and then for `friendlyName`. If match(es) found using `friendlyName` output is a JSON array.

#### job delete

Delete a job record

`jobengine job delete <_id>`

#### job repri

Edit the priority of a job

`jobengine job repri <_id> <new_priority>`

#### job log

Add an optional log item

`jobengine job log <_id> "<log message string>"`

Also you may use `friendlyName`, if the value is unique. If the value for `friendlyName` is not unique an error message will be shown.

`jobengine job log <friendlyName> "<log message string>"`

### families

Interact with job families

#### families list

Get a list of job families

`jobengine families list`

### family

Interact with a specific job family.

#### family show

View details of the job family

`jobengine family show <family>`

#### family pause

Mark a job family as paused

`jobengine family pause <family>`

#### family unpause|resume

Unpause a job family

`jobengine family unpause <family>`

`jobengine family resume <family>`

#### family prop

Set arbitrary properties on the job family. 

`jobengine family prop <family> <property> <new_value>`

#### family stats

Get statistics of job family and its workers.

`jobengine family stats <family>`

#### family delete

Delete the job family and all its jobs

`jobengine family delete <family>`

### count

Get the count of jobs by status.

 `jobengine count`

### workers

Interact with job workers

#### workers list

Get a list of job workers

`jobengine workers list`

### worker

Interact with a specific worker

#### worker show

View details of worker

`jobengine worker show <worker>`

#### worker delete

Delete the worker record

`jobengine worker delete <worker>`

## Additional options

Additional options that can be provided via flags or environment variables

### Output Format

You may also specify an output format with `--format` valid formats are: `table,json,xml`. `table` is the default. You can configure `JE_FORMAT` in an environment variable to avoid specifying `--format` each time. If provided, `--format` overrides `JE_FORMAT`.

### Debug mode

Is debug mode enabled?

Default: `false`

Environment `JE_DEBUG=true|false`

Flag `--debug`

## Pretty output

Enables pretty (human readable) output for for the `json` and `xml` output formats. If disabled no/minimal whitespacing is used.

Default: `false`

Environment `JE_PRETTY=true|false`

Flag `--pretty`, `-p`

## API Host

The URI of the API endpoint to talk to. The trailing slash is required.

Default: `http://localhost:8008/`

Environment: `JE_HOST=<value>`

Flag: `--uri <value>`, `-u <value>`

## job family

Provide a job family. Used when filtering or when adding.

Default: `""`

Environment: `JE_FAMILY=<value>`

Flag: `--family <value>`, `-f <value>`

## Filter by job status

The output of `jobs list` can be filtered by status. Valid values are: `pending|running|completed|failed`.

Default: `""`

Flag: `--status <value>`, `-s <value>`

## Specify table columns in Table format

If using `--format table` you can specify the columns that are given. Works for any multi or single records output.

Default for `jobs list`: `friendlyName,jobFamily,status,progress`

Default for `families list`: `_id,pause`

Flag: `--cols <col1,col2,col3>`, `-c <col1,col2,col3>`

## Limit results

Limit the result count for multi record outputs.

Default: `1000`

Environment: `JE_LIMIT=<value>`

Flag: `--limit <value>`, `-l <value>`

## Input file (bulk add)

Input file when adding bulk records

Flag: `--input <file>`, `-i <file>`

## Outout file

If output file is provided results are written to it instead of `stdout`.

Flag: `--output <file>`, `-o <file>`
