#!/usr/bin/env bash 


clear

set -x
shopt -s expand_aliases

alias je='./jobengine'

clear

je --help
echo
je count
echo
je families list
echo
je jobs list --limit 2
echo
je job show 3dd3545b-6ead-49d1-bc82-1ed3bda0f396 -p
echo
je workers list --family primes
echo
je -u http://192.168.0.70:30008/ families list
je -u http://192.168.0.70:30008/ family stats h265ize
echo
je job add mypayload --family demo --priority 2
echo
je jobs list --family demo -p -l 1