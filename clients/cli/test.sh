#!/usr/bin/env bash 

#### run tests for jobengine CLI client.

set -x
set -e
shopt -s expand_aliases

export PYTHONPATH="gpc-je-python"
alias je='cli/jobengine'

echo JE_HOST is $JE_HOST

runTests() {

    echo running tests with format $JE_FORMAT

    je jobs purge

    # empty results should not die

    je jobs list
    je families list
    je workers list

    # test adding single file
    je jobs add -i cli/jobs.json 

    je jobs delete
    je jobs add -i cli/jobs.json -i cli/jobs2.json
    
    #lookup job by friendlyName

    je job show 'IsPrime 99'
    je jobs delete --family primes2
    je count
    je jobs delete -s completed
    je job show 'IsPrime 99'
    je jobs delete -n 'IsPrime 99'
    je job show 'IsPrime 99'
    je count

    echo testing log feature

    je job log "IsPrime 1" "test log 1"
    je job log "IsPrime 1" test log 2

    je job show "IsPrime 1" --format json -p

    echo testing jobs command
    je jobs list
    je jobs list -l 2
    je jobs list --limit 2

    je jobs list -p -l 2
    je jobs list --pretty -l 2

    echo testing workers command

    je workers list -p
    je workers list 

    echo testing families command

    je families list
    je families list -p

    echo testing family command

    je family pause primes
    je family show primes -p
    je family unpause primes
    je family pause primes
    je family show primes -p
    je family resume primes
    je family show primes -p

    je jobs delete

    
}

export JE_FORMAT=json
runTests

export JE_FORMAT=xml
runTests

export JE_FORMAT=table
runTests
