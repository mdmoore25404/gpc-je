# Python Client Examples

This folder contains example use cases using the Python client for the job engine API.

The examples are:

* [primes](primes) - `n` workers used to determine if a number is prime. Job parameters and results stored using the API
* [h265ize](h265ize) - `n` workers. API used to store job parameters (files to encode), the significant output (encoded file) is saved to disk.
* [fib](fib) - `1` worker calculates several steps of the Fibonacchi sequence. Demonstrates chaining outputs as input to a new job (effectively recursion)
