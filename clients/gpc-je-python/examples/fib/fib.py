#/usr/bin/python3
import sys
sys.path.append("../../")
import python_client
import json
import os

url = os.getenv("JE_HOST", "http://localhost:8007/")
MAX_SEQ = os.getenv("JE_MAX_SEQ", 50) # limited to 50 because the numbers get huge

def calcFib(prev, lastPrev):
    return prev + lastPrev

def myWorker(j,jec):
    # collect the fields from the dict into convenient variables
    payload = j['payload']
    prev = payload['prev']
    lastPrev = payload['lastPrev']
    sequence = payload['sequence']
    #calculate the new fibonacci value using prev and lastPrev
    r = calcFib(prev, lastPrev)
    # mark this job completed with the new value
    jec.completeJob(j['_id'], str(r))
    # if we haven't hit our maximum number of calculations we'll submit the next request
    if sequence < MAX_SEQ-1:
        payload = {
            "lastPrev" : prev,
            "prev" : r,
            "sequence" : (sequence + 1)
        }
        jec.addJob(json.dumps(payload), f"{prev},{r}", jobFamily='fib' )
########

# API client using the family "fib", terminate when no jobs left in queue, and the userworker function myworker
jec = python_client.GpcJobEngineClient(url, family='fib', terminateOnNoJobs=True, userWorker = myWorker)

#payload for the initial job
payload = {
        "lastPrev" : 0,
        "prev" : 1,
        "sequence" : 0
    }

#submit the first item.
jec.addJob(json.dumps(payload), f"0,1", jobFamily='fib')
#start the client worker thread
jec.start()
