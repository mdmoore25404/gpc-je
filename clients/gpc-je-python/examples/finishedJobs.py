#!/usr/bin/env python3
import sys
sys.path.append("../gpc-je/clients/python")
import python_client
import os
import time
import smtplib
import requests
import json

checkSleep = int(os.getenv("JE_CHECK_SLEEP", 60))

url = os.getenv("JE_HOST", "http://localhost:8007/")
# define the API basepoint and a worker for worker example
jec = python_client.GpcJobEngineClient(url)
lastCount = None
allDone = {}

def sendNotification(n, message):    
    print(f"sendNotification called with{n}, {message}")
    if n["type"] == "pushover":
        fields = n
        fields.update({ "message" : message})        
        print(fields)
        url = "https://api.pushover.net/1/messages.json"
        r = requests.post(url,fields)
        print(r)

while True:
    countsResult = jec.getCounts()    
    for key in countsResult:
        # ensure 
        if key not in allDone:
            allDone[key] = True

        counts = countsResult[key]
        print(f"Checking family '{key}' value {counts}'")

        #debugging
        #allDone[key] = False

        if counts['pending'] == 0 and counts['running'] == 0:
            if not allDone[key]:
                # on the last check there was stuff in the queue
                fam = jec.getFamily(key)

                if "notification" in fam:
                    notification = fam["notification"]    
                    print(f"notification value {notification}")                                                                                                
                    sendNotification(notification, f"Jobs Complete for family {key}")

                allDone[key] = True
        else:
            # stuff in the queue
            print(f"stuff in the queue, setting allDone=False for {key}")
            allDone[key] = False

    time.sleep(checkSleep)
    
##### Example adding notification setting to family
# curl $JE_HOST/jobs/families/primes -F notification='{"type":"pushover","token":"<app token>","user":"<user token>"}' -X PUT