#!/usr/bin/python3
import os, sys, json
from optparse import OptionParser
import ffmpeg
sys.path.append("../../")
import python_client

url = os.getenv("JE_HOST", "http://localhost:8007/")
fileExtensions = None
parser = OptionParser()
parser.add_option("-d", "--dir", dest="dir", action="append",
                  help="A directory to search in")
parser.add_option("-e", "--ext", dest="extensions", action="store", default="mkv, mp4, m4v, avi, m2ts, mpeg, mts, ogm, mpg, mov, wmv",
                  help="Comma seperated list of file extensions to include")
parser.add_option("--debug", dest="DEBUG", action="store_true", default=False,
                  help="Detailed debugging info")

(opts, args) = parser.parse_args()
fileList = []
oList = []
jec = python_client.GpcJobEngineClient(url)

ext = [ x.strip() for x in opts.extensions.split(',') ]
cleanedExt = []

for e in ext:
    if len(e) == 0:
        continue

    if not e.startswith("."):
        e = "." + e
    cleanedExt.append(e)

fileExtensions = tuple(cleanedExt)
print(f"Working with file extensions: {fileExtensions}")

print(f"Opts.debug is {opts.DEBUG}")

def log(s):
    if opts.DEBUG:
        print(s)

def excludePath(filename):
    if "@eaDir" in filename:
        return True
    if not filename.lower().endswith(fileExtensions):
        log(f"file {filename} extension is not in the list to queue.")
        return True

def isH265(filename):
    try:
        probe = ffmpeg.probe(filename)
    except ffmpeg.Error as e:
        print(e.stderr, file=sys.stderr)
        return True
        
    vs = next((stream for stream in probe['streams'] if stream['codec_type'] == 'video'), None)
    if vs is None:
        log('No video stream found', file=sys.stderr)
        return True
    else:                                      
        codec = vs['codec_name']
        if codec.lower() != "hevc":                
            return False
        else:
            log(f"file {filename} is already H265 so skipping")
            return True

def workDirs(dirs):
    directory = os.fsencode(d)

    for root, subdirs, files in os.walk(directory):        
        for f in files:
            filename = os.fsdecode(os.path.join(root, f))            
            if excludePath(filename):
                continue

            if isH265(filename):
                continue
            else:
                fileList.append(filename)


for d in opts.dir:
    workDirs(d)


for f in fileList:
    o = { 
            "payload": {
                "file": f,
                "directory": os.path.dirname(f)
            },
            "jobFamily" : "h265ize",
            "friendlyName" : os.path.basename(f)
        }
    oList.append(o)

payload = json.dumps(oList, indent=3)


with open("payload.json", "w") as payloadFile:
    payloadFile.write(payload)

if jec.addJobsFromFile("payload.json"):
    print("Added jobs")
    ##os.remove("payload.json")
else:
    print("Could not add jobs")
