#!/usr/bin/python3

import os
import sys
sys.path.append("../../")
import python_client
import ffmpeg
import threading
import argparse
import shutil
import socket
import time
import subprocess
import pwd
from threading import Lock
import grp

uid = pwd.getpwnam("plex").pw_uid
gid = grp.getgrnam("plex").gr_gid

lock = Lock()

print(f"For chown operations will use uid:gid {uid}:{gid} ")

WORKER_THREADS = 1
TEMP_DIR = "/tmp/h265ize"
url = os.getenv("JE_HOST", "http://localhost:8007/")
jec = python_client.GpcJobEngineClient(url)
TERMINATE_ON_NO_JOBS = os.getenv("JE_TERMINATE", "true") == "true"
deleteOriginal = os.getenv("DELETE_ORG", "false").lower() == "true"

videoLengthMicroSeconds = 0
current_job = None
KeepRunning = True
DOIT = True
jec = None

def progressLoop():
    global current_job
    global KeepRunning
    global videoLengthMicroSeconds
    global jec

    # Create a TCP/IP socket        
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Bind the socket to the port
    server_address = ('localhost', 9999)
    print('starting up progress socket on {} port {}'.format(*server_address))
    sock.bind(server_address)
    # Listen for incoming connections    
    sock.listen(1)
    # Wait for a connection
    print('waiting for a connection')
    connection, client_address = sock.accept()
    try:
        while KeepRunning: #keep going until we are signaled to stop
            data = connection.recv(1024)
            #print('received {!r}'.format(data))
            if not data:            
                #print('no data from', client_address)                                    
                pass
            else:
                s = data.decode("ascii")
                
                lines = s.splitlines()
                d = {}
                for line in lines:
                    (key, value) = line.split("=")
                    d[key] = value
                                    
                out_time_ms = d['out_time_ms']
                
                progress =  int(out_time_ms) / videoLengthMicroSeconds
                if progress > 1.0:
                    progress = 1.0
                #print(f"Progress {progress}")
                lock.acquire()
                try:
                    if current_job != None:
                        jec.progressJob(current_job, progress)                    
                finally:
                    lock.release() # release lock, no matter what
                
    finally:
        # Clean up the connection
        connection.close()


def spawnme(workerid):
    global jec
    ## define the API basepoint and a worker for worker example
    jec = python_client.GpcJobEngineClient(url, userWorker= doTranscode, terminateOnNoJobs= TERMINATE_ON_NO_JOBS, workerId=workerid, family='h265ize')
    #### maybe you just want to call your function only if there's a job and not deal with that check/wait yourself:    
    jec.start()

def get_duration_microseconds(file):
    """Get the duration of a video using ffprobe."""
    cmd = 'ffprobe -i "{}" -show_entries format=duration -v quiet -of csv="p=0"'.format(file)
    output = subprocess.check_output(
        cmd,
        shell=True, # Let this run in the shell
        stderr=subprocess.STDOUT
    )    
    return float(output) * 1000000

def doTranscode(j, jec):   
    global current_job
    global KeepRunning
    global videoLengthMicroSeconds
    print(f"doTranscode got job {j}") 
    f = j['payload']['file']
    _id = j['_id']
    current_job = _id
    filename, extension = os.path.splitext(f)
    print(f"Filename '{filename}' Extension '{extension}'")
    outputfilename = f"{filename}.h265{extension}"    
    print(f"outputfilename = {outputfilename}")

    if os.path.exists(os.path.join( j['payload']['directory'], outputfilename)):
        jec.failJob(_id, f"Outputfilename exists in target directory: {os.path.join( j['payload']['directory'], outputfilename)}")
        return

    try:
        probe = ffmpeg.probe(f)
    except ffmpeg.Error as e:
        print(e.stderr, file=sys.stderr)
        jec.failJob(_id, f"Could not ffmpeg probe {e}")

    try:
        vs = next((stream for stream in probe['streams'] if stream['codec_type'] == 'video'), None)
        if vs is None:
            print('No video stream found', file=sys.stderr)
            sys.exit(1)
        else:
            #print(vs)            
            if not os.path.exists(TEMP_DIR):
                os.makedirs(TEMP_DIR)
                
            if DOIT:
                temp_file = os.path.join(TEMP_DIR, f"{_id}.mkv")
                videoLengthMicroSeconds = get_duration_microseconds(f)
                print(f"Video Length is: {videoLengthMicroSeconds} microsec")                
                lock.acquire()
                try:
                    KeepRunning = True
                finally:
                    lock.release()
                print("Starting progress thread")
                progressThread = threading.Thread(target=progressLoop)
                progressThread.start()
                
                (
                    ffmpeg
                    .input(f)
                    .output(os.path.join(temp_file), 
                            **{
                                "vaapi_device" : "/dev/dri/renderD128",
                                "vf" : "format=p010,hwupload",
                                "c:v" : "hevc_vaapi",
                                "c:a" : "copy",
                                "map_metadata" : "0",
                                #"t" : "30",
                                "progress" : "tcp://localhost:9999",
                                "q" : ""                                 
                            }
                        )            
                    .run()
                )
                lock.acquire()
                try:
                    print("current_job set to none")
                    current_job = None  
                    print("KeepRunning set to false")
                    KeepRunning = False
                finally:
                    lock.release()
                print("Moving file")
                # change file perms before moving                
                shutil.move(temp_file, os.path.join( j['payload']['directory'], outputfilename) )
                #print(f"chowning file to {uid}:{gid}")
                #os.chown(outputfilename, uid, gid)
                if deleteOriginal:
                    os.remove(f)
                else:
                    shutil.move(f, f"{f}.bak")

                print("Marking job completed")
                jec.completeJob(_id, f"Completed")
                print("Job marked completed")                                     
                
                print("Joining progress thread")
                progressThread.join()    
                print("Joined")
                videoLengthMicroSeconds = 0

            else:
                print("DOIT is false")
                jec.completeJob(_id, f"DOIT is false")            
        #mark completed
        
    except Exception as e:
        print(f"Exception {e}")
        jec.failJob(_id, f"{e}")

def main():
    t = threading.Thread(target=spawnme(socket.gethostname()))
    t.start()
    t.join()

if __name__ == "__main__":
    main()
