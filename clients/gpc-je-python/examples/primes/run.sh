#!/bin/bash


# export JE_HOST=http://flask:8007/
# optionally specify the host here to be seen by the python scripts

function usage() {
    echo "Usage: ./run.sh <targetjobs> <targetworkers>"
    exit 1
}

if [[ $EUID -ne 0 ]]; then
   echo "Because this script calls docker-compose it must be run as root" 
   exit 1
fi

if [[ -z $1 ]]; then
    usage
fi

targetjobs=$1


if [[ -z $2 ]]; then
    usage
fi
targetworkers=$2

echo "purging any jobs leftover"
python3 jobpurge.py 
echo "loading example jobs"
python3 jobload.py $targetjobs
echo "starting workers via docker-compose"
docker-compose up --build --scale worker=$targetworkers
