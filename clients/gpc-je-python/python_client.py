#### #!/usr/bin/python3

import threading
import sys
import requests
import time
import os
import warnings

class GpcJobEngineClient:
    """GPC Job EngineClient"""

    def __init__(self, url, userWorker = None, noJobsSleep = 5, noJobsSignal = None, workerId = '', terminateOnNoJobs = True, DEBUG = False, family='', httpuser = '', httppass = '', **kwargs):
        """ constructor
        Args:
            url: URL to API server. e.g. http://localhost:8008/
            userWorker: method in your code that will be called once for each job found matching the filters on this client
            noJobsSleep: If no jobs are returned from the API, wait this long (seconds) before trying again.
            noJobsSignal: If no jobs are found call this method in your code
            workerId: unique ID of this client. This could be a hostname or hostname-threadid etc
            terminateOnNoJobs: bool. If True when no jobs are found shuts down the client
            DEBUG: bool, should we output debug logs
            family: to filter to a specific `jobFamily` provide the family name
        """
        self.url = url
        self.processing = False
        self.workerThread = None        
        self.noJobsSleep = noJobsSleep
        self.noJobsSignal = noJobsSignal        
        self.userWorker = userWorker
        self.DEBUG = DEBUG
        self.terminateOnNoJobs = terminateOnNoJobs
        self.workerId = workerId
        self.family = family
        self.httpuser = httpuser
        self.httppass = httppass
            
    def log(self, s, level= "info"):
        """
        Log string to stdout. 
        
        If `GpcJobEngineClient.DEBUG` is True writes to stdout.

        If `level` is "warn" uses `warnings.warn`

        Args:
            s: string to log
            level: level of the log event
        
        """
        if self.DEBUG == True:
            sys.stdout.write("%s\n" % s)
        
        if level == "warn":
            warnings.warn(s)

    def addJob(self, payload, friendlyName = "", jobFamily = "", priority=1, parentId = ""):
        """
        Add a job to queue
        
        :param payload: dict of job settings
        :param priority=1: int optional priority for job
        """        
        return self.post("jobs", { 'payload': payload, "friendlyName" : friendlyName, "jobFamily" : jobFamily, "priority" : priority, "parentId" : parentId})
    
    
    def addJobsFromFile(self, filepath):
        """
        Add multiple jobs from a file. File must be a JSON array of objects with a required payload field. Optional fields are jobFamily and priority.
        
        Args:
            filepath: path to the file containing JSON
        
        Returns:
            bool indicating success
        
        Raises:
            Exception if `filepath` is not a file.
        """
        if os.path.isfile(filepath):
            with open(filepath, "r") as f:
                return self.postFile("jobs", f)
        else:
            raise Exception(f"{filepath} is not a file according to os.path.isfile")

    def get(self, m, params = {}):
        """
        Handles HTTP GET calls to API.
        
        Args:
            m: sub URL. E.g. `jobs` or `jobs/families`
            params: dict of parameters to include
        
        Returns:
            False if bad request or list of dicts

        Raises:
            Exception if unknown response code
        """
        if 'family' not in params:
            params['family'] = self.family
                
        self.log("Request GET " + self.url + m + " with parameters " + str(params))
        r = requests.get(url = self.url + m, params = params, auth=(self.httpuser, self.httppass))        
        if r.status_code == 200:
            data = r.json()            
            self.log(data)
            return data
        elif r.status_code == 204 or r.status_code == 404:
            return False
        else:
            raise Exception("Not sure how to handle response code {}".format(r.status_code))

    def postFile(self, m, file):            
        self.log("Request POST file " + self.url + m)
        r = requests.post(url = self.url + m, files={'file' : file}, auth=(self.httpuser, self.httppass) )
        if r.status_code == 201:
            data = r.json()            
            self.log(data)
            return data                
        else:
            raise Exception(f"Not sure how to handle response code {r.status_code} message from api: {r.text}")    

    def post(self, m, params = {}):
        """
        Handles HTTP POST calls to API.
        
        Args:
            m: sub URL. E.g. `jobs` or `jobs/families`
            params: dict of parameters to include
        
        Returns:
            False if bad request or list of dicts

        Raises:
            Exception if unknown response code
        """
        if 'family' not in params:
            params['family'] = self.family
        self.log("Request POST " + self.url + m + " params=" + str(params))
        r = requests.post(url = self.url + m, data = params, auth=(self.httpuser, self.httppass))
        if r.status_code == 201:
            data = r.json()
            self.log(data)
            return data
        else:
            raise Exception(f"Not sure how to handle response code {r.status_code} message from api: {r.text}")

    def put(self, m, params = {}):
        """
        Handles HTTP PUT calls to API.
        
        Args:
            m: sub URL. E.g. `jobs` or `jobs/families`
            params: dict of parameters to include
        
        Returns:
            False if bad request or list of dicts

        Raises:
            Exception if unknown response code
        """
        self.log("Request PUT " + self.url + m)
        r = requests.put(url = self.url + m, data = params, auth=(self.httpuser, self.httppass))        
        if r.status_code == 201:
            return True
        if r.status_code == 404:
            s = f"Got a 404 on a PUT. Job may have been deleted"
            self.log(s, level = "warn")
            return True
        else:
            raise Exception(f"Not sure how to handle response code {r.status_code} message from api: {r.text}")
    
    def delete(self, m, params = {}):
        """
        Handles HTTP Delete calls to API.
        
        Args:
            m: sub URL. E.g. `jobs` or `jobs/families`
            params: dict of parameters to include
        
        Returns:
            False if bad request or list of dicts

        Raises:
            Exception if unknown response code
        """        
        self.log("Request DELETE " + self.url + m + " with parameters " + str(params))
        if 'family' not in params:
            params['family'] = self.family                

        self.log("Request DELETE " + self.url + m)
        r = requests.delete(url = self.url + m, params = params, auth=(self.httpuser, self.httppass))        
        if r.status_code == 204:
            return True        
        else:
            raise Exception(f"Not sure how to handle response code {r.status_code} message from api: {r.text}")

    def purgeDelete(self, m):
        """
        Call API to delete `m` with no filtering.
        """
        self.log(f"Request PURGE DELETE {self.url + m} ")
        r = requests.delete(url = self.url + m, auth=(self.httpuser, self.httppass))
        if r.status_code == 204:
            return True
        else:
            raise Exception(f"Not sure how to handle response code {r.status_code} message from api: {r.text}")
            

    def getJobById(self, _id):
        """ Return job matching `_id` """
        return self.get(f"jobs/{_id}")
        
    def checkoutJob(self):
        """ Attempt to checkout a job.
        Returns:
            dict of job or empty
            """
        return self.get("jobs/checkout_one", {"workerid" : self.workerId})        
    
    def getJobsWithFilters(self, filters):
        """ return jobs matching `filters` """
        return self.get("jobs", filters)

    def deleteJobsWithFilters(self, filters):
        self.log(f"deleteJobsWithFilters called with filters: {filters}")
        """ delete jobs matching `filters` """
        return self.delete("jobs", filters)

    def purgeJobs(self):
        self.log(f"purgeJobs called")
        return self.purgeDelete("jobs")

    def getJobs(self, limit=1000):
        """ return jobs up to `limit` """
        return self.get("jobs", {"limit" : limit})
    
    def getPendingJobs(self, limit=1000):
        """ gets pending jobs up to `limit` """
        return self.get("jobs", {"limit" : limit, "status" : "pending"})

    def getRunningJobs(self, limit=1000):
        """ gets running jobs up to `limit`"""
        return self.get("jobs", {"limit" : limit, "status" : "running"})

    def getCompletedJobs(self, limit=1000):
        """ gets completed jobs up to `limit` """
        return self.get("jobs", {"limit" : limit, "status" : "completed"})

        """ Mark a job completed
        Args:
            _id: the ID of the job
            result: string stored in the `result` property of the job
            """
    def completeJob(self, _id, result):
        return self.put(f"jobs/{_id}", {"result": result })

    def progressJob(self, _id, progress):
        """ Update the progress of a job
        Args:
            _id: ID of job
            progress: Progress in decimal format. E.g. 0.5 for 50% complete
            """
        return self.put(f"jobs/{_id}", {"progress" : progress})
    
    def addLog(self, _id, log):
        """ add a log event to job
        Args:
            _id: ID of job
            log: text of log message
            """
        return self.put(f"jobs/{_id}", {"log" : log})

    def failJob(self, _id, error):
        """ Mark job failed
        Args:
            _id: ID of job
            error: string containing error message stored in the `error` property of the job
            """
        return self.put(f"jobs/{_id}", {"error": error })

    def returnJob(self, _id):
        """ return job. Useful if the client is unable to fulfil the request, but it's not the fault of the job parameters
        Args:
            _id: ID of job
        """
        return self.put(f"jobs/return/{_id}")


    def rePrioritizeJob(self, _id, priority):
        """ Update the priority of a job """
        return self.put(f"jobs/{_id}", {"priority" : int(priority)})

    def deleteAllJobs(self):
        """ Deletes ALL jobs for the client's defined family"""
        return self.delete("jobs")        
    
    def deleteJob(self, _id):
        """  Delete the specified job by `_id`"""
        return self.delete(f"jobs/{_id}")
    
    def getFamilies(self):
        """ returns job families """
        return self.get(f"jobs/families")

    def getCount(self, family, parentId):
        """ Returns count of total|pending|complete|running|failed jobs
        Args:
            family: family's `_id` if not provided the count is for all jobs across all families
        """
        self.log(f"getcount(family={family}, parentId={parentId})")
        return self.get(f"jobs/count", { "family" : family, "parentId" : parentId })

    def getCounts(self):
        """
            Returns the count for all families using `python_client.GpcJobEngine.getCount()` and `python_client.GpcJobEngine.getFamilies()`
        """
        counts = {}
        families = self.getFamilies()        
        for item in families:
            family = item["_id"]
            if family == "":
                continue
            c = self.getCount(family, parentId="")
            self.log(f"c={c}, family={family}")
            counts[family] = c
        return counts

    def worker(self):        
        """
        Internal worker thread entry point
        """
        self.log("Worker thread active")
        
        while True:  # keep looping until signaled to stop          
            if self.processing == False:
                break
            job = None
            try:
                job = self.checkoutJob()
            except Exception as e:
                self.log(f"Exception when trying to checkout a job. Performing a sleep.\n Exception detail {e}")
                time.sleep(self.noJobsSleep)
                continue               
            
            if job == False:
                #no job
                if self.terminateOnNoJobs:
                    # self terminate worker thread since no jobs pending
                    print("Terminating because terminateOnNoJobs is true and there are no jobs pending.")
                    self.stopProcessing()
                else:
                    #long running poll mode
                    time.sleep(self.noJobsSleep)
            else:
                #got a job
                if job == "PAUSED":
                    if self.terminateOnNoJobs:
                        print("Terminating because terminateNoJobs is true and the family queue is paused")
                        self.stopProcessing()
                    else:
                        print("Job family queue is paused")
                        time.sleep(self.noJobsSleep)
                else:                    
                    self.userWorker(job,self)

    def start(self,poolid=None):
        """ starts the worker thread"""
        if self.userWorker == None:
            raise Exception("userWorker not defined")
        else:
            if poolid != None:
                self.log(f"worker pool id is {poolid}")
            self.processing = True
            self.workerThread = threading.Thread(target=self.worker)        
            self.log("Starting job engine")
            self.workerThread.start()

    def stopProcessing(self):
        """ signals that processing should be stopped """
        self.processing = False
    
    def end(self):
        """ stops processing, joins worker thread and returns"""
        self.stopProcessing()
        self.workerThread.join()
        self.log("ending job engine")
    
    def getFamily(self, family):
        """ return family matching `family`"""
        self.log(f"in getFamilyConfig family={family}")
        return self.get(f"jobs/families/{family}")

    def setFamilyProp(self, family, prop, value):
        """ set a property on the given `family` so that `prop`=`value`"""
        self.log(f"in setFamilyProp {family}, {prop}, {value}")
        params = {}
        params[prop] = value
        return self.put(f"jobs/families/{family}", params)
        
    def familyPause(self, family):
        """ pauses `family` """
        return self.setFamilyProp(family, "pause", "true") # use string because it interprets python True/False as "True" instead of JSON true/false. lowercase true/false is translated to bool
    
    def familyUnPause(self, family):
        """ unpauses `family`"""
        return self.setFamilyProp(family, "pause", "false") # use string because it interprets python True/False as "True" instead of JSON true/false. lowercase true/false is translated to bool

    def resubmitJobs(self, family, status):
        """ Uses API /jobs/resubmit to resubmit jobs matching `family` and `status` """
        self.log(f"in resubmitJobs family={family}, status={status}")
        return self.post(f"jobs/resubmit?family={family}&status={status}")

    def getFamilyStats(self,family):
        """ return statistics for `family`"""
        return self.get(f"jobs/families/{family}/stats")

    def getWorkers(self, filters = {}):
        """ returns job workers"""
        return self.get(f"jobs/workers", filters)

    def getWorker(self, _id):
        """ returns job worker matching `_id`"""
        return self.get(f"/jobs/workers/{_id}")

    def deleteWorker(self, _id):
        """ deletes worker matching `_id`"""
        return self.delete(f"/jobs/workers/{_id}")

    def deleteFamily(self, family):
        """ Deletes family (and jobs) matching `family` """
        return self.delete(f"/jobs/families/{family}")

    def getChildren(self, parentid, family):
        """ gets children of parentid """
        return self.get(f"/jobs?parentId={parentid}&family={family}")
        
    