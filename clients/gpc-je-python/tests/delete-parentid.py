#!/usr/bin/env python3
##
## Test to create a parent with children jobs and then purge only the children jobs

import sys
sys.path.append("gpc-je-python")
import python_client
import json
import os

url = os.getenv("JE_HOST", "http://localhost:8007/")

# define the API basepoint and a worker for worker example
jec = python_client.GpcJobEngineClient(url, family = 'tests', DEBUG=True)

TARGET = 4
TARGET_CHILD = 5

assert jec.deleteAllJobs(), "delete any existing tests jobs"
assert jec.deleteJobsWithFilters( { "family" : "tests-children" } ), "delete existing tests-children jobs"


#load some jobs
for i in range(1,TARGET+1):
    payload = {}
    payload['testjob'] = i
    jec.addJob(json.dumps(payload), f"tests {i}", jobFamily='tests')
    for j in range(1, TARGET_CHILD+1):
        child = {}
        child['child'] = f"child {j} of parent {i}"
        jec.addJob(json.dumps(child), child['child'], jobFamily='tests-children', parentId=i)


c1 = jec.getCount("tests", "")
print(c1)
assert c1['total'] == TARGET, "expected parent count"

c2 = jec.getCount("tests-children", "")
print(c2)
assert c2['total'] == TARGET_CHILD * TARGET, "expected child count"


# step through deleting children

for i in range(1, TARGET+1):
    assert jec.deleteJobsWithFilters( { "family" : "tests-children", "parentId" : i}), "delete one set of children"
    c2 = jec.getCount("tests-children", "")
    print(c2)
    assert c2['total'] == TARGET_CHILD * (TARGET - i), "expected child count"

c3 = jec.getCount("tests-children", "")
assert c3['total'] == 0, "no children left"


c1 = jec.getCount("tests", "")
print(c1)
assert c1['total'] == TARGET, "expected parent count"


# cleanup
assert jec.deleteAllJobs(), "cleanup parent jobs"
jec.deleteJobsWithFilters( { "family" : "tests-children"}), "cleanup children, though there should be 0 left here"

print("tests complete")

