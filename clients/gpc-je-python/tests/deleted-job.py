#!/usr/bin/env python3
##
## Test to create jobs, check them out, and kill the job before attempting to PUT a status
## python client should raise a warning but not crash
## Tests gitlab issue #24's patch
##
import sys
sys.path.append("gpc-je-python")
import python_client
import json
import os

url = os.getenv("JE_HOST", "http://localhost:8007/")

# define the API basepoint and a worker for worker example
jec = python_client.GpcJobEngineClient(url, family = 'tests', DEBUG=True)


TARGET = 4

jec.deleteAllJobs()

#load some jobs
for i in range(1,TARGET+1):
    payload = {}
    payload['testjob'] = i
    jec.addJob(json.dumps(payload), f"tests {i}", jobFamily='tests')

#test a job

j1 = jec.checkoutJob()

if j1 != False:
    jec.progressJob(j1['_id'], 0.5) # should work
    jec.deleteJob(j1['_id']) #should work
    jec.progressJob(j1['_id'], 0.6) # should not crash

j2 = jec.checkoutJob()
if j2 != False:
    jec.deleteJob(j2['_id']) #should work
    jec.completeJob(j2['_id'], "woot") # should not crash


j3 = jec.checkoutJob()
if j3 != False:
    jec.deleteJob(j3['_id']) # should work
    jec.failJob(j3['_id'], "test failJob after deleted") # should not crash
    

print("tests complete")

jec.deleteAllJobs()