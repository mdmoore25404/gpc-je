#!/usr/bin/env python3
##
## Test to create a parent with children jobs and then purge only the children jobs

import sys
sys.path.append("gpc-je-python")
import python_client
import json
import os

url = os.getenv("JE_HOST", "http://localhost:8007/")

# define the API basepoint and a worker for worker example
jec = python_client.GpcJobEngineClient(url, family = 'tests', DEBUG=True)

TARGET = 4

assert jec.deleteJobsWithFilters( { "family": ['tests-1', 'tests-2'] }), "delete any leftover test family jobs"


#load some jobs
for i in range(1,TARGET+1):
    payload = {}
    payload['testjob'] = i
    jec.addJob(json.dumps(payload), f"tests-1 {i}", jobFamily='tests-1')

#load another family
for i in range(1,TARGET+1):
    payload = {}
    payload['testjob'] = i
    jec.addJob(json.dumps(payload), f"tests-2 {i}", jobFamily='tests-2')


c1 = jec.getCount("tests-1", "")
print(c1)
assert int(c1['total']) == TARGET, "expected tests-1 count"

c2 = jec.getCount("tests-2", "")
print(c1)
assert c2['total'] == TARGET, "expected tests-2 count"

ca = jec.getCount(['tests-1', 'tests-2'], "")
print (ca)
assert ca['total'] == TARGET * 2, 'expected total count of target*2'

# cleanup
assert jec.deleteJobsWithFilters( { "family": ['tests-1', 'tests-2'] }), "delete any leftover test family jobs"


print("tests complete")
