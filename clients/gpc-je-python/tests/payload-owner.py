#!/usr/bin/env python3
##

##
import sys
sys.path.append("gpc-je-python")
import python_client
import json
import os
import requests


url = os.getenv("JE_HOST", "http://localhost:8008/")

# define the API basepoint and a worker for worker example
jec = python_client.GpcJobEngineClient(url, family = 'tests', DEBUG=True)


TARGET = 5

assert jec.purgeJobs(), "clean working environment."


c = jec.getCount(family = "", parentId = "")
assert c['total'] == 0, "no jobs"

#add jobs
for i in range(1,TARGET+1):
    payload = {}    
    payload['testjob'] = i    
    payload['owner'] = f"{i}"
    jec.addJob(json.dumps(payload), f"tests {i}", jobFamily='tests')


for i in range(1,TARGET+1):
    payload = {}    
    payload['testjob'] = i    
    payload['owner'] = f"{i}"
    jec.addJob(json.dumps(payload), f"tests2 {i}", jobFamily='tests2')


j1 = jec.getJobsWithFilters( { "payload.owner" : "1",  "family" : "tests" })[0]


for i in range(1, TARGET+1):
    j1 = jec.getJobsWithFilters( { "payload.owner" : f"{i}",  "family" : "tests" })[0]
    assert j1['payload']['owner'] == f"{i}", "got the owner we expect"
    assert j1['jobFamily'] == "tests", "family we expected"


for i in range(1, TARGET+1):
    j2 = jec.getJobsWithFilters( { "payload.owner" : f"{i}",  "family" : "tests2" })[0]
    assert j2['payload']['owner'] == f"{i}", "got the owner we expect"
    assert j2['jobFamily'] == "tests2", "family we expected"

assert jec.purgeJobs(), "purge completes"


