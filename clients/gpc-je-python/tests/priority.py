#!/usr/bin/env python3
##
## Test job priorities
##
import sys
sys.path.append("gpc-je-python")
import python_client
import json
import os
import requests

url = os.getenv("JE_HOST", "http://localhost:8007/")

# define the API basepoint and a worker for worker example
jec = python_client.GpcJobEngineClient(url, family = 'tests', DEBUG=True)


TARGET = 4

assert jec.purgeJobs() #clean working environment.

#load some jobs 1-4 starting at 1
for i in range(1,TARGET+1):
    payload = {}    
    payload['testjob'] = i    
    jec.addJob(json.dumps(payload), f"tests {i}", jobFamily='tests', priority=i)

#while we inserted jobs 1,2,3,4 we should checkout in descending order
j = jec.checkoutJob()
assert j["friendlyName"] == "tests 4"

j = jec.checkoutJob()
assert j["friendlyName"] == "tests 3"

j = jec.checkoutJob()
assert j["friendlyName"] == "tests 2"

j = jec.checkoutJob()
assert j["friendlyName"] == "tests 1"

### submitting a job without specifying priority should result in priority=1

newJob = jec.addJob( json.dumps({ "payload": { "foo" : "bar"} }), f"autopri")
fromUri = jec.getJobById(newJob['_id'])
assert fromUri['priority'] == 1    

#jec.deleteAllJobs()

## here we just do a straight HTTP call, not routed through the client, it should be defaulted to pri=1
params = { "payload" : "curl-equiv", "jobFamily" : "tests"}
r = requests.post(url = url + "jobs", data = params)
assert r.status_code == 201

curlJob = jec.checkoutJob()
assert curlJob['priority'] == 1

#jec.deleteAllJobs()

#straight http call with a pri
params = { "payload" : "curl-equiv", "jobFamily" : "tests", "priority" : 111}
r = requests.post(url = url + "jobs", data = params)
assert r.status_code == 201

curlJob = jec.checkoutJob()
assert curlJob['priority'] == 111

### changing pri
#chang curlJob's pri to 222
jec.rePrioritizeJob(curlJob['_id'], 222)
updatedJob = jec.getJobById(curlJob['_id'])
assert updatedJob['priority'] == 222

#changing to 333 using HTTP call
r = requests.put(url = url + f"jobs/{curlJob['_id']}", data= { "priority" : 333})
updatedJob = jec.getJobById(curlJob['_id'])
assert updatedJob['priority'] == 333



assert jec.purgeJobs()


#got here so it all worked yay