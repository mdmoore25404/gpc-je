#!/usr/bin/env bash

set -x
#set -e



python3 -m pytest gpc-je-python/tests/*.py

if [ $? -eq 5 ]
then
    echo 'exit code was 5 - no tests collected. this is expected.'
    exit 0
fi


# for file in gpc-je-python/tests/*.py
# do
#     exec $file
# done

