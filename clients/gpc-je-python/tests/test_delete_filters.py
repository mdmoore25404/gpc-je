#!/usr/bin/env python3
##
## Test job priorities
##
import sys
sys.path.append("gpc-je-python")
import python_client
import json
import os
import requests


url = os.getenv("JE_HOST", "http://localhost:8008/")

# define the API basepoint and a worker for worker example
jec = python_client.GpcJobEngineClient(url, family = 'tests', DEBUG=True)


TARGET = 5

assert jec.purgeJobs(), "clean working environment."

c = jec.getCount(family = "", parentId = "")
assert c['total'] == 0, "no jobs"

#add jobs
for i in range(1,TARGET+1):
    payload = {}    
    payload['testjob'] = i    
    jec.addJob(json.dumps(payload), f"tests {i}", jobFamily='tests')


for i in range(1,TARGET+1):
    payload = {}    
    payload['testjob'] = i    
    jec.addJob(json.dumps(payload), f"tests2 {i}", jobFamily='tests2')


count1 = jec.getCount(family = "", parentId = "")

assert count1['total'] == 10, "total jobs"

familyCounts = jec.getCounts()

assert familyCounts["tests"]["total"] == 5, "5 jobs in tests family"
assert familyCounts["tests2"]["total"] == 5, "5 jobs in tests2 family"

assert jec.deleteJobsWithFilters({ "family": "tests2"})

count2 = jec.getCount(family = "", parentId = "")
assert count2['total'] == 5, "left the tests family alone"

## test deleting by friendlyName

assert jec.deleteJobsWithFilters( { "family" : "tests", "name" : "tests 2" }), "delete one"

count3 = jec.getCount(family = "", parentId = "")
assert count3['total'] == 4, "one job removed"

jobs1 = jec.getJobsWithFilters( { "name" : "tests2"})
assert len(jobs1) == 0, "empty result for deleted job"

assert jec.purgeJobs(), "purge completes"


