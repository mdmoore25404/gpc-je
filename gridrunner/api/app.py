#!/usr/bin/python3

from flask import Flask, g
from flask.globals import session
from flask_restful import Resource, Api, reqparse, request
import os
import json
import flask_compress
import pymongo

import uuid
import datetime
from flask_cors import CORS
from pymongo.common import validate
import tempfile
from werkzeug.utils import secure_filename
import bcrypt
import python_client
from stat import *
from minio import Minio
from minio.error import *
import tarfile
import requests
import db_bootstrap

_API_ROLES = ["admin"]

## Read environment for any changes to default options
_GPC_MONGO_HOST = os.getenv('GPC_MONGO_HOST', 'localhost') # read mongo host from environment
_GPC_MONGO_PORT = int(os.getenv( 'GPC_MONGO_PORT', 27017))
_JE_HOST = os.getenv("GRIDRUNNER_JE_HOST", "http://localhost:8007/")


#### environment vars
## must be supplied by user
MINIO_HOST          = os.getenv("MINIO_HOST", "localhost:9000")
MINIO_ACCESS_KEY    = os.getenv("MINIO_ACCESS_KEY", "example")
MINIO_SECRET_KEY    = os.getenv("MINIO_SECRET_KEY", "exampledontuse")
MINIO_SECURE        = os.getenv("MINIO_SECURE", False)
INPUT_BUCKET_NAME   = os.getenv("INPUT_BUCKET_NAME", "psg-inputs") #safe default
JE_FAMILY           = os.getenv("JE_FAMILY", "psg-parent") # safe default
JE_CHILD_FAMILY     = os.getenv("JE_CHILD_FAMILY", "psg-child") #sane default
JE_USER             = os.getenv("JE_USER", "")
JE_PASS             = os.getenv("JE_PASS", "")
SESSION_EXP_MINUTES = int(os.getenv("SESSION_EXP_MINUTES", 15 ))

BIG_JOB_PRIORITY    = int(os.getenv("BIG_JOB_PRIORITY", 1))
SMALL_JOB_PRIORITY  = int(os.getenv("SMALL_JOB_PRIORITY", 2))
SMALL_JOB_LIMIT     = int(os.getenv("SMALL_JOB_LIMIT", 500))
SMALL_JOB_FAMILY    = os.getenv("SMALL_JOB_FAMILY", "psg-parent-small")
SMALL_JOB_CHILD_FAMILY = os.getenv("SMALL_JOB_CHILD_FAMILY", "psg-child-small")

PSG_HOST            = os.getenv("PSG_HOST", "http://localhost:3000")

acceptableUpdateAgeSec = int(os.getenv("ACCEPTABLE_WORKER_AGE_SEC",  60  * 5)) #5 minutes

minioClient = Minio(MINIO_HOST,
                    access_key = MINIO_ACCESS_KEY, 
                    secret_key = MINIO_SECRET_KEY, 
                    secure=MINIO_SECURE)


je = python_client.GpcJobEngineClient(_JE_HOST, DEBUG=False)

parser = reqparse.RequestParser()
parser.add_argument('result', type=dict)

app = Flask(__name__)
CORS(app)
api = Api(app)


COMPRESS_MIMETYPES=['text/html', 'application/json'] # compress html and json
COMPRESS_LEVEL = 6
COMPRESS_MIN_SIZE = 500
ALLOWED_EXTENSIONS = ('tar.gz')
flask_compress.Compress(app)


def requireRole(role, roles):
    return role in roles

def bcryptPassword(cleartext):
    salt = bcrypt.gensalt(rounds=16)
    hashed = bcrypt.hashpw(cleartext.encode('utf8'), salt)
    return hashed
    
def compareBcryptPassword(cleartext, hashed):
    tmp = cleartext.encode('utf8')
    return bcrypt.checkpw(tmp, hashed)


def is_json(myjson):
  try:
    json_object = json.loads(myjson)
  except ValueError as e:
    return False
  return True

def jsonOrString(s):
    if is_json(s):
        return json.loads(s)
    else:
        return s

def NoneToEmptyString(s):
    if s == None:
        return ""
    else:
        return s

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def validateAccessToJob(sessionid,_id):    
    (session, user) = getSessionUser(sessionid)
    if session == False or user == False:
            return False
    else:
        return  _id in user['jobs']

def getMongoConnection():
    conn = pymongo.MongoClient(host=_GPC_MONGO_HOST, port=_GPC_MONGO_PORT)
    db = conn['gridrunner']
    usersColl = db['users']
    sessionsColl = db['sessions']        
    return (conn, db, usersColl, sessionsColl)

def getSessionUser(sessionid):
    (conn, db, users, sessionsColl) = getMongoConnection()    
    session = sessionsColl.find_one({ "_id" : sessionid})
    if session == None:
        return (False, False)
        #raise Exception(f"invalid sessionid {sessionid}")
    else:
        ############################################################ use projection to not return password field since we don't need it
        user = users.find_one( { "username" : session['username'] }, {'password' : False} )
        if user == None:
            raise Exception(f"invalid user {session['username']}")
        else:
            now = datetime.datetime.utcnow()
            sessionsColl.update_one( { "username" : user['username']}, { "$set" : { "lastAction" : now } })
            return (session, user)

class Login(Resource):
    def post(self):
        rf = request.form.to_dict()
        if "username" in rf.keys() and "password" in rf.keys():
            username = request.form['username']
            password = request.form['password']
            (conn, db, users, sessionsColl) = getMongoConnection()            
            user = users.find_one({ "username" : username })
            
            if user == None:
                return "invalid credentials", 400
            
            if compareBcryptPassword(password, user['password']):
                sessionid = str(uuid.uuid4())
                result = {
                    "sessionid": sessionid,
                    "roles" : user['roles']
                }                
                now = datetime.datetime.utcnow()
                sessionsColl.delete_many( { "username" : username }) #purge user's old sessions
                sessionsColl.insert_one({ "_id": sessionid, "username" : username, "createdOn" : now, "lastAction" : now } )
                return (result),200
            else:
                return "invalid credentials",400
        else:
            return "username and password are required form fields", 400

class SubmitJob(Resource):
    def post(self):
        return 401

class ToggleRole(Resource):
    def put(self, username):        
        sessionid = request.args.get("sessionid")
        (session, user) = getSessionUser(sessionid)
        if session == False or user == False:
            return "invalid session", 400

        if not requireRole("admin", user['roles']):
            return "not authorized", 401

        role = request.form['role']
        has = request.form['has']

        if role == None or role == "":
            return "missing field role", 401
        
        if has == None or has == "":
            return "missing field has", 401

        has = has == "true"

        (conn, db, users, sessionsColl) = getMongoConnection()
        
        if has:
            users.update ( { 'username' : username}, { "$push" : { "roles" : role } })
        else:
            users.update ( { 'username' : username}, { "$pull" : { "roles" : role } })

        return "record updated", 204
        

class GetMyJobs(Resource):
    def get(self):
        sessionid = request.args.get("sessionid")
        (session, user) = getSessionUser(sessionid)
        
        if session == False or user == False:
            return "invalid session", 400
        # print(session)
        # print(user)

        jobs = list(je.getJobsWithFilters( { "payload.owner" : user['username'], "family" : [JE_FAMILY, SMALL_JOB_FAMILY] }))
        return (jobs), 200

class Job(Resource):
    def get(self, _id):
        sessionid = request.args.get("sessionid")
        try:
            if validateAccessToJob(sessionid, _id):
                job = je.getJobById(_id)
                if not job:
                    return "job not found",404
                else:
                    return job
            else:
                return "not authorized to view job " + _id, 401
        except Exception as e:
            return str(e),500

    def delete(self, _id):
        sessionid = request.args.get("sessionid")
        try:
            if validateAccessToJob(sessionid, _id):
                je.deleteJob(_id)                
                (conn, db, users, sessionsColl) = getMongoConnection()
                session = sessionsColl.find_one({ "_id" : sessionid})
                username = session['username']
                users.update( { 'username' : username}, {  '$pull' : { 'jobs' : _id } })

                return {}, 204
            else:
                return "not authorized to delete job " + _id, 401
        except Exception as e:
            return str(e),500


class Logout(Resource):
    def delete(self, sessionid):
        print(f"Logging out {sessionid}")
        (conn, db, users, sessionsColl) = getMongoConnection()
        sessionsColl.delete_one({ "_id" : str(sessionid)})
        return "",200




class submitUpload(Resource):
    def post(self):
        if 'file' not in request.files:
            return "no file", 400

        if 'name' not in request.form:
            return "no name", 400

        if request.form['name'] == "":
            return "no name", 400        

        file = request.files['file']
        name = request.form['name']

        sessionid = request.args.get("sessionid")
        # print(f"Sessionid {sessionid}")

        if file.filename == '':
            return "no file", 400
        

        if file and allowed_file(file.filename):
            try:
                with tempfile.TemporaryDirectory() as td:
                    print(f"Working with tempdir: {td}")
                    secFilename = secure_filename(file.filename)
                    temptarfile = os.path.join(td,  secFilename)
                    print(f"temptarfile = {temptarfile}")
                    file.save( temptarfile )                    
                    priority = BIG_JOB_PRIORITY
                    submissionFamily = JE_FAMILY

                    ### get menber count in tarfile
                    with tarfile.open(temptarfile) as tf:             
                        print(f"investigating tarfile {temptarfile}")           
                        members = tf.getmembers()
                        count = len(members)
                        print(f"temp tarfile {temptarfile} has {count} members")
                        if count <= SMALL_JOB_LIMIT:
                            priority = SMALL_JOB_PRIORITY     
                            submissionFamily = SMALL_JOB_FAMILY              

                    print(f"Job priority will be {priority}")
                    uid = uuid.uuid4() # create a uuid for the file
                    print(f"attempting to submit file to bucket as {INPUT_BUCKET_NAME}/{uid}.tar.gz")
                    minioClient.fput_object(INPUT_BUCKET_NAME, f"{uid}.tar.gz", temptarfile, content_type="application/gzip")
                    print(f"tarball stored in bucket {MINIO_HOST}/{INPUT_BUCKET_NAME}")  

                    (conn, db, users, sessionsColl) = getMongoConnection()
                    session = sessionsColl.find_one({ "_id" : sessionid})
                    # print(f"session {session}")
                    username = session['username']
                    # print(f"username {username}")                   

                    parentJobPayload = {
                        "tarballName" : f"{uid}.tar.gz",
                        "owner" : username,
                        "uid" : str(uid),
                        "jobType" : "parent",                        
                        "flags": {  } # refer to https://psg.gsfc.nasa.gov/helpapi.php for additional flags and values
                    }

                    output = je.addJob(json.dumps(parentJobPayload), name, jobFamily=submissionFamily, priority=priority)
                    # print(f"Ooutput {output}")
                    
                    users.update( { 'username' : username}, {  '$push' : { 'jobs' : output['_id'] } })
                    return "submitted", 201
            except Exception as e:                
                print(e.with_traceback)
                return f"Exception error {e}", 500

        else:
            return "invalid file", 400

class Health(Resource):
    def get(self):
        parentWorkers = je.getWorkers( { "family" : [ JE_FAMILY, SMALL_JOB_FAMILY  ] } )
        childWorkers = je.getWorkers(  { "family" : [  JE_CHILD_FAMILY,  SMALL_JOB_CHILD_FAMILY ] } )

        now = datetime.datetime.utcnow()        
        inSpec = True

        for w in parentWorkers:
            timestamp = datetime.datetime.strptime(w['updatedTime'], "%Y-%m-%d %H:%M:%S.%f" )
            delta = now - timestamp
            if delta.total_seconds() > acceptableUpdateAgeSec:
                inSpec = False
                w['inSpec'] = False
            else:
                w['inSpec'] = True
            
        for w in childWorkers:
            timestamp = datetime.datetime.strptime(w['updatedTime'], "%Y-%m-%d %H:%M:%S.%f" )
            delta = now - timestamp
            if delta.total_seconds() > acceptableUpdateAgeSec:
                inSpec = False
                w['inSpec'] = False
            else:
                w['inSpec'] = True

        ret = {
            "parents" : parentWorkers,
            "children" : childWorkers,
            "inSpec" : inSpec
        }
        return ret
        
class User(Resource):
    def get(self, username):
        sessionid = request.args.get("sessionid")
        (session, user) = getSessionUser(sessionid)        
        if session == False or user == False:
            return "invalid session", 400

        if requireRole("admin", user['roles']):
            print(f"deleting username {username} action performed by user {user['username']}")
            (conn, db, users, sessionsColl) = getMongoConnection()
            return users.find_one( { "username" : username}), 200
        else:
            return "not an admin", 401
            

    def put(self, username):
        sessionid = request.args.get("sessionid")
        (session, user) = getSessionUser(sessionid)
        if session == False or user == False:
            return "invalid session", 400
        
        if not "operation" in request.form:
            return "operation field required", 400

        operation = request.form['operation']

        if operation == "pwchange":
            # require this be the user 
            if user['username'] != username:
                return "that's not you", 401
            else:
                if not "newpass" in request.form:
                    return "newpass field required", 400
                
                if not "oldpass" in request.form:
                    return "oldpass field required", 400

                newpass = request.form['newpass']
                oldpass = request.form['oldpass']
                
                (conn, db, users, sessionsColl) = getMongoConnection()
                fullUserRecord = users.find_one( { "username" : username })                

                if compareBcryptPassword(oldpass, fullUserRecord['password']):
                    newHash = bcryptPassword(newpass)
                    users.update_one( { "username" : username }, { "$set" : { "password" : newHash, "lastPwChange" : datetime.datetime.utcnow() } })
                    #toss existing session to force login
                    sessionsColl.delete_one( { "username" : username, "_id" : sessionid})
                    return "",200
                else:
                    return "invalid credentials", 401

        elif operation == "addrole":            
            if not "role" in request.form:
                return "role is required", 400

            role = request.form['role']

            if requireRole("admin", user['roles']):
                print(f"adding role {role} to {username} action performed by user {user['username']}")
                (conn, db, users, sessionsColl) = getMongoConnection()
                users.update( { 'username' : username}, {  '$push' : { 'roles' : role } })
                return f"role {role} added to {username}",200
            else:
                return "not an admin", 401


        elif operation == "remrole":
            if not "role" in request.form:
                return "role is required", 400

            role = request.form['role']

            if requireRole("admin", user['roles']):
                print(f"adding role {role} to {username} action performed by user {user['username']}")
                (conn, db, users, sessionsColl) = getMongoConnection()
                users.update( { 'username' : username}, {  '$pull' : { 'roles' : role } })
                return f"role {role} removed from {username}",200
            else:
                return "not an admin", 401

        else:
            return f"unknown operation {operation}", 400


    def delete(self, username):
        sessionid = request.args.get("sessionid")
        (session, user) = getSessionUser(sessionid)
        if session == False or user == False:
            return "invalid session", 400

        if requireRole("admin", user['roles']):
            print(f"deleting username {username} action performed by user {user['username']}")
            (conn, db, users, sessionsColl) = getMongoConnection()
            users.find_one_and_delete({ "username" : username })
            sessionsColl.delete_many( { "username" : username })            
            return "",204
        else:
            return "not authorized", 401
      
        

class Users(Resource):
    def get(self):
        sessionid = request.args.get("sessionid")
        (session, user) = getSessionUser(sessionid)
        if session == False or user == False:
            return "invalid session", 400

        if requireRole("admin", user['roles']):
            (conn, db, users, sessionsColl) = getMongoConnection()
            l = list(users.find( { }, projection= { "_id": 0, "password" : 0 } ) )
            for u in l:                
                u['lastPwChange'] = str(u['lastPwChange'])

            return l,200
        else:
            return "not authorized", 401

    def post(self):
        sessionid = request.args.get("sessionid")
        (session, user) = getSessionUser(sessionid)        
        if session == False or user == False:
            return "invalid session", 400

        if not requireRole("admin", user['roles']):
            return "not authorized", 401

        (conn, db, users, sessionsColl) = getMongoConnection()
        username = request.form['username']
        password = request.form['password']
        email = request.form['email']

        if username != None and password != None and username != "" and password != "":
            newUser = {
                "username" : username,
                "password" : bcryptPassword(password),
                "email" : email,
                "jobs": [],
                "roles" : [],
                "lastPwChange" : datetime.datetime.utcnow()

            }

            users.insert_one(newUser)
            return "created", 201

        else:
            return "invalid request", 401

class Roles(Resource):
    def get(self):
        return _API_ROLES, 200

class PsgInfo(Resource):
    def get(self):
        r = requests.get(PSG_HOST, headers= { 'User-Agent':  "curl/7.64.1"} ) # emulate curl for plain text output
        if r.status_code == 200:
            return r.text, 200
        else:
            return f"Error communicting with PSG at {PSG_HOST}", 500



api.add_resource(Login, '/api/login')
api.add_resource(SubmitJob, '/api/psg-job')
api.add_resource(GetMyJobs, '/api/myjobs')
api.add_resource(Job, '/api/job/<string:_id>')
api.add_resource(Logout,'/api/logout/<string:sessionid>')
api.add_resource(submitUpload, '/api/submitUpload')
api.add_resource(Health, '/api/health')
api.add_resource(User, "/api/user/<string:username>")
api.add_resource(ToggleRole, '/api/togglerole/<string:username>')
api.add_resource(Users, "/api/users")
api.add_resource(Roles, "/api/roles")
api.add_resource(PsgInfo, "/api/psginfo")

        

            
#####       
# 
# 
#  



if __name__ == '__main__':    
    db_bootstrap.db_bootstrap()
    app.run(host='0.0.0.0', port=8005)