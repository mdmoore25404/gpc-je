#!/usr/bin/env python3

import pymongo
from minio.error import *
import datetime
import bcrypt
import os
from minio import Minio
from minio.error import *

## Read environment for any changes to default options
_GPC_MONGO_HOST = os.getenv('GPC_MONGO_HOST', 'localhost') # read mongo host from environment
_GPC_MONGO_PORT = int(os.getenv( 'GPC_MONGO_PORT', 27017))
SESSION_EXP_MINUTES = int(os.getenv("SESSION_EXP_MINUTES", 15 ))
acceptableUpdateAgeSec = int(os.getenv("ACCEPTABLE_WORKER_AGE_SEC",  60  * 5)) #5 minutes


MINIO_HOST          = os.getenv("MINIO_HOST", "localhost:9000")
MINIO_ACCESS_KEY    = os.getenv("MINIO_ACCESS_KEY", "example")
MINIO_SECRET_KEY    = os.getenv("MINIO_SECRET_KEY", "exampledontuse")
MINIO_SECURE        = os.getenv("MINIO_SECURE", False)
INPUT_BUCKET_NAME   = os.getenv("INPUT_BUCKET_NAME", "psg-inputs") #safe default
OUTPUT_BUCKET_NAME   = os.getenv("OUTPUT_BUCKET_NAME", "psg-outputs") #safe default


minioClient = Minio(MINIO_HOST,
                    access_key = MINIO_ACCESS_KEY, 
                    secret_key = MINIO_SECRET_KEY, 
                    secure=MINIO_SECURE)

def bcryptPassword(cleartext):
    salt = bcrypt.gensalt(rounds=16)
    hashed = bcrypt.hashpw(cleartext.encode('utf8'), salt)
    return hashed


######## Methods
def initBucket(n):
    if not minioClient.bucket_exists(n):
        try:
            minioClient.make_bucket(n)
        except BucketAlreadyOwnedByYou as err:
            pass
        except BucketAlreadyExists as err:
            pass
        except ResponseError as err:
            raise


def db_bootstrap():
    # #bootstrap database
    # print("Starting up. Configuration follows....")
    # print(f"ACCEPTABLE_WORKER_AGE_SEC = {acceptableUpdateAgeSec}")
    # print(f"SESSION_EXP_MINUTES = {SESSION_EXP_MINUTES}")
    # print(f"GPC_MONGO_HOST = {_GPC_MONGO_HOST}")
    # print(f"GPC_MONGO_PORT = {_GPC_MONGO_PORT}")

    ###

    initBucket(INPUT_BUCKET_NAME)
    initBucket(OUTPUT_BUCKET_NAME)

    conn = pymongo.MongoClient(host=_GPC_MONGO_HOST, port=_GPC_MONGO_PORT)
    dbNames = conn.list_database_names()    

    if "gridrunner" not in dbNames:
        print(f"Gridrunner DB does not exist. Bootstrapping database")
        db = conn["gridrunner"]
        db.users.insert_one({
            "username" : "gridadmin",
            "password" : bcryptPassword( "foobar" ),
            "jobs" : [],
            "roles" : [ "admin" ],
            "email" : "user@example.com",
            "lastPwChange" : datetime.datetime.utcnow()
        })
        #create temp session to create the collection
        db.sessions.insert_one({
            "username" : "tempsession",
            "createdAt" : datetime.datetime.utcnow(),
            "lastAction" : datetime.datetime.utcnow()
        })
        
        db.sessions.delete_many({
            "username" : "tempsession"
        })

        #create index for username
        db.users.create_index("username", unique=True)

        #create index for _id
        db.sessions.create_index("_id")

        ### create auto expire session index
        db.sessions.create_index( "lastAction", expireAfterSeconds = SESSION_EXP_MINUTES * 60)
        #for queries by username
        db.sessions.create_indezx("username")

        

if __name__ == '__main__':    
    db_bootstrap()