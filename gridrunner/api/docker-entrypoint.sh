#!/usr/bin/env bash

set -x


cd /app

python3 db_bootstrap.py

uwsgi --ini ./app.ini
