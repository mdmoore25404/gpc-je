#!/usr/bin/env python3
import os, subprocess


def scanFile(f):
    """ returns True if file is OK, False if bad. f is assumed to be a full path """
    # ensure clamd scanner is running
    print(f"scanFile: {f}")
    
    if not os.path.exists(f):
        raise Exception(f"file {f} does not exist. can't scan it")
        return False        
   
    cp = subprocess.run(["clamdscan", "--fdpass", "-m",  f], stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)

    # print(f"CP is: {cp}")

    if cp.returncode == 0:
        return True # no viruses found

    elif cp.returncode == 1:
        print(f"Virus detected: {cp.stdout}")
        return False #viruses found
    
    elif cp.returncode == 2:
        print(f"Clamscan error message: {cp.stderr}\n{cp.stdout}")
        return False #some error happened
    
    else:
        raise Exception(f"Unexpected clamscan return code of {cp.returncode}")

    return False

###### 

scanfullpath = "/mnt/scratch/gpc-je-tarballer/da840476-a7ee-4d34-982c-655f56024908"

if scanFile(scanfullpath):
    print("scan ok")
else:
    print("scan not ok")
