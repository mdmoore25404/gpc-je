Default login banner.

This is the default login banner for gridrunner.

To login to gridrunner the default username/passowrd is: gridadmin/foobar

It is extremely important that you use that account to create another admin account and then delete the original account.

To cuzomize the login banner replace logon-banner.txt (this file) on the webserver.

If you are using docker you can replace with your own login banner via a docker volume mount to overwrite: /web/logon-banner.txt 

For example:

    volumes: 
      - ./gridrunner-ui/logon-banner.prod.txt:/web/logon-banner.txt

