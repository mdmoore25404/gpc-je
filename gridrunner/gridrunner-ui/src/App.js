
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
  
} from "react-router-dom";

import { Component } from 'react';
import axios from 'axios';

import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import DropdownButton from 'react-bootstrap/DropdownButton';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import ProgressBar from 'react-bootstrap/ProgressBar';
import FormControl from 'react-bootstrap/FormControl';
import Accordion from 'react-bootstrap/Accordion';

import Dropdown from 'react-bootstrap/Dropdown'

import moment from 'moment';


import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {  faUser, faSignOutAlt, faCloudDownloadAlt, faTrash, faCloudUploadAlt, faCheckCircle , faInfoCircle,  
          faExclamationTriangle, faPlayCircle, faUsers, faUserPlus, faCopy, faCheck } from '@fortawesome/free-solid-svg-icons';

import './App.css';
// import 'bootstrap/dist/css/bootstrap.min.css';


var isLoggedIn = false;
var usersSessionId;
var loggedInUsername ;
var roles = [];


////////////dev
const API_BASE = process.env.REACT_APP_API_BASE
const requiredPasswordLength = process.env.REACT_APP_PASSWORD_LEN;
//////////////

function runTimeAsHumanReadable(sec)
{
  var dur = moment.duration(sec, 'seconds')

  return dur.hours() + " hours, " + dur.minutes() + " minutes, " + dur.seconds() + " seconds. "
}


console.log("process.env: ")
console.dir(process.env);

function copyToClipboard(txt)
{
  navigator.clipboard.writeText(txt)
    .catch(err => {
      alert("error copying to clipboard: ", err);
    });
}

function requireRole(role)
{
  return roles.includes(role);
}

class Login extends Component
{
  constructor(props)
  {
    super();
    this.state = {
      username: "",
      password: "",
      submitting: false,
      logonBanner: ""
    }
  }


  passwordOnChange = (e) =>
  {
    this.setState({
      password: e.target.value
    });
  }

  usernameOnChange = (e) => {
    this.setState(
      {
        username: e.target.value
      }
    )
  }

  onKeyPress = (e) =>   {
    if (e.key === 'Enter')
      this.submit();
  }

  submit = () => {
    if (this.state.username !== "" && this.state.password !== "")
    {
      this.setState({ submitting: true })
      var formData = new FormData();
      formData.append("username", this.state.username);
      formData.append("password", this.state.password)

      axios(
        { 
          method: 'post',
          url: API_BASE +  'login',
          data: formData,
          headers: { "Content-Type": "multipart/form-data"}
        }
      ).then( (resp) => {
          // console.dir(resp);
          if (resp.status !== 200)
          {
            alert(resp.data);
          }
          else{
            this.props.onLogin(resp.data['sessionid'], this.state.username, resp.data['roles'])                        
          }


      }).catch( (error) => {
        // console.dir(error);
          if (error.response)          
            alert(error.response.data);          
          else if (error.request)          
            alert("no response or request didn't send")          
          else           
            alert("some kind of problem\n" + error);

          this.setState({submitting: false});
          
      });    
    }
  }

  componentDidMount() {
    axios.get("logon-banner.txt").then( (resp) => { 
      this.setState({ logonBanner: resp.data});
    })
    
  }

  componentWillUnmount() {
    this.setState({ logonBanner: "" });
  }

  render()
  {
    return (
      <div className='container'>
        
      <Card>
        <Card.Header>
          Login
        </Card.Header>
        <Card.Body>
        <Row>
        <Col className=''>
            <div className='login-banner'>
              <pre>
                {this.state.logonBanner}
              </pre>
              
            </div>
            </Col>
          </Row> 
          <Row>            
            <Col className='login-form'>
              <div>
                <Form.Control onKeyPress={this.onKeyPress} onChange={this.usernameOnChange} type="text" placeholder="Username" />
              </div>          
              <div>
                <Form.Control onKeyPress={this.onKeyPress} onChange={this.passwordOnChange} type="password" placeholder="password"></Form.Control>
              </div>
              <div>
                <Button disabled={this.state.submitting} onClick={this.submit}> {this.state.submitting ? "Working..." : "Login"} </Button>
              </div>
              </Col>
            
            </Row>   
        </Card.Body>
      </Card>
      
      </div>
    )
  }
}

class Job extends Component {
   
  downloadResults = () =>
  {
    //return alert(this.state.job.result.downloadUrl);
    console.log("Sending user to downlaod link at " + this.props.job.result.downloadUrl);
    window.open(this.props.job.result.downloadUrl);
  }

  deleteJob = () =>
  {
    
    if ( window.confirm("Are you sure you wish to delete this job? This cannot be undone"))
    {
        axios.delete(API_BASE + "job/" + this.props.job._id + "?sessionid=" +usersSessionId).then(( resp) => {
          this.props.onDelete();
        });
    }

    
  }


  calculateDuration()
  {
    var row = this.props.job;
    if (row.status === "running" && typeof(row.progress) !== "undefined" && row.progress > 0.0)
    {                              
      // solve for total time (x):  x = t / p 
      // where t is time so far
      // where p is percent completed (e.g. 0.34)
      var startTime = moment.utc( row.startTime);
      var progress = row.progress;                 
      var runTime = moment.utc().diff(startTime);       
//      var runTimeDuration = moment.duration(runTime);                       
      var total = runTime / progress;
      var remain = total - runTime;
  //    var totalDuration = moment.duration(total);
      var remainDuration = moment.duration(remain);                              
      return remainDuration.humanize();
    }
    else
    {
      return "N/A";
    }
}

  StatusInfo() {

  if (this.props.job.status === "running")
    return (
      <div>
      <span  className='margin-left'>Est. Completion: {this.calculateDuration()}</span>
      <div className='col-2 ilb'>
        <ProgressBar variant='info' key={"progress-" + this.props.job._id} now={ this.props.job.progress * 100} /> 
      </div>
      { Math.round(this.props.job.progress * 100) }%
      <span className='text-info margin-left'>Running <FontAwesomeIcon icon={faPlayCircle} /></span>
      </div>
    ) 
  
  else if (this.props.job.status === "completed") 
    return ( <span className='text-success'>Completed <FontAwesomeIcon icon={faCheckCircle} /></span>)

  else if (this.props.job.status === "pending")
  return ( <span>{this.props.job.jobsBefore} jobs before - <span className='text-secondary'>Pending <FontAwesomeIcon icon={faInfoCircle} /></span></span>)

  else if (this.props.job.status === "failed")
    return ( <span className='text-danger'>Failed <FontAwesomeIcon icon={faExclamationTriangle} /></span>)

  }

  render()
  {
    if (this.props.job === null  )
      return null;

    var histMap;

    if (this.props.job.history.length > 0)
    {
      histMap = this.props.job.history.map((item) => 
      {
        return (
          <Row key={"hist-" + this.props.job._id + "-" + item.timestamp}>
            <Col className='col-sm-4'>{item.timestamp}</Col>
            <Col className='col-sm'>{item.message}</Col>
          </Row>
        )
      });
    }
    else {
      histMap = "None"
    }
    

    return (

      <div key={this.props.job._id} className='container job-list'>
        <Accordion>
        <Card key={"card-" + this.props.job._id} >
          
        <Accordion.Toggle as={Card.Header} eventKey="0" className='width-100 clickable' > 
            <Row>

            <Col className=''>{this.props.job.friendlyName}</Col>
            <Col className='alr'>                                                                        
                {this.StatusInfo()}                
            </Col>

            
            </Row>

          </Accordion.Toggle>

          <Accordion.Collapse eventKey="0">
          <Card.Body>
            <Row>
              <Col className='col-sm-2 alr'>ID</Col>
              <Col>
              {this.props.job._id}
              <FontAwesomeIcon 
                icon={faCopy} 
                title='Click to copy to clipboard' 
                className='margin-left clickable' 
                onClick={() => { copyToClipboard(this.props.job._id) } } ></FontAwesomeIcon>
              </Col>
            </Row>
            <Row>
              <Col className='col-sm-2 alr'>Progress</Col>
              <Col className='col-sm-8'>
                <Row>
                  <Col className='col-sm-3'>
                    <ProgressBar variant='info' key={"progress-" + this.props.job._id} now={ this.props.job.progress * 100} />
                  </Col>
                  <Col className='col-lg-5' > 
                    { Math.round(this.props.job.progress * 100) }%
                  </Col>
                </Row>                
              </Col>
            </Row>
            <Row>
              <Col className='col-sm-2 alr'>Family</Col>
              <Col className='col-sm-5'>{this.props.job.jobFamily}</Col>
            </Row>
            <Row>
              <Col className='col-sm-2 alr'>Priority</Col>
              <Col className='col-sm-5'>{this.props.job.priority} </Col>
            </Row>
            <Row>
              <Col className='col-sm-2 alr'>Created</Col>
              <Col className='col-sm-5'>{this.props.job.createdOn} </Col>
            </Row>
            <Row>
              <Col className='col-sm-2 alr'>Started</Col>
              <Col className='col-sm-5'>{this.props.job.startTime} </Col>
            </Row>
            <Row>
              <Col className='col-sm-2 alr'>Finished</Col>
              <Col className='col-sm-5'>{this.props.job.endTime} </Col>
            </Row>
            <Row>
              <Col className='col-sm-2 alr'>Est. Completion</Col>
              <Col className='col-sm-5'> {this.calculateDuration()}</Col>
            </Row>
            
              <Row>
                <Col className="col-sm-2 alr">
                  History                  
                </Col>
                <Col>
                      <div>{histMap}</div>
                </Col>
              </Row>
              <Row>
                <Col className='col-sm-2 alr'>
                  Download Url
                </Col>
                <Col>                
                { this.props.job.result.downloadUrl === undefined ? ("pending") : (                  
                   <span>
                    { this.props.job.result.downloadUrl }
                  <FontAwesomeIcon 
                  icon={faCopy} 
                  title='Click to copy to clipboard' 
                  className='margin-left clickable' 
                  onClick={() => { copyToClipboard(this.props.job.result.downloadUrl) } } ></FontAwesomeIcon>
                  </span>
                  )}
                  
                </Col>
              </Row>
              <Row>
              <Col className='col-sm-2 alr'>Runtime</Col>
              <Col className='col-sm-5'> { 
                  this.props.job.runTimeSec !== null ? runTimeAsHumanReadable(this.props.job.runTimeSec) : ""
                }                 
              </Col>
            </Row>
              
            <Row className='margin-top'>
              <Col className='col-sm-2 alr'>Actions</Col>
              <Col>
                <Button key={"download-" + this.props.job._id} onClick={this.downloadResults} disabled={this.props.job.status !== "completed"} className='margin-right'><FontAwesomeIcon className='margin-right' icon={faCloudDownloadAlt} />Download Results</Button>
                
                <Button key={"delete-" + this.props.job._id} disabled={this.props.job.status === "running" } className='margin-right' onDelete={this.props.onDelete} onClick={this.deleteJob} variant='danger'><FontAwesomeIcon className='margin-right' icon={faTrash} />Delete</Button>

                

              </Col>
            </Row>
            </Card.Body>
            </Accordion.Collapse>
          
        </Card>
        </Accordion>
      </div>
    )
  }
}

class JobsList extends Component 
{
  constructor(props)
  {
    super();    

    this.state = 
    {
      jobs: null,
      refreshIntervalRef: null,
      refreshIntervalSec: null
    }
  }

  componentDidMount()
  {
    this.loadJobs();
    this.setRefreshInterval();
  }

  componentWillUnmount()
  {
    this.clearRefreshInterval();
    this.setState(
      {
        jobs: null
      }
    )
  }

  clearRefreshInterval = () =>
  {
    if (this.state.refreshIntervalRef !== null)
      {
        clearInterval(this.state.refreshIntervalRef);
        this.setState({ refreshIntervalRef: null});
      }
  }

  setRefreshInterval = () =>
  {
    if (this.state.refreshIntervalRef !== null)
    {
      this.clearRefreshInterval();
    }
    var refreshIntervalRef = setInterval( this.loadJobs.bind(this), 5 * 1000 );
    this.setState({ refreshIntervalSec: 5 * 1000, refreshIntervalRef : refreshIntervalRef } );
    
  }

  loadJobs()
  {    
    if (isLoggedIn && usersSessionId !== null )
    { 
      //get list of jobs
      axios.get(API_BASE + "myjobs?sessionid=" + usersSessionId).then( (resp) =>
      {          
        this.setState( { jobs: resp.data });
      }).catch( error =>
        {
          if (error.response.status === 400)
          {
            alert("invalid session. You must login again");
            logout();
          }
        })
    }
  }

  render() {
    if (isLoggedIn && usersSessionId !== null)    
    {
      //console.dir(this.state.jobs);
      if (this.state.jobs === null || this.state.jobs === undefined || (
        this.state.jobs instanceof Array && this.state.jobs.length === 0 //is a zero length array
        ))
      {        
        return (
          <div className='container margin-top'>
            <Card >
              <Card.Header>No Jobs Found</Card.Header>
              <Card.Body>
                You don't have any Gridrunner jobs at this time. 
                You can choose to submit one via the top-right menu button. Or you can <a href={process.env.PUBLIC_URL + "/submit"}>Click Here</a>
              </Card.Body>
            </Card>
          </div>
        )
      }
      else
      {
      var jobIter = this.state.jobs.map( (item) => { 
        return <Job key={item._id} job={item} onDelete={ () => { this.loadJobs(); } } ></Job>
      });

      return ( 
        jobIter
      )}
    }
    else
      return ( <Redirect to={process.env.REACT_APP_URL_BASE + '/login'}></Redirect>) 
  }
}
/*
class SelectedFile extends Component {
  
  render()
  {  
    const f = this.props.file;

    if (f === undefined)
      return "";

    return (
      <div>
        {f.type}
      </div>
    )
  }
}*/

class SubmitForm extends Component{
  constructor(props)
  {
    super(props)
    this.state = {
      file : null,
      name:  null,
      submitting: false
    }
  }

  fileChangeHandler = (event) =>
  {
    //validate file is ok

    var f = event.target.files[0];

    if (f.type !== "application/x-gzip" && f.type !== "application/gzip")
    {
      event.target.value = null;      
      this.setState(
        {
          file: null
        }
      );      
      alert("Invlaid file type. Must be gzip")
    }
    else
    {
      this.setState({
        file: event.target.files[0]
      });
      
    }
  }

  nameChangeHandler = (event) =>
  {
    const name = event.target.value;
    if (name !== "" && name !== undefined && name !== null)
    {
      this.setState({ name: name });
    }
    else 
    {
      this.setState({ name: null });
    }
  }

  doSubmit = () =>
  {
    if (this.state.name == null || this.state.file == null || this.state.submitting)
    {
      return
    }
    else
    {
        this.setState( { submitting: true}); 
        const formData = new FormData();
        formData.append("file",this.state.file, this.state.file.name)
        formData.append("name", this.state.name);

        axios(
          { 
            method: 'post',
            url: API_BASE +  "submitUpload?sessionid=" + usersSessionId,
            data: formData,
            headers: { "Content-Type": "multipart/form-data"}
          }).then( (resp) => {            
            if (resp.status !== 201)
            {
              alert("Error with submitting: " + resp.response)
            }
            else
            {
              this.setState({submitting: false});
              document.location = process.env.REACT_APP_URL_BASE + "/";
            }

          });
    }
  }

  render() 
  {
    if (!isLoggedIn || usersSessionId === null)        
      return ( <Redirect to={process.env.REACT_APP_URL_BASE + '/login'}></Redirect>) ;
////////
    return (
      <div className='container'>
        <Card>
          <Card.Header>Submit PSG Job</Card.Header>
          <Card.Body>
            <Row>
              <Col className='col-md-2'>Job Name</Col>
              <Col className='col-md-4'>
                <FormControl type="text" placeholder="name" onChange={this.nameChangeHandler}
                />
              </Col>
            </Row>
            <Row>
              <Col className='col-md-2'>Tarball</Col>
              <Col className='col-md-4'>                
                <Row>
                  <FormControl type="file" accept="*.tar.gz,application/x-gzip" onChange={this.fileChangeHandler} />                
                </Row>
              </Col>
            </Row>
            <Row>
              <Col className='col-md-2'>File Requirements</Col>
              <Col className='col-md-8'>                
                  <ul>
                    <li>The uploaded file must be a g-zipped tarball (.tar.gz) [INFO ON HOW TO CREATE THIS]</li>
                    <li>
                      The tarball and content files will be virus scanned. 
                      Any positive results will be investigated and reported to appropriate authorities.
                    </li>
                    <li>The cluster currently runs with 128 worker threads.</li>
                    <li>
                        Each sub-directory within the tarball is assigned to one of the above worker threads. 
                        For the most efficient utilization you should organize your submission into 128 sub-folders if possible.
                        If not your submission will take much longer to process.
                    </li>
                    <li>A submission with under {process.env.REACT_APP_SMALL_JOB_LIMIT} files is considered a <b>prototype</b> job for those who are
                      prototyping their grid. The prototype queue has fewer and unique workers compared to the production queue for submissions with over {process.env.REACT_APP_SMALL_JOB_LIMIT}
                    </li>
                  </ul>
              </Col>
            </Row>
            <Row>
              <Col className='col-md-2'>Results Retrieval</Col>
              <Col>
                <ul>
                  <li>
                    Your submission and it's progress will be tracked in this tool. 
                    Once the results are ready you will be able to click the Download Results button.
                  </li>
                  <li>
                    The results file format is a g-zipped tarball (.tar.gz) that will match your submission tarball's directory structure. 
                    Each PSG .cfg file in your submission will have the resulting PSG output.
                  </li>
                </ul>
              </Col>
            </Row>
            <Row>
              <Col className='col-md-2'>Submit</Col>
              <Col>
               <Button variant="primary" onClick={this.doSubmit} disabled={ this.state.submitting || !(this.state.file !== null && this.state.name !== null) }> 
                <FontAwesomeIcon className='margin-right' icon={faCloudUploadAlt} />  { this.state.submitting ? "Working..." : "Submit"} 
              </Button>
              </Col>
            </Row> 
          </Card.Body>
        </Card>
      </div>

    );
  }
}

/////

class User extends Component {


  deleteUser = () =>
  {
    if (! requireRole("admin"))
    {
        document.location = process.env.REACT_APP_URL_BASE
        return;
    }

    if (! window.confirm("Are you sure you wish to delete this user? This cannot be undone."))
      return;



        axios.delete(API_BASE + "user/" + this.props.user.username + "?sessionid=" + usersSessionId).then( (resp) => {       
            this.props.loadUsers();
      }).catch( error =>
        {
          if (error.response.status === 400)
          {
            alert("invalid session. You must login again");
            logout();
          }
        });
  }

  getPwChangeHumanize = () => {
    var startTime = moment.utc( this.props.user.lastPwChange);                
    var age = moment.duration( moment.utc() - startTime )
    return age.humanize()
  }

//<FontAwesomeIcon className='clickable text-danger' icon={faTrash} onClick={ this.deleteUser } /> -->

  userHasRole(role) {
    return this.props.user.roles.includes(role)
  }

  toggleUserRole(role)
  {
    var currentState = this.userHasRole(role);

    var newState = !currentState;

    const formData = new FormData();
    formData.append("role", role);
    formData.append("has", newState ? "true" : "false");

    axios(
      { 
        method: 'put',
        url: API_BASE +  "togglerole/" + this.props.user.username + "?sessionid=" + usersSessionId,
        data: formData,
        headers: { "Content-Type": "multipart/form-data"}
      }).then( (resp) => {            
        if (resp.status !== 204)
        {
          alert("Error with submitting: " + resp.response)
        }
        else
        {
          this.props.loadUsers();          
        }

      });

  }

  render()
  {
    var rolesIter;

    if (this.props.rolesList.length > 0) 
    {
      rolesIter = this.props.rolesList.map( (val, ix) => {
        return (<Dropdown.Item  disabled={this.props.user.username === loggedInUsername} key={this.props.user.username} title="Toggle role"   onClick={() => { this.toggleUserRole(val) } }  > 
          {val} { this.userHasRole(val) ? (<FontAwesomeIcon className='margin-left' icon={faCheck} />) : "" }
        </Dropdown.Item>)
    });
  }
  else {
    rolesIter = "";
  }
      

      return (
        <Row>
          <Col className='col-2'>
            <DropdownButton drop='right' className='thin-button' title="Actions">
              <Dropdown.Item disabled={this.props.user.username === loggedInUsername}  className=' text-danger' onClick={this.deleteUser} >
                <FontAwesomeIcon icon={faTrash} className='margin-right' />
                Delete
              </Dropdown.Item>
              <Dropdown.Header>Roles</Dropdown.Header>
              {rolesIter}
            </DropdownButton>
          </Col>
          <Col className='col-2'>{this.props.user.username}</Col>
          <Col className='col-2'>{this.props.user.email}</Col>
          <Col className='col-2'>{this.props.user.roles}</Col>
          <Col className='col-3'>{ this.getPwChangeHumanize() + " ago"}</Col>
        </Row>
      )

  }
}

class UserManagement extends Component {


  constructor(props)
  {
    super(props)

    this.state = 
    {
      oldpass: "",
      newpass: "",
      newpass2: "",
      submitting: false

    }
  }

  componentWillMount()
  {
    this.setState({ oldpass: "", newpass: "", newpass2: ""})
  }

  oldpassOnChange = (evt) => {
    this.setState({oldpass: evt.target.value})
  }

  oldPassOnChange = (evt) => {
    this.setState({oldpass: evt.target.value})
  }

  newPassOnChange = (evt) => {
    this.setState({newpass: evt.target.value})
  }

  newPass2OnChange = (evt) => {
    this.setState({ newpass2: evt.target.value})
  }


  submit = () => {

    if (this.state.olpass === "" || this.state.newpass === "" || this.state.newpass2 === "")
      return alert("All fields are required");

    if (this.state.newpass !== this.state.newpass2)
      return alert("Your new password didn't match, try again")

    if (this.state.newpass.length < requiredPasswordLength)
      return alert("Your new password must be at least " + requiredPasswordLength + " characters long.")
    
    this.setState({submitting: true});
    var formData = new FormData();
    formData.append("operation", "pwchange");
    formData.append("oldpass", this.state.oldpass);
    formData.append("newpass", this.state.newpass);

    axios(
      { 
        method: 'put',
        url: API_BASE +  'user/' + loggedInUsername + '?sessionid=' + usersSessionId ,
        data: formData,
        headers: { "Content-Type": "multipart/form-data"}
      }).then(resp => {
        if (resp.status === 200)
        {          
          alert("Password changed. You now need to login again");
          logout();
        }
        else
        {
          alert("unexpected result code: " + resp.status);
        }
      });
    
  }

  render()
  {
    return (
      <div className='container margin-top'>
      <Card>
        <Card.Header>Change Password</Card.Header>
        <Card.Body>
          <Row>
            <Col>Here you can change your passowrd. Your password must be at least {requiredPasswordLength} characters in length.</Col>
          </Row>
          <Row className='margin-top'>
            <Col className='alr col-2'>Current Password</Col>
            <Col className='col-3'>
              <FormControl type='password' onChange={this.oldPassOnChange} />
            </Col>
          </Row>
          <Row className='margin-top'>
            <Col className='alr col-2'>New Password</Col>            
            <Col className='col-3'>
              <FormControl type='password' onChange={this.newPassOnChange} />
            </Col>
          </Row>
          <Row className='margin-top'>
            <Col className='alr col-2'>Retype New Password</Col>
            <Col className='col-3'>
            <FormControl type='password' onChange={this.newPass2OnChange} />
            </Col>
          </Row>
          <Row className='margin-top'>
            <Col className='alr'>
              <Button variant='primary' onClick={this.submit} disabled={this.state.submitting} >  {this.state.submitting ? "Working..." : "Change Password" } </Button>
            </Col>
          </Row>
        </Card.Body>
      </Card>
      </div>
    )
  }
}

class UsersAdmin extends Component {
  
  constructor(props)
  {
    super(props)
    this.state = {
      users: [],
      username: "",
      password: "",
      email: "",
      submitting: false
    }
  }

  componentDidMount()
  {
      this.loadUsers();
  }

  loadUsers() {
    if (! requireRole("admin"))
        document.location = process.env.REACT_APP_URL_BASE

      axios.get(API_BASE + "roles").then( (resp) => {
        this.setState({ rolesList: resp.data});
      });

      axios.get(API_BASE + "users?sessionid=" + usersSessionId).then( (resp) => {       
            this.setState({ users: resp.data});
      }).catch( error =>
        {
          if (error.response.status === 400)
          {
            alert("invalid session. You must login again");
            logout();
          }
        });
  }

  componentWillUnmount()
  {
      this.setState( { users: [], username: null, password: null, email: null });
  }
  
  userAddUsernameChange = (evt) => {
    this.setState({ username: evt.target.value });
  }

  addPasswordChange = (evt) => {
    this.setState( { password: evt.target.value });
  }

  addEmailChange = (evt) =>
  {
    this.setState( { email : evt.target.value });
  }

  addUserSubmit = () => {

    if (this.state.email === null || this.state.email === "" || 
        this.state.username === null || this.state.user === "" ||
        this.state.password === null || this.state.password === "")
        {
          return alert("Invalid fields, check again");          
        }

    if (this.state.password.length < requiredPasswordLength)
        return alert("The minimum password length is " + requiredPasswordLength);

    this.setState({ submitting: true })
      var formData = new FormData();
      formData.append("username", this.state.username);
      formData.append("password", this.state.password)
      formData.append("email", this.state.email);

      axios(
        { 
          method: 'post',
          url: API_BASE +  'users?sessionid=' + usersSessionId ,
          data: formData,
          headers: { "Content-Type": "multipart/form-data"}
        }).then(resp => {
          if (resp.status === 201)
          {
            this.loadUsers();
          }
          else
          {
            alert("unexpected result code: " + resp.status);
          }

          this.setState( { submitting: false, username: "", email: "", password: ""})

      }).catch(error => {
            alert("Error on submit " + error);
            this.setState( { submitting: false, username: "", email: "", password: ""})
      });
  }

  deleteUser = (username) =>
  {

  }

  render() {


      if (this.state === undefined || this.state.users === [])
        return ""
        
      var userMap = this.state.users.map( (user, ix) => {
          return <User rolesList={this.state.rolesList} loadUsers= { () => { this.loadUsers(); }} key={user.username} user={user} />
      });

      return (

        

                    
            <Accordion className='container margin-top'>
                <Card>
                <Accordion.Toggle as={Card.Header} className='clickable' eventKey="0"> <FontAwesomeIcon icon={faUserPlus} className='margin-right' /> Add User </Accordion.Toggle>
                
                <Accordion.Collapse eventKey="0">
                  <Card.Body>
                    <Form>
                      <Row  className='margin-top'>
                          <Col className='col-2 alr strong'>Username</Col>
                        <Col>
                          <Form.Control  onChange={this.userAddUsernameChange} type="text" value={this.state.username}></Form.Control>
                        </Col>                        
                      </Row>
                      <Row  className='margin-top'>
                        <Col className='col-2 alr strong'>Email</Col>
                        <Col>
                          <Form.Control onChange={this.addEmailChange} type="email" value={this.state.email}></Form.Control>
                        </Col>
                      </Row>
                      <Row  className='margin-top'>
                        <Col className='col-2 alr strong'>Initial Password</Col>
                        <Col>
                          <Form.Control onChange={this.addPasswordChange} type='password' value={this.state.password}></Form.Control>
                        </Col>
                      </Row>
                      <Row className='margin-top'>
                        <Col className='alr'>
                          <Button onClick={this.addUserSubmit} disabled={this.state.submitting } variant='primary'> <FontAwesomeIcon className='margin-right' icon={faUserPlus}  />  
                          { this.state.submitting ? "Working..." : "Add User" }
                          </Button>
                        </Col>
                      </Row>
                    </Form>

                  </Card.Body>
                </Accordion.Collapse>
                </Card>
            
               
          <Card className='margin-top'>
          <Card.Header>Users</Card.Header>
          <Card.Body>
          
          <Row className='strong margin-top'>
            <Col className='col-2'>              
            </Col>
            <Col className='col-2'>Username</Col>
            <Col className='col-2'>Email</Col>            
            <Col className='col-2'>Roles</Col>
            <Col className='col-3'>Last PW Change</Col>
          </Row>
          {userMap}
          </Card.Body>
          </Card>
          
          </Accordion>
        
      )
  }
}

////

function onLogin(sessionid, username, roles) {  
  console.log("logged in with session id " + sessionid);  
  sessionStorage.setItem("sessionId", sessionid);  
  sessionStorage.setItem("loggedInUsername", username);
  sessionStorage.setItem("roles", roles);
  document.location = process.env.REACT_APP_URL_BASE + "/";
}

function logout()
{
  axios.delete(
    API_BASE + "logout/" + usersSessionId
  ).then( (resp) =>
  {
    usersSessionId = null;
    loggedInUsername = null;
    isLoggedIn = false;
    sessionStorage.removeItem("sessionId");
    sessionStorage.removeItem("loggedInUsername");
    document.location = process.env.REACT_APP_URL_BASE + "/";
    
  });
}

class Health extends Component {


  constructor(props)
  {
    super(props);
    this.state = { health: [] }
  }

  componentDidMount() {
    axios.get(API_BASE + "health").then( (resp) =>
    {
      this.setState({health: resp.data});
    });
  }

  getWorkerRow(worker)
  {
    var age = moment().utc().diff(  moment.utc( worker.updatedTime ));
    var ageDuration = moment.duration(age);

    var statusString = worker.status.split(" ")[0]

    return (
      <Row key={worker._id}>
        <Col className='col-2'> { worker.inSpec ?   ( <span className='text-success'>OK</span>)   :  (<span className='text-danger strong'>ALARM</span>) }   </Col>
        <Col className='col-2'>{worker.jobFamily}</Col>
        <Col className='col-2'>{worker.name}</Col>            
        <Col className='col-3'>{statusString}</Col>
        <Col className='col-3'>{ageDuration.humanize()}</Col>            
      </Row>
    );
  }

  getWorkerHeader() 
  {
      return (
          <Row className='strong'>
            <Col className='col-2'>Status</Col>
            <Col className='col-2'>Family</Col>
            <Col className='col-2'>Worker</Col>
            <Col className='col-3'>Last Operation</Col>
            <Col className='col-3'>Last Operation Age</Col>                  
          </Row>
      )
  }

  render()
  {

    if (this.state.health.length === 0)
      return "";
    
    var parentWorkers = this.state.health['parents'].map( (worker) =>
    {
      return this.getWorkerRow(worker);     
    });

    var childWorkers = this.state.health['children'].map( (worker) =>
    {
      return this.getWorkerRow(worker);
    });

    return (
      <div className='container'>
      <Card className='margin-top'> 
        <Card.Header>Cluster Health Status</Card.Header>
        <Card.Body>
            <Card className='margin-bottom'>
              <Card.Header>PSG Parent Workers</Card.Header>
              <Card.Body>
                {this.getWorkerHeader()}
                {parentWorkers}
              </Card.Body>
            </Card>
            <Card>
              <Card.Header>PSG Child Workers</Card.Header>
              <Card.Body>
                {this.getWorkerHeader()}
                {childWorkers}
              </Card.Body>
            </Card>
          
        </Card.Body>
      </Card>
      </div>
    )
  }
}

class HealthButton extends Component {
  constructor(props)
  {
    super(props);

    this.state = {
      health: null
    };
  }

  componentDidMount()
  {    
      axios.get(API_BASE + "health").then( (resp) => {        
        
        this.setState( { health: resp.data })
      });
  }

  healthClick()
  {
    document.location= process.env.REACT_APP_URL_BASE + "/health";
  }


  render() {
      if (this.state.health === null)
        return "";

      if (this.props.variant === "button")
        return (
          <Button onClick={this.healthClick}  variant={this.state.health.inSpec ? "success" : "danger"} >Cluster Status: {this.state.health.inSpec ? "Healthy" : "Unhealthy"}</Button>
        );
      else
        return (
          <span>
              Cluster Status: <a href={process.env.REACT_APP_URL_BASE + '/health'} className={this.state.health.inSpec ? "text-success" : "text-danger strong"} > {this.state.health.inSpec ? "Healthy" : "Unhealthy"} </a>
          </span>
          //<Button onClick={this.healthClick}  variant={this.state.health.inSpec ? "success" : "danger"} >Cluster Health: {this.state.health.inSpec ? "Healthy" : "Unhealthy"}</Button>
        );
  }
}


function App() {


  if (sessionStorage.length > 0 )
  {
    usersSessionId = sessionStorage.getItem("sessionId");
    loggedInUsername = sessionStorage.getItem("loggedInUsername");
    roles = sessionStorage.getItem("roles").split(",");  
  }

  if (usersSessionId !== undefined)
    isLoggedIn = true;

  var loggedInControls = ""

  var adminControls = "";

  if ( isLoggedIn && usersSessionId !== null && requireRole("admin"))
  {
    adminControls = (
      <div>
        <Dropdown.Item href={process.env.REACT_APP_URL_BASE + '/users'}> <FontAwesomeIcon className='margin-right' icon={faUsers} /> Users</Dropdown.Item>        
      </div>
    );
  }

  if (isLoggedIn && usersSessionId !== null)
  {
    loggedInControls = (
      <div>
        <Dropdown>
          <Dropdown.Toggle>
              <FontAwesomeIcon icon={faUser} className='margin-right' /> {loggedInUsername}  
          </Dropdown.Toggle>
          <Dropdown.Menu>                        
            <Dropdown.Item href={process.env.REACT_APP_URL_BASE + '/submit'}><FontAwesomeIcon className='margin-right' icon={faCloudUploadAlt} /> Submit Job</Dropdown.Item>
            <Dropdown.Item href={process.env.REACT_APP_URL_BASE + '/user'}><FontAwesomeIcon className='margin-right' icon={faUser} /> User Settings </Dropdown.Item>
            <Dropdown.Item onClick={logout}><FontAwesomeIcon className='margin-right' icon={faSignOutAlt} /> Logout</Dropdown.Item>

            {adminControls}
          </Dropdown.Menu>
        </Dropdown>
      </div>
    );
  }
  

  return (    
    <Router >            
      <div className="">
      <Navbar variant="dark" expand="lg">
        <Navbar.Brand variant='light' href="/">EMAC Grid Runner BETA</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />        
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">            
            <HealthButton />
          </Nav>                    
        {loggedInControls}          
      </Navbar.Collapse>
    </Navbar>
        <div>
          <Switch>
            <Route path={process.env.REACT_APP_URL_BASE + "/login"}>
              <Login onLogin={onLogin} />
            </Route>
            <Route path={process.env.REACT_APP_URL_BASE + "/submit"}>
              <SubmitForm></SubmitForm>
            </Route>
            <Route path={process.env.REACT_APP_URL_BASE + "/health"}>
              <Health></Health>
            </Route>            
            <Route path={process.env.REACT_APP_URL_BASE + "/users"}>
              <UsersAdmin></UsersAdmin>
            </Route>
            <Route path={process.env.REACT_APP_URL_BASE + '/user' }>
              <UserManagement></UserManagement>
            </Route>
            <Route path="">
              <JobsList></JobsList>
            </Route>
          </Switch>
            
        </div>
      </div>
    </Router>
  );
}

export default App;
