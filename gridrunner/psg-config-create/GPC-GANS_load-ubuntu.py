#!/usr/bin/env python3
## This is a python 3 implementation of GPC-JE to the GANS problem.

import sys
import json
import os, glob
import itertools
import numpy

#import threading, queue #multithread support
import multiprocessing, concurrent.futures

from CallPSGFunctions import *

# The purpose of this program is to define the parameter space, create configuration files, and submit GPC jobs to process those config files through PSG.

CPU_COUNT = os.cpu_count()
configCount = 0

print(f"Working with {CPU_COUNT} CPUs")

project = 'Multidirectory/' # unique for each run. This is a directory! Include a '/'!

# Creating all the directories required for computation.
if not os.path.exists('Cfgs/'+project):
    os.mkdir('Cfgs/'+project)

# Here we begin assigning parameter values.
directory = r'Cfgs' #define directory with planet profiles
planet_files = sorted(glob.glob(directory +'/*.txt'))
file = open(planet_files[0], 'r') #reads in the planet profile txt file
lines = file.readlines() #iterates through the file reading each line

matching_m = [s for s in lines if '<MASS>' in s] #searches for the Mass property line
matching_r = [s for s in lines if '<RADIUS>' in s] #searches for the Radius property line
matching_t = [s for s in lines if '<SURFACE-TEMPERATURE>' in s] #searches for the Temperature property line

start_m = float(matching_m[0].split('>')[1].split(',')[0])
stop_m = float(matching_m[0].split('>')[1].split(',')[1])
step_m = float(matching_m[0].split('>')[1].split(',')[-1].rstrip())

start_r = float(matching_r[0].split('>')[1].split(',')[0])
stop_r = float(matching_r[0].split('>')[1].split(',')[1])
step_r = float(matching_r[0].split('>')[1].split(',')[-1].rstrip())

start_t = float(matching_t[0].split('>')[1].split(',')[0])
stop_t = float(matching_t[0].split('>')[1].split(',')[1])
step_t = float(matching_t[0].split('>')[1].split(',')[-1].rstrip())

Mlist = np.linspace(start_m,stop_m,round((stop_m-start_m+step_m)/step_m)) #creates the list of Masses to iterate through
Rlist = np.linspace(start_r,stop_r,round((stop_r-start_r+step_r)/step_r)) #creates the list of Radii to iterate through
Tlist = np.linspace(start_t,stop_t,round((stop_t-start_t+step_t)/step_t)) #creates the list of Temperatures to iterate through


############ ORIGINAL VARIABLES
P0list = [0] #[ -1.0,-0.5, 0.0,0.5, 1.0,1.5, 2.0 ] # Surface pressure in log(bars)
Aslist = [0.05] #[ 0.05, 0.2, 0.35, 0.5 ]  # Surface grey albedo. Dimensionless

cloudstate = ['clouds',]#,'haze','clear'] # Name the cloud state. Include 'clouds' for clouds, 'clear' for clear sky, 'haze' for faux haze
rcldlist = [ 10 ] # Radius of cloud particulates in microns
lcldlist = [-3.0,-2.5,-2.0,-1.5,-1.0 ] # Linear cloud density in log(kg/m2)
Ptoplist = [ -3.0,-2.0,-1.5,-1.0,-0.5,0.0]#, 0.5, 1.0 ] # Pressure level of the top of the cloud in bars
fadelist = [ 0.1] # The rate at which the clouds fade outside of the "core" layers. Dimensionless.

gasNames = ['H2O']#'O3','CO','CO2','CH4','O2','SO2'] # names of gasses
gasTypes = ['HIT[1]']#'HIT[3]','HIT[5]','HIT[2]','HIT[6]','HIT[7]','HIT[9]'] # database to use for gas line features. one entry for each entry in gasNames
gasMixRat = [-7.0,-6.0, -5.0, -4.0, -3.0, -2.0, -1.0,-0.5, -0.1, -0.01 ] # log(mixing ratio)

H2mix = [ 0.0,0.2, 0.4, 0.6, 0.8,1.0 ] # The quantity H2/(H2+N2). Dimensionless.
#step = 0.125 #Increment desired stepping out from remainder of background gas for N2 and CO2

gasAbundanceList = []
#N2steps = np.arange(0,(1-H2mix[-1])+step,step)
#CO2steps = np.arange(0,(1-H2mix[-1])+step,step)[::-1]

#for i,j,k,l in itertools.product(range(len(gasNames)),range(len(gasMixRat)),range(len(H2mix)),range(len(N2steps))): # constructing appropriate mixing ratio assemblies
#    gasAbundanceList.append({'gas':gasNames[i],'gasType':gasTypes[i],'gasMix':10**gasMixRat[j],'H2Type':'HIT[45]','N2Type':'HIT[22]','CO2Type':'HIT[2]','H2Mix':(1-10**gasMixRat[j])*H2mix[k],'N2Mix':(1-10**gasMixRat[j])*(N2steps[l]),'CO2Mix':(1-10**gasMixRat[j])*(CO2steps[l])})

for i,j,k in itertools.product(range(len(gasNames)),range(len(gasMixRat)),range(len(H2mix))): # constructing appropriate mixing ratio assemblies
    gasAbundanceList.append({'gas':gasNames[i],'gasType':gasTypes[i],'gasMix':10**gasMixRat[j],'H2Type':'HIT[45]','N2Type':'HIT[22]','H2Mix':(1-10**gasMixRat[j])*H2mix[k],'N2Mix':(1-10**gasMixRat[j])*(1-H2mix[k])})

configList = []

print('Processing...')
print()

iterator = 0 # This is just a counter, to track how many loops we've been through.


threads = []
threadTarget = 6

badClouds = 0

############# THREAD WORKERS

def cloudsWorker(data):        
    (R,M,P0,As,atm,rcld,lcld,Ptop,fade,T) = data
    
    if Ptop >= P0: # catch impossible cloud locations            
        return

    g = 9.80665*M*R**(-2)

    subdirectory = 'R'+str(R) +'_g'+str(g) + '_T'+str(T) + '/'

    if not os.path.exists('Cfgs/'+project+subdirectory):
        os.mkdir('Cfgs/'+project+subdirectory)

    # construct the config file name
    filename = project # start with project name
    filename += subdirectory
    filename += atm['gas']+str(round(numpy.log10(atm['gasMix']),3))+'_H2'+str(round(atm['H2Mix']/(1-atm['gasMix']),3))+'_R'+str(R)+'_M'+str(M)+'_g'+str(g)+'_T'+str(T)+'_P'+str(P0)+'_A'+str(As) # add most parameter values
    filename += '_r'+str(rcld)+'_l'+str(lcld)+'_ct'+str(Ptop)+'_f'+str(fade)+'.cfg' # add cloud parameter values and filetype name

    # move pressure parameters out of log space
    P0 = round(10**P0,3)
    Ptop = round(10**Ptop,3)

    try: # running the configuration file maker, and adding the output to our future joblist
        MakeCFG(filename,[R,g,P0,As,atm,rcld,lcld,Ptop,clouds,fade,T])
    except BadClouds: # BadClouds should only come up from MakeCFG, if the cloud top pressure is too high (i.e. would produce underground clouds)                    
        return

def clearSkyWorker(data):
    (R,M,P0,As,atm,T) = data

    g = 9.80665*M*R**(-2)

    subdirectory = 'R'+str(R) +'_g'+str(g) + '_T'+str(T) + '/'

    if not os.path.exists('Cfgs/'+project+subdirectory):
        os.mkdir('Cfgs/'+project+subdirectory)

    # construct the config file name
    filename = project # start with project name
    filename += subdirectory
    filename += atm['gas']+str(round(numpy.log10(atm['gasMix']),3))+'_H2'+str(round(atm['H2Mix']/(1-atm['gasMix']),3))+'_R'+str(R)+'_M'+str(M)+'_g'+str(g)+'_T'+str(T)+'_P'+str(P0)+'_A'+str(As) # add the parameter values

    filename += '.cfg' # tack on the file type name

    P0 = round(10**P0,3) # move pressure out of log space
    # create the configuration file
    # sys.stdout.write(' '+str(iterator)+': '+MakeCFG(filename,[R,g,P0,As,atm,1,1,1,clouds,1,T])+'        ')
    MakeCFG(filename,[R,g,P0,As,atm,1,1,1,clouds,1,T])
    
def hazyWorker(data):
    (R,M,P0,As,atm,rcld,lcld,fade,T) = data

    g = 9.80665*M*R**(-2)

    subdirectory = 'R'+str(R) +'_g'+str(g) + '_T'+str(T) + '/'
    
    if not os.path.exists('Cfgs/'+project+subdirectory):
        os.mkdir('Cfgs/'+project+subdirectory)

    # construct the config file name
    filename = project # start with project name
    filename += subdirectory
    filename += atm['gas']+str(round(numpy.log10(atm['gasMix']),3))+'_H2'+str(round(atm['H2Mix']/(1-atm['gasMix']),3))+'_R'+str(R)+'_M'+str(M)+'_g'+str(g)+'_T'+str(T)+'_P'+str(P0)+'_A'+str(As) # add most parameter values
    filename += '_r'+str(rcld)+'_l'+str(lcld)+'_f'+str(fade) # add aerosol parameter values

    filename += '.cfg' # add filetype name

    # move pressure out of log space
    P0 = round(10**P0,3)
    # create the configuration file
    MakeCFG(filename,[R,g,P0,As,atm,rcld,lcld,1,clouds,fade,T])
    

#########################



# for i in range(len(Rlist)):
#     R = Rlist[i]
#     T = Tlist[i]
#     g = glist[i]

# Beginning creation of config files and GPC jobs.
# Handle cloudy atmospheres.
if ('clouds' in cloudstate):
    clouds = 'clouds'
    print('Starting clouds...')
    print()

    # loop over possible atmosphere configurations
    myList = list(itertools.product(Rlist,Mlist,P0list,Aslist,gasAbundanceList,rcldlist,lcldlist,Ptoplist,fadelist,Tlist))

    myLen = len(myList)
    print(f"Working on {myLen} possible configs")
    configCount += myLen
    
    executor = concurrent.futures.ProcessPoolExecutor(CPU_COUNT)
    futures = [executor.submit(cloudsWorker, item) for item in myList]
    concurrent.futures.wait(futures)

    print()
    print('Clouds finished!') # Status tracker
    print()



# Handle clear atmospheres.
if 'clear' in cloudstate:
    clouds = 'clear'
    print('Starting clearsky...')
    print()

    # loop over possible atmosphere configuraitons.
    myList = list(itertools.product(Rlist,Mlist,P0list,Aslist,gasAbundanceList,Tlist))
    
    myLen = len(myList)
    print(f"Working on {myLen} possible configs")
    configCount += myLen

    executor = concurrent.futures.ProcessPoolExecutor(CPU_COUNT)
    futures = [executor.submit(clearSkyWorker, item) for item in myList]
    concurrent.futures.wait(futures)

    print()
    print('clearsky finished!') # Status tracker
    print()

# Handle "hazy" atmospheres.
if 'haze' in cloudstate:
    clouds = 'haze'
    print('Starting haze...')
    print()

    # loop over possible atmosphere configurations
    myList = list(itertools.product(Rlist,Mlist,P0list,Aslist,gasAbundanceList,rcldlist,lcldlist,fadelist,Tlist))
    
    myLen = len(myList)
    print(f"Working on {myLen} possible configs")
    configCount += myLen

    executor = concurrent.futures.ProcessPoolExecutor(CPU_COUNT)
    futures = [executor.submit(hazyWorker, item) for item in myList]
    concurrent.futures.wait(futures)

    print()
    print('Haze finished!') # Status tracker
    print()

# Congrats, we've finished making our configuration files!
print()
print(f'Approx {configCount} Config Files Prepared! (less any with bad clouds)') # How many did we make?