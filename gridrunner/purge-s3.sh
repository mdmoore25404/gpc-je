#!/usr/bin/env bash

mc rm nuc1/psg-inputs/ --recursive --force

mc rm nuc1/psg-outputs/ --recursive --force