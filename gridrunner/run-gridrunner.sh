#!/usr/bin/env bash

set -x

cd ../

docker pull ubuntu:18.04

docker-compose -f docker-compose-gridrunner.yml pull

docker-compose -f docker-compose-gridrunner.yml up -d

docker-compose -f docker-compose-gridrunner.yml ps