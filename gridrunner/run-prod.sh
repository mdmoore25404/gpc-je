#!/usr/bin/env bash

set -x

if [ "$EUID" -ne 0 ]; then
    echo This must run as root
    exit 1
fi

docker-compose -f docker-compose.prod.yml up -d

