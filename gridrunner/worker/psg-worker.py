#!/usr/bin/env python3

import os
import uuid, tarfile, subprocess
import tempfile
import magic
import shutil, json, socket, threading, time, requests, smtplib, ssl
from stat import *
from minio import Minio
from minio.error import *
from pathlib import Path
                        
import python_client
import updatePsg

#### environment vars

RESULT_URL_STUB     = os.getenv("RESULT_URL_STUB", "http://nuc1:9000/")

MINIO_HOST          = os.getenv("MINIO_HOST", "localhost:9000")
MINIO_ACCESS_KEY    = os.getenv("MINIO_ACCESS_KEY", "example")
MINIO_SECRET_KEY    = os.getenv("MINIO_SECRET_KEY", "exampledontuse")
MINIO_SECURE        = os.getenv("MINIO_SECURE", False)

print(f"MINIO SECURE IS {MINIO_SECURE}")

if type(MINIO_SECURE) != bool:
    MINIO_SECURE = MINIO_SECURE.upper() == "TRUE"

INPUT_BUCKET_NAME   = os.getenv("INPUT_BUCKET_NAME", "psg-inputs")
OUTPUT_BUCKET_NAME  = os.getenv("OUT_BUCKET_NAME", "psg-outputs")
SCRATCH_PATH        = os.getenv("SCRATCH_PATH", "/mnt/scratch/gpc-je-tarballer") ### this must be the same path on job monitor and worker processes. simply mount the NFS to the same location

DIRECT_PATH         = os.getenv("DIRECT_PATH", "/mnt/scratch/gpc-je-tarballer")

JE_FAMILY_PARENT    = os.getenv("JE_FAMILY_PARENT", "psg-parent")
JE_FAMILY_CHILD     = os.getenv("JE_FAMILY_CHILD", "psg-child")
JE_HOST             = os.getenv("JE_HOST", "http://localhost:8008/")
JE_THREADS          = os.getenv("JE_THREADS", 5)

JE_THREADS          = int(JE_THREADS)

PSG_HOST            = os.getenv("PSG_HOST", "http://localhost:3000/")

WORKER_MODE         = os.getenv("WORKER_MODE", "PARENT")

EMAIL_HOST          = os.getenv("EMAIL_HOST", "localhost")
EMAIL_PORT          = os.getenv("EMAIL_PORT", 587)
EMAIL_FROM          = os.getenv("EMAIL_FROM", "")
EMAIL_USER          = os.getenv("EMAIL_USER", "")
EMAIL_PASS          = os.getenv("EMAIL_PASS")
EMAIL_ENABLED       = os.getenv("EMAIL_ENABLED", False)

BYPASS_AV           = os.getenv("BYPASS_AV", False)

MONITOR_DELAY_SEC   = os.getenv("MONITOR_DELAY_SEC", 5)

RM_CONFIG_SUCCESS   = os.getenv("RM_CONFIG_SUCCESS", True)

PSG_NOT_READY_SLEEP = os.getenv("PSG_NOT_READY_SLEEP", 60) # seconds

PSG_PACKAGES        = os.getenv("PSG_PACKAGES", "base,programs")

WORKER_NAME         = os.getenv("WORKER_NAME", socket.gethostname() )


if type(RM_CONFIG_SUCCESS) != bool:
    RM_CONFIG_SUCCESS = RM_CONFIG_SUCCESS.upper() == "TRUE"

if type(BYPASS_AV) != bool:
    BYPASS_AV = BYPASS_AV.upper() == "TRUE"

if type(EMAIL_ENABLED) != bool:
    EMAIL_ENABLED = EMAIL_ENABLED.upper() == "TRUE"

######

DEBUG = False

TEXT_MIMES = ["text/plain"] ##acceptable MIME types

minioClient = Minio(MINIO_HOST,
                    access_key = MINIO_ACCESS_KEY, 
                    secret_key = MINIO_SECRET_KEY, 
                    secure=MINIO_SECURE)

######## Methods
def initBucket(n):
    if not minioClient.bucket_exists(n):
        try:
            minioClient.make_bucket(n)
        except BucketAlreadyOwnedByYou as err:
            pass
        except BucketAlreadyExists as err:
            pass
        except ResponseError as err:
            raise

def putObject(uid, filepath):
    """ wrapper for minio client. stores `filepath` as {uid}-.tar.gz """
    if(os.path.exists(filepath)):
        return minioClient.fput_object(INPUT_BUCKET_NAME, f"{uid}.tar.gz", filepath, content_type="application/gzip")

def getObject(oname, td):
    print(f"getObject {oname} {td} from {INPUT_BUCKET_NAME}")
    """ wrapper for minio client. gets {uid}.tar.gz and stores in directory td"""
    return minioClient.fget_object(INPUT_BUCKET_NAME, oname, os.path.join(td, oname ))

def deleteObject(oname):
    return minioClient.remove_object(INPUT_BUCKET_NAME, oname)

def scanFile(f):
    """ returns True if file is OK, False if bad. f is assumed to be a full path """
    # ensure clamd scanner is running
    print(f"scanFile: {f}")
    
    if not os.path.exists(f):
        raise Exception(f"file {f} does not exist. can't scan it")
        return False        
   
    cp = subprocess.run(["/usr/bin/clamdscan", "--fdpass", "-m",  f], stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    # print(f"CP is: {cp}")

    if cp.returncode == 0:
        return True # no viruses found

    elif cp.returncode == 1:
        print(f"Virus detected: {cp.stdout}")
        return False #viruses found
    
    elif cp.returncode == 2:
        print(f"Clamscan error message: {cp.stderr}\n{cp.stdout}")
        return False #some error happened
    
    else:
        raise Exception(f"Unexpected clamscan return code of {cp.returncode}")

    return False

########

def handleParentJob(j, jec):
    print(f"In handleParentJob")
    payload = j['payload']
    friendlyName = j['friendlyName']
    _id = j['_id']
    uid = payload['uid']
    tarballName = payload['tarballName']

    mytarball = os.path.join(DIRECT_PATH, tarballName)
    extractedPath = os.path.join(SCRATCH_PATH, f"{uid}")   
    resultsPath = os.path.join(SCRATCH_PATH, f"results-{uid}")

    try:
        jobsList = []
        m = magic.Magic(mime=True)
        print( "Downloading tarball from s3")
        jec.addLog(_id, "Downloading tarball from s3")
        getObject(tarballName, DIRECT_PATH)

        if not os.path.exists(mytarball):
            print(f"{mytarball} doesn't exist after getObject. panic")
            jec.failJob(_id, f"Tarball couldn't be downloaded from bucket")
       
        print(f"Downloaded tarball to {mytarball}")
        # jec.addLog(_id, "Scanning tarball with clamav")00000
        # scanFileResult = scanFile(mytarball)
    
        with tarfile.open(mytarball) as myTarfile:

            if os.path.exists(extractedPath):
                jec.addLog(_id, f"tarball extract path already exists, skipping extract. Assuming development is underway")
                print(f"tarball extract path already exists, skipping extract. Assuming development is underway")
            else:   
                print(f"Extracting tarball to {extractedPath}")                        
                jec.addLog(_id, f"Extracting tarball to {extractedPath}")
                
                myTarfile.extractall(path=extractedPath)

                print("extract stat: ")
                print(os.stat(extractedPath))
                print("path exists test")
                print(os.path.exists(extractedPath))                

                if not os.path.exists(extractedPath):
                    jec.addLog(_id, "Failed to extract tarball")                
                    raise Exception("failed to extract tarball")
                else:
                    print("Tarball extracted")
                    jec.addLog(_id, f"removing tarball {mytarball}")
                    if os.path.exists(mytarball):
                        os.remove(mytarball)

                
            if not BYPASS_AV:
                 jec.addLog(_id, f"Antivirus scanning extracted tarball")

            if not BYPASS_AV and not scanFile(extractedPath):
                jec.addLog(_id, f"Extracted tarball failed virus scan")
                jec.failJob(_id, "virus scan fail")
                shutil.rmtree(extractedPath)
            else:            
                # iterate files to ensure flat text

                jec.addLog(_id, "creating results path")         
                if not os.path.exists(resultsPath):                      
                    os.mkdir(resultsPath)
                else:
                    jec.addLog(_id, "results path already exists. This may be unexpected")

                print(f"Investigating files to ensure plain text. Accepted mime types: {TEXT_MIMES}")
                jec.addLog(_id, "Investigating individual files and building queue")

                chunkCounter = 0
                
                for root, dirs, files in os.walk(extractedPath):
                    baseName = os.path.basename(root)                       
                    fileCount = len(files)
                    dirCount = len(dirs)

                    print(f"Looking at basepath: {baseName}")          
                    print(f"There are {fileCount} files to handle and {dirCount} dirs to handle in {baseName}")          

                    if fileCount == 0:
                        jec.addLog(_id, f"This directory has no files in it, though it may be expected: {root}")
                        if not os.path.exists(root):
                            os.mkdir(root)
                    
                    cfgList = []
                    
                    for f in files:
                        
                        fullf = os.path.join(root, f)
                        # nfsPath = os.path.join(SCRATCH_PATH, uid, "input", f) 
                        
                        kind = m.from_file(fullf)    
                        # print(f"Basename is {baseName} and root is {root}")
                        # print(f"Investigating file {baseName}/{f} as {kind}")                        
                        if kind in TEXT_MIMES:
                            cfgList.append(                                    
                                    fullf,  
                                )    
                        else:
                            jec.addLog(_id, f"invalid mime value of {kind}. deleting '{f}' from shared storage")
                            os.remove(fullf)                        
                    
                    payload = {
                        "jobType": "child",
                        "flags" : payload['flags'],
                        "cfgList": cfgList,
                        "baseName" : baseName
                    }

                    #write to file so we can work with it
                    if len(cfgList) > 0:            
                        chunkCounter = chunkCounter + 1            
                         #jec.addJob(json.dumps(payload), f"IsPrime {i}", jobFamily='primes')
                        jec.addJob(json.dumps(payload), friendlyName = f"{friendlyName} child {chunkCounter}", jobFamily = JE_FAMILY_CHILD, parentId = uid )                        
                        print("jobset sent to job engine api")
                        
                
                ## here we will start monitoring the progress of the child jobs
                jec.addLog(_id, "Starting loop to monitor children")
                childrenWorking = True
                while childrenWorking:
                    c = jec.getCount(JE_FAMILY_CHILD, uid)                
                    if c['pending'] > 0 or c['running'] > 0:
                        #still jobs pending or running
                        # progress = (c['completed'] + c['failed']) / c['total']

                        myChildren = list(jec.getChildren(uid, JE_FAMILY_CHILD))
                        # print(f"My children: {myChildren}")

                        trackableChildren = 0
                        progressCountup = 0.0
                        for child in myChildren:
                            if child['status'] == 'failed':
                                trackableChildren += 1
                                progressCountup += 1
                            else: # completed pending running statuses here
                                trackableChildren += 1
                                progressCountup += child['progress']    
                        
                        progress = 0.0

                        if trackableChildren > 0.0 :
                            progress = progressCountup / trackableChildren

                        print(f"parent progress check. {progress} = {progressCountup} / {trackableChildren}", flush=True)



                        jec.progressJob(_id, progress)                    
                        time.sleep(MONITOR_DELAY_SEC)
                    else:
                        childrenWorking = False                    

                jec.addLog(_id, "Creating results tarball")    

                c = jec.getCount(JE_FAMILY_CHILD, uid)   
                reportString = (
                    f"PSG on GPC Job Engine Report: \n"
                    f"For this submission we processed a total of {c['total']} jobs/folders. Of those jobs/folders "
                    f"{c['completed']} completed successfully and {c['failed']} encountered error. \n\n"                
                    f"Successful results reside in the <psg_config_file_name>.txt files. \n"
                    f"Failed job error messages reside in the <psg_config_file_name>.ERROR files\n"
                )

                with open( os.path.join(resultsPath, "_GPC_REPORT.txt"), "w") as f:
                    f.write(reportString)


                filepath = os.path.join(resultsPath, ".tar.gz")
                with tarfile.open(filepath, "w:gz") as tar:
                    tar.add(resultsPath, arcname=os.path.basename("results"))                

                jec.addLog(_id, "Storing results tarball in s3 bucket")
                resultsTarFileName = f"results-{uid}.tar.gz"
                minioClient.fput_object(OUTPUT_BUCKET_NAME, resultsTarFileName , 
                    filepath, content_type="application/gzip")

                jec.addLog(_id, "Deleting input tarball from bucket")
                deleteObject(tarballName)

                resultDict = {
                    "downloadUrl" : f"{RESULT_URL_STUB}{OUTPUT_BUCKET_NAME}/{resultsTarFileName}",
                    "tally" : c
                }

                ######## email notification

                if  EMAIL_ENABLED and payload['notifyEmail'] != "":
                    em = payload['notifyEmail']
                    message = f"""From: PSG on EMAC <{EMAIL_FROM}>
To: {em}
Subject: PSG on EMAC Submission '{j['friendlyName']}' finished!


Your submission to PSG on EMAC has finished. You may download the results tarball at: {resultDict['downloadUrl']}

The download URL will expire in 14 days.

"""

                    try:
                        jec.addLog(_id, f"Notify email is provided and email is enabled. Attempting to send email to {em}")
                        context = ssl.SSLContext(ssl.PROTOCOL_TLS)
                        connection = smtplib.SMTP(EMAIL_HOST, EMAIL_PORT)
                        connection.ehlo()
                        connection.starttls(context=context)
                        connection.ehlo()
                        connection.login(EMAIL_USER, EMAIL_PASS)
                        connection.sendmail(EMAIL_FROM, em, message)
                        jec.addLog(_id, f"Email sent.")

                    except smtplib.SMTPException as e:
                        jec.addLog(_id, f"Unable to send email notification: {e}")

                #########

                jec.completeJob(_id, json.dumps(resultDict))

                ##### remove completed child jobs (retain errors for investigation)
                jec.deleteJobsWithFilters( { "status" : "completed", "parentId" : uid, "family" : JE_FAMILY_CHILD } )




    except Exception as e:
        jec.failJob(_id, f"Exception in handling: {e}")        
    finally: #final cleanup of scratch if there were exceptions
        pass
        jec.addLog(_id, f"Purging scratch dir for uid {uid}")        
        if os.path.exists(resultsPath):
            shutil.rmtree(resultsPath)
        if os.path.exists(mytarball):
            os.remove(mytarball)
        if os.path.exists(extractedPath):
           shutil.rmtree(extractedPath)

    print("At end of handleParentJob")
        
########### end handleParentJob


def isPsgReady():    
    try:
        result = requests.get(PSG_HOST)        
        if result.status_code == 200:                        
            return True            
        else:
            print(f"PSG not online: {result.status_code} {result.text}")
            return False
    except Exception as e:
        print(f"PSG not online. Error was exception. {e}")
        return False             
    

def handleChildJob(j, jec):
    print(f"handling child job")
    _id = j['_id']
    
    if not isPsgReady():
        #PSG not ready        
        print("System thinks PSG is not ready. Returning job")
        jec.returnJob(_id)
        time.sleep(PSG_NOT_READY_SLEEP)
    else:
        payload = j['payload']
        uid = j['parentId']
        url = f"{PSG_HOST}api.php"
        # cfg = payload['filepath']
        baseName = payload['baseName']

        print(f"Basename for job {_id} is {baseName}")

        cfgList = payload['cfgList']
        try:
            totalCfgs = len(cfgList)
            counter = 0
            lastBigProgress = 0
            for cfg in cfgList:            
                cfgFileName = os.path.basename(cfg)  
                  
                # print(f"config dir is {cfgDir}")

                cfgDir = Path(cfg)
                resultsPathParts = Path(* cfgDir.parent.parts[5:] )

                print(f"result path parts is  {resultsPathParts}")
                # print(f"path parent: {cfgDir.parent}")
                # print(f"path name: {cfgDir.name}")


                resultsPath = os.path.join(SCRATCH_PATH, f"results-{uid}")
                print(f"ResultsPath is {resultsPath}")
                try:
                    if not os.path.exists( os.path.join(resultsPath, resultsPathParts)):
                        print( f"creating sub results folder {os.path.join(resultsPath, resultsPathParts)}")
                        jec.addLog(_id, f"creating sub results folder {os.path.join(resultsPath, resultsPathParts)}")
                        Path(os.path.join(resultsPath, resultsPathParts)).mkdir(parents=True, exist_ok=True) 
                except Exception as e:
                    jec.addLog(_id, f"Exception creating results dir! {e}")
                    jec.failJob(_id, f"Failed to create results dir. see log")
                    return

                s = ""
                data = payload['flags']
                with open(cfg, "r") as f:
                    s = f.read()                
                    data['file'] = s

                
                r = requests.post(url, data = data)  
                if r.status_code == 200:                    
                    if len(r.text) > 100:                    
                        # print(f"resultsPath = {resultsPath}")         
                        print(f"os.path.join({resultsPath}, {resultsPathParts}, {cfgFileName}.txt)")               
                        with open(os.path.join(resultsPath, resultsPathParts, f"{cfgFileName}.txt"), "w") as f:
                            f.write(r.text)

                        #remove config if desired
                        if RM_CONFIG_SUCCESS:
                            os.remove(cfg)

                        counter = counter + 1
                        progress = counter / totalCfgs
                        progressInt = int(progress * 100)
                        # only update when whole number updates e.g. 1% 2% 3% to reduce hammering API with tiny updates
                        if progressInt > lastBigProgress:
                            lastBigProgress = progressInt
                            jec.progressJob(_id, progress)                        
                    else:
                        # jec.failJob(_id,f"The result from PSG was less than 100 characters. {r.text}")
                        with open(os.path.join(resultsPath, resultsPathParts, f"{cfgFileName}.ERROR"), "w") as f:
                            f.write(f"Result from PSG < 100 characters. Output below:\n\n{r.text}\n\n")
                else:
                    # jec.failJob(_id, f"When sending job to PSG did not get a 200 response. {r.status_code} - {r.text}")            
                    with open(os.path.join(resultsPath, resultsPathParts,  f"{cfgFileName}.ERROR"), "w") as f:
                            f.write(f"PSG response code was not 200. {r.status_code} {r.text}\n")


            jec.completeJob(_id, f"Completed {len(cfgList)} inner jobs")

        except Exception as e:
            jec.failJob(_id, f"Exception handling child job: {e}")

def jobWorker(j, jec):
    print(f"picked up job: {j['_id']}")
    payload = j['payload']
    if payload['jobType'] == "parent":
        handleParentJob(j, jec)
    elif payload['jobType'] == "child":
        handleChildJob(j, jec)
    else:
        raise Exception("Invalid jobType")



def spawnme(workerid, family):
    jec = python_client.GpcJobEngineClient(JE_HOST, jobWorker, family=family, terminateOnNoJobs=False, workerId=workerid, noJobsSleep=1, DEBUG=DEBUG)
    jec.start()


def main():
    if WORKER_MODE == "CHILD":
        print("Starting daemon in child mode")        
        ## checking for working local PSG
        checking = True
        while checking:
            try:
                print(f"Attempting to work with PSG host: {PSG_HOST}", flush=True)
                result = requests.get(PSG_HOST, headers= { 'User-Agent':  "curl/7.64.1"} ) #emulate curl so PSG returns brief text output
                if result.status_code == 200:
                    print(f"Local PSG is online!", flush=True)
                    print("PSG package info: ", flush=True)
                    print(result.text)
                    checking = False
                else:
                    print(f"PSG not online: {result.status_code} {result.text}", flush=True)
                    time.sleep(5)
            except Exception as e:
                print(f"PSG not online. Error was exception. {e}", flush=True)
                time.sleep(5)

        doUpdate            = os.getenv("PSG_DO_UPDATE", "True") == "True"

        status = False
        while not status and doUpdate == True:
            status = updatePsg.updatePsg(PSG_PACKAGES=PSG_PACKAGES, doUpdate = doUpdate)        
            if not status:
                print("PSG packages not updated yet", flush=True)
                time.sleep(30)
        print("Done PSG package update process", flush=True)

        print(f"JE_HOST={JE_HOST}")

        workers = []
        for i in range(JE_THREADS):
            t = threading.Thread(target=spawnme(WORKER_NAME + "-" + str(i), JE_FAMILY_CHILD))
            workers.append(t)
        
        for w in workers:
            w.start()

        for w in workers:
            w.join()
    
        print(f"spawned {JE_THREADS} threads")
    elif WORKER_MODE == "PARENT":

        print("starting daemon in parent mode")        
        ############ initialize the two buckets
        initBucket(INPUT_BUCKET_NAME)
        initBucket(OUTPUT_BUCKET_NAME)
        ############# 
        t = threading.Thread(target=spawnme(WORKER_NAME, JE_FAMILY_PARENT))
        t.start()
        t.join()
    else:
        print("unknown worker mode")
        exit(1)

if __name__ == "__main__":
    print(f"before main(). worker mode is {WORKER_MODE}")
    main()

