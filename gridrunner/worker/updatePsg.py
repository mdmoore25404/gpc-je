#!/usr/bin/env python3
###

import os
import requests
import re
import time

PSG_HOST            = os.getenv("PSG_HOST", "http://localhost:3000/")


def getPackagesInstalled(result_text):
    print("parseInstalledPackages reviewing:")
    print(result_text)
    packageDetails = list()
    p = re.compile('^([A-Z]+)\ -\ (Update available|Using latest version).*$', re.MULTILINE)
    m = p.findall(result_text)    
    for g in m:        
        (package, status) = g
        package = package.lower()
        if status == "Update available":
            packageDetails.append( (package, True ))
        else:
            packageDetails.append( (package, False ) )
    
    print(f"Packages installed and if need update: {packageDetails}", flush=True)
    return packageDetails


def isPackageInList(package, packageList):
    print(f"searching for {package} in {packageList}")
    for (p, installUpdate) in packageList:
        if package == p:
            return True    
    return False

def findMissingOutdatedPackages(packageList): #packagelist should be a list of packages e.g. ['programs', 'base']
    headers= { 'User-Agent':  "curl/7.64.1"}

    r = requests.get(PSG_HOST, headers = headers )    
    if r.status_code == 200:        
        packagesToUpdate = getPackagesInstalled(r.text) #list of tuples (package, needInstallUpdate)
        for p in packageList:
            if not isPackageInList(p, packagesToUpdate):
                print(f"Adding {p} to packagesToUpdate list")                
                packagesToUpdate.append(  (p, True) )                    
        return packagesToUpdate        
    else:
        raise Exception(f"unexpected status code from PSG: {r.status_code}")

def updatePsg(PSG_PACKAGES, doUpdate = True):
    if PSG_PACKAGES != "" and PSG_PACKAGES != None:
        packages = PSG_PACKAGES.split(',')
        for p in packages:
            p = p.lower()

        print(f"User requested package list: {packages}. doUpdate is {doUpdate}")    
        packages = findMissingOutdatedPackages(packages)

        packagesToUpdate = []
        for (p, installUpdate) in packages:
            if installUpdate:
                packagesToUpdate.append( (p, installUpdate) )

        print(f"Need to update packages: {packagesToUpdate}")

        if doUpdate:
            if len(packagesToUpdate) > 0: #any to update                
                for (package, installUpdate) in packagesToUpdate:
                    if installUpdate:                 
                        print(f"Attempting to install/updatae PSG package: {package}")
                        installUrl = f"{PSG_HOST}?install={package}"
                        print(f"Install url: {installUrl}", flush=True)
                        resultInstall = requests.get( installUrl, headers= { 'User-Agent':  "curl/7.64.1"} ) #emulate curl so PSG returns brief text output            
                        if resultInstall.status_code == 200:
                            print(f"Installed package! PSG output: \n{resultInstall.text}", flush=True)
                        else:
                            print(f"Unable to install package {package}. HTTP error code: {resultInstall.status_code}. Message text: {resultInstall.text}")
                            return False # couldn't install package so return false            
            return True #packages were updated                
        else: #don't do update            
            return len(packagesToUpdate) == 0 #client isn't wanting to update but would like to know if packages are up to date.
            
            

if __name__ == "__main__":
    PSG_PACKAGES        = os.getenv("PSG_PACKAGES", "base,programs")
    doUpdate            = os.getenv("PSG_DO_UPDATE", "True") == "True"

    status = False
    while not status:
        status = updatePsg(PSG_PACKAGES=PSG_PACKAGES, doUpdate = doUpdate)        
        if not status:
            print("PSG packages not updated yet", flush=True)
            time.sleep(30)
    print("Done PSG package update process", flush=True)
   
