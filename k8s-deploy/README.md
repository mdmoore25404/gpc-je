# Examples to deploy GPC-JE API on kubernetes.

*Note: these examples were tested on Ubuntu 18.04 with [MicroK8s](https://microk8s.io/)*

This example exposes port `30000` to open the webapp and API in `nginx-service.yaml` on the single `microk8s` node. For production k8s you'd likely have multiple nodes and thus need to configure [k8s ingress](https://kubernetes.io/docs/concepts/services-networking/ingress/)

## see what is running 

    root@nuc1:/home/michaelmoore/projects/gpc-je/k8s-deploy# kubectl get all
    NAME                         READY   STATUS    RESTARTS   AGE
    pod/flask-6fd4f5f577-gxqbt   0/1     Pending   0          3s
    pod/flask-6fd4f5f577-xvmg9   0/1     Pending   0          3s
    pod/mongo-546555bdb9-sncg6   0/1     Pending   0          3s
    pod/nginx-565f8b4bd7-6lj7x   0/1     Pending   0          3s
    pod/nginx-565f8b4bd7-jb68l   0/1     Pending   0          3s

    NAME                 TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)           AGE
    service/flask        ClusterIP   10.152.183.184   <none>        8007/TCP          3s
    service/kubernetes   ClusterIP   10.152.183.1     <none>        443/TCP           24d
    service/mongo        ClusterIP   10.152.183.26    <none>        27017/TCP         3s
    service/nginx        NodePort    10.152.183.56    <none>        30000:30000/TCP   3s

    NAME                    READY   UP-TO-DATE   AVAILABLE   AGE
    deployment.apps/flask   0/2     2            0           3s
    deployment.apps/mongo   0/1     1            0           3s
    deployment.apps/nginx   0/2     2            0           3s

    NAME                               DESIRED   CURRENT   READY   AGE
    replicaset.apps/flask-6fd4f5f577   2         2         0       3s
    replicaset.apps/mongo-546555bdb9   1         1         0       3s
    replicaset.apps/nginx-565f8b4bd7   2         2         0       3s
    

## Deploy sample deployments and services

The examples include one replica for the mongo container and 2 each for flask and nginx.
The example will provide an nginx service on your `microk8s` node on port 30000.

*Note: The example below assumes your current working directory is `k8s-deploy` (where this README is located). If not specify the full or relative path to the `.yaml` files.*

    kubectl apply -f .

## Remove sample deployments

*Note: The example below assumes your current working directory is `k8s-deploy` (where this README is located). If not specify the full or relative path to the `.yaml` files.*

    kubectl delete -f .



