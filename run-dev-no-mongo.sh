#!/usr/bin/env bash

set -x

if [ "$EUID" -ne 0 ]; then
    echo This must run as root
    exit 1
fi

docker-compose -f docker-compose-no-mongo.yml down && docker-compose  -f docker-compose-no-mongo.yml build && docker-compose  -f docker-compose-no-mongo.yml up -d

