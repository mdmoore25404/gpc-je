#!/usr/bin/env bash

set -x

if [ "$EUID" -ne 0 ]; then
    echo This must run as root
    exit 1
fi

docker-compose down && docker-compose build && docker-compose up -d

