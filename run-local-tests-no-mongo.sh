#!/usr/bin/env bash

## script to run as root on dev environment. 


set -x
set -e

#export JE_HOST=http://localhost:8008/

if [ "$EUID" -ne 0 ]; then
    echo This must run as root
    exit 1
fi

./run-dev-no-mongo.sh
./build-test-runner.sh
./run-postman-tests.sh
./run-test-runner.sh cli
./run-test-runner.sh python

echo
echo
echo run-local-tests.sh completed.