#!/usr/bin/env bash

set -x
set -e

# run postman tests
docker run --net gpc-je_default --name testclient --link=flask:flask -v $(pwd)/build/tests/collections:/etc/newman --rm  -t postman/newman run gpc-je-test.postman_collection.json
