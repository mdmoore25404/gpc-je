#!/usr/bin/env bash

if [ "$EUID" -ne 0 ]; then
    echo This must run as root
    exit 1
fi

if [ -z "$TEST_NET" ]; then
    export TEST_NET=gpc-je_default
fi


if [ -z "$JE_HOST" ]; then
    export JE_HOST=http://nginx/
fi

if [ -z "$JE_TEST" ]; then
    export JE_TEST=$1
fi


docker run --rm  --net $TEST_NET --link=nginx:nginx -e "JE_TEST=$JE_TEST" -e "JE_HOST=$JE_HOST" -e "JE_DEBUG:true"  mdmoore25404/test-runner

