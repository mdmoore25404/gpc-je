#!/usr/bin/python3

import 
import time

import os

from PIL import Image
from resizeimage import resizeimage

#we're working with gigantic astronomy images so disable the decompression bomb error
Image.MAX_IMAGE_PIXELS = None 

def myCleanup():    
    je.stopProcessing()

def myWorker(di):
    print("I have the job: %s" % di)
    payload = di['payload']
    filepath = "/mnt/images/top100/" + payload['file']
    filename = payload['file']
    size = payload['size']

    with open(filepath, 'r+b') as fh:
        with Image.open(fh) as image:
            print("image.height %s, size %s" % (image.height, size))
            isValid = image.height > size

            if isValid:                
                resized = resizeimage.resize_height(image, size)
                newfile = "/mnt/images/" + str(size) + "-" + filename + ".png"
                resized.save(newfile)
                payload['newFile'] = newfile
            else:
                print("Resizing this file isn't valid")
                payload['error'] = "invalid resize"

    print("End of myWorker Call")

je = jobengine.GpcJobEngine(userWorker = myWorker, mongoHost= os.getenv('GPC_MONGO_HOST', 'localhost') ) 
je.start() #start worker
#time.sleep(60) # just simulate running
#je.end()      # and ending