import React, {Component} from 'react';
import cloneDeep from 'lodash/cloneDeep';
import axios from 'axios';
import './App.css';
import moment from 'moment';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Table from 'react-bootstrap/Table';
import Dropdown from 'react-bootstrap/Dropdown';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ProgressBar from 'react-bootstrap/ProgressBar';
import {FontAwesomeIcon}  from '@fortawesome/react-fontawesome'
//import { library } from '@fortawesome/fontawesome-svg-core'
import { faCircle, faPlayCircle, faPauseCircle, faCheckCircle, faArrowUp, faExclamationTriangle, faTrash, faClipboard, faTag, faHistory, faCog, faSync, faHome, faInfo, faFilter, faCheckSquare, faSquare, faColumns, faTasks, faRedo, faArrowDown} from '@fortawesome/free-solid-svg-icons'
//import { Route, Switch } from 'react-router-dom'
import { PieChart } from 'react-minimal-pie-chart';

function moveItem(data, from, to) {
  var f = data.splice(from, 1)[0];
  data.splice(to, 0, f);
}

function GetIconByStatus(status)
{
  switch(status)
    {
      case "" : return <FontAwesomeIcon icon={faCircle} />;
      case "running" : return <FontAwesomeIcon icon={faPlayCircle} />;
      case "pending" : return <FontAwesomeIcon icon={faCircle} />;
      case "failed" : return <FontAwesomeIcon icon={faExclamationTriangle} />;
      case "completed" : return <FontAwesomeIcon icon={faCheckCircle} />;
      default: throw new Error("unknown status" + this.props.status);
    }
}

class App extends Component
{

  constructor(props)
  {
    super(props);

    this.state = 
    {
      page: "home"
    }
  }


  getPageComponent()
  {
    switch(this.state.page)
    {
      case "home" : return <HomePage />;
      case "details" : return <DetailPage />;
      default : throw new Error("unknown page " + this.state.page);
    }
  }

  pageNav(page)
  {
    this.setState({ page: page});
  }

  render() { 
    
    return (
    <div>

    <Navbar variant="dark" expand="lg">
      <Navbar.Brand href="/">GPC-JE</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link onClick={() => { this.pageNav("home") } }> <FontAwesomeIcon icon={faHome} /> Home</Nav.Link>
          <Nav.Link onClick={() => { this.pageNav("details") } }> <FontAwesomeIcon icon={faInfo}/> Job Details</Nav.Link>      
        </Nav>        
      </Navbar.Collapse>
    </Navbar>

      {this.getPageComponent()}

      </div>

  );
  }

}

class DetailPage extends Component
{
  render()
  {
    return <JobsList cols={[ "jobFamily", "friendlyName", "status", "estCompletion", "progress", "jobsBefore" ]} />
  }
}

class JobsList extends Component {
  constructor(props)
  {
    super(props);
    this.state = {};
    this.state.cols = props.cols;    
    this.state.modal = false;
    this.state.columnsModal = false;
    this.state.possibleCols = 
    [
      "friendlyName", 
      "jobFamily",
      "_id", 
      "status",
      "priority", 
      "estCompletion", 
      "progress", 
      "jobsBefore",
      "startTime", 
      "endTime", 
      "payload",
      "result", 
      "error",
      "workerid",
      "lastLog"
    ];
    this.addCol = this.addCol.bind(this);
    this.loadCounts = this.loadCounts.bind(this);
    this.state.counts = {};
    this.state.families = [];
    this.state.filters = {
      family: "",
      status: "",
      limit: 1000
    }

    this.refreshInterval = 0;
    this.state.refreshIntervalSec = 0;
    this.state.refreshIntervalRef = null;

    axios.get("/jobs/families").then(res => {
      this.setState({families : res.data});
    })

   this.loadCounts();
  }
 
  loadCounts = () =>
  {    
    axios.get("/jobs/count", 
    {
      params: this.state.filters
    }).then(res => 
      {        
        this.setState({counts : res.data});
      })
  }

  addCol = (col) =>
  {
    this.setState({ cols: this.state.cols.concat(col)});
    this.forceUpdate();
  }

  delCol = (col) =>
  {
    var newCols = this.state.cols.filter(c => c !== col);
    this.setState({ cols: newCols});
  }

  moveCol = (col, dir) => 
  {
    var copyArr = cloneDeep(this.state.possibleCols);
    var ix = copyArr.indexOf(col);
    var newIx = ix + dir; //dir is 1 or -1

    if (newIx < 0 || newIx === (copyArr.length))
      throw new Error("newIx of " + newIx + " is invalid for moving in this direction");

    moveItem(copyArr, ix, newIx);
    this.setState({possibleCols: copyArr});
  }

  toggleCol = (col, event) =>
  {
    if (this.state.cols.includes(col))
      this.delCol(col);
    else
      this.addCol(col);
  }

  setFilterFamily = (family) =>
  {
    this.setState(prevState => ({
      filters: {
        ...prevState.filters,
        family: family
      }
    }), this.loadCounts);
  }

  filterFamilyClick = (eventKey, event) =>
  {
    this.setFilterFamily(eventKey);
  }

  limitChange = (event ) =>
  {
    let v = event.target.value;
    this.setState(prevState => ({
      filters: {
        ...prevState.filters,
        limit: v
      }
    }));
  }

  stateFilterClick = (status) => {
    this.setState(prevState => (
      {
        filters: {
          ...prevState.filters,
          status: status
        }
      }
    ))
  }

  clearRefreshInterval = () =>
  {
    if (this.state.refreshIntervalRef !== null)
      {
        clearInterval(this.state.refreshIntervalRef);
      }
  }

  //eventKey is the number of seconds
  changeRefreshRate = (eventKey, event) =>
  {
    if (this.state.refreshIntervalRef !== null)
      {
        clearInterval(this.state.refreshIntervalRef);                
      }
    this.setState({ refreshInterval: eventKey * 1000, refreshIntervalRef : eventKey > 0 ? setInterval(this.loadCounts.bind(this), eventKey * 1000) : null });    
    this.setState({ refreshIntervalSec: eventKey});    
  }

  componentWillUnmount = () =>
  {
    this.clearRefreshInterval();
  }

  closeColumnsModal = () =>{
    this.setState({columnsModal: false});
  }

  actionSelect = (eventKey, event) =>
  {
    switch(eventKey)
    {
      case "delete": {

        if(window.confirm("Are you sure you wish to delete jobs?"))
        {
          axios.delete("/jobs?family=" + this.state.filters.family + "&status=" + this.state.filters.status).then(res =>
            {
              this.loadCounts();
            });
        }
        break;
      }
      case "resubmit":
      {
        if (window.confirm("Are you sure you wish to resubmit multiple jobs?"))
        {
            axios.post("/jobs/resubmit?family=" + this.state.filters.family + "&status=" + this.state.filters.status).then(res => {
              this.loadCounts();
            });
        }
        break
      }
      default: throw new Error("unknown eventKey" + eventKey);
    }
  }

  closeModal = () =>
  {
    this.setState({modal: false});
  }

  render()
  {
    const familyButtons = this.state.families.filter(f => f._id !== "").map(f => {
      return (<Button variant="secondary" key={f._id} onClick={() => { this.filterFamilyClick(f._id)}}>{f._id}</Button> )
  });

    const refreshItems = [0, 1, 5, 15, 30, 60, 120].map(c =>
    {
        return <Dropdown.Item key={c} eventKey={c}> {c} Secs</Dropdown.Item>
    });

    return (
      <Container>
        <Button 
           title="Customize the fields shown" 
           onClick={() => {this.setState({columnsModal: true})}} 
           variant="secondary" 
           size="sm">
             <FontAwesomeIcon icon={faColumns}/> Columns
           </Button>
        <ColumnsModal show={this.state.columnsModal} 
          handleClose={this.closeColumnsModal} 
          cols={this.state.cols} 
          possibleCols={this.state.possibleCols}
          toggleCol={this.toggleCol}
          moveCol={this.moveCol}
          />
       <span>&nbsp;</span>

        <Dropdown className='ilb' onSelect={this.changeRefreshRate} >
          <Dropdown.Toggle size="sm" variant="secondary" id="dropdown-basic">
            <FontAwesomeIcon icon={faSync}/> ({this.state.refreshIntervalSec} sec)
          </Dropdown.Toggle>
          <Dropdown.Menu>
            {refreshItems}
          </Dropdown.Menu>
        </Dropdown>

        <span>&nbsp;</span>

      <div className='ilb' >
        <Button size='sm' variant='secondary' onClick={() => { this.setState({modal: true})}}>
        <FontAwesomeIcon icon={faFilter} title='Active Filters' />( 
          <span>{ GetIconByStatus(this.state.filters.status) }  { (this.state.filters.status === "" ? "All" : this.state.filters.status)}</span> &nbsp;
          <span><FontAwesomeIcon icon={faTag} /> {this.state.filters.family === "" ? "All" : this.state.filters.family}</span>
          )
      </Button>

      <Modal show={this.state.modal} size="lg" onHide={this.closeModal}>
        <Modal.Header closeButton>
          <Modal.Title>
            Filters</Modal.Title>
          </Modal.Header>
        <Modal.Body>
          <Card>
            <Card.Header>
              Status (current: {this.state.filters.status === "" ? "All" : this.state.filters.status})
            </Card.Header>
            <Card.Body>
              <Button variant="primary" size="sm" onClick={() => { this.stateFilterClick(""); } }>
                All ({this.state.counts.total})
              </Button>
              <Button variant="secondary"  size="sm"  onClick={() => { this.stateFilterClick("pending"); } } >
                Pending ({this.state.counts.pending})
              </Button>
              <Button variant="info"  size="sm" onClick={() => { this.stateFilterClick("running"); } }>
                Running ({this.state.counts.running})
              </Button>
              <Button variant="success"  size="sm" onClick={() => { this.stateFilterClick("completed"); } }>
                Completed ({this.state.counts.completed})
              </Button>
              <Button variant="danger"  size="sm" onClick={() => { this.stateFilterClick("failed"); } } >
                Failed ({this.state.counts.failed})
              </Button>
            </Card.Body>
            
          </Card>

          <Card>
            <Card.Header>Family (curent: {this.state.filters.family === "" ? "ALL" : this.state.filters.family})</Card.Header>
            <Card.Body>
            <Button variant="secondary" onClick={() => { this.filterFamilyClick("")}}>All Families</Button>
            {familyButtons}
            </Card.Body>
          </Card>

          <Card>
              <Card.Header>Limit</Card.Header>
              <Card.Body>
                  <InputGroup className=" ilb" size="sm">
                  <InputGroup.Prepend>
                    <InputGroup.Text id="limitLabel">Limit</InputGroup.Text>
                  </InputGroup.Prepend> 
                  <FormControl
                    aria-describedby="limitlabel"
                    type="number"
                    width="5"
                    className='limitInput'
                    defaultValue={this.state.filters.limit}
                    onChange={this.limitChange.bind(this)}
                  />
                </InputGroup>
              </Card.Body>
          </Card>
      
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.closeModal}>Close</Button>
        </Modal.Footer>
      </Modal>

      </div>
      
      <span>&nbsp;</span>

      <div className='ilb'>
        <Dropdown title="Available actions for the current view" className='ilb' size="sm" onSelect={this.actionSelect}>
          <Dropdown.Toggle variant="secondary" id="dropdown-basic" size="sm">
            <FontAwesomeIcon icon={faTasks}/> Actions
          </Dropdown.Toggle>
          <Dropdown.Menu>
            <Dropdown.Item eventKey='delete'> <FontAwesomeIcon icon={faTrash} /> Delete</Dropdown.Item>
            <Dropdown.Item eventKey='resubmit' 
              disabled={ (!(
                this.state.filters.status === "completed" ||
                this.state.filters.status === "failed")) ||
                (this.state.filters.family === "")
                } > <FontAwesomeIcon icon={faRedo} /> Resubmit</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </div>
        
    <JobsListTable cols={this.state.cols} possibleCols={this.state.possibleCols}
      delCol={this.delCol} 
      filters={this.state.filters} 
      refreshInterval={this.state.refreshInterval} 
      loadCounts={this.loadCounts}/>
    </Container>
    );
  }
}

class ColumnsModal extends Component {
  constructor(props){
    super(props);
    this.state = {};
  }

  render()
  {
    const colItems = this.props.possibleCols.map( (c, index) =>
    {
       var icon = this.props.cols.includes(c) ? faCheckSquare : faSquare;
       return <Row  key={index}>  
            <Col >
              <Button title='Toggle visibility of field' onClick={() => {this.props.toggleCol(c)}} ><FontAwesomeIcon icon={icon} ></FontAwesomeIcon> </Button>
          
            <Button 
              title='Move field up' 
              disabled={index <= 0}> <FontAwesomeIcon icon={faArrowUp} onClick={() => { this.props.moveCol(c, -1)}} /> </Button> 
            <Button 
              title='Move field down' 
              disabled={index >= this.props.possibleCols.length-1}>  <FontAwesomeIcon icon={faArrowDown} onClick={() => { this.props.moveCol(c, 1)}}/> </Button>
          &nbsp; {c}</Col>
          
          </Row>
    });
    return (
    <Modal show={this.props.show} onHide={this.props.handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Columns</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {colItems}
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={this.props.handleClose} >Close</Button>
      </Modal.Footer>
    </Modal>
    );
  }
}

class JobsListTable extends Component {
  constructor(props)
  {
    super(props);
    this.state = { 
      jobs: []
    }

    this.state.filters = props.filters;
    this.state.refreshIntervalRef = null;
    this.loadJobs = this.loadJobs.bind(this);

    if (this.props.cols == null)
     throw new Error("cols is a required property");

    this.loadJobs();

  }

  loadJobs()
  {
    axios.get("/jobs", {
      params: this.props.filters
    }).then(res => {
      this.setState({jobs: res.data});
    });
  }

  componentWillUnmount()
  {
    if(this.state.refreshIntervalRef !== null)
      clearInterval(this.state.refreshIntervalRef)
  }
 
  componentDidUpdate(prev)
  {
    if (prev !== this.props)    
    {    
      if (this.refreshIntervalRef !== null) 
      {
        clearInterval(this.state.refreshIntervalRef);
        this.setState ({ refreshIntervalRef: null});
      }

      if (this.props.refreshInterval > 0)
      {
        var refreshIntervalRef = setInterval(this.loadJobs.bind(this), this.props.refreshInterval);
        this.setState({ refreshIntervalRef: refreshIntervalRef});
      }
              
      this.loadJobs();
    }

  }

  colNameToHeaderString(c)
  {
    switch(c)
    {
      case "friendlyName" : return "Name";
      case "jobFamily" : return "Family";
      case "status" : return "Status";
      case "estCompletion" : return "Est. Completion";
      case "startTime" : return "Start Time";
      case "endTime" : return "End Time";
      case "workerid" : return "Worker";
      case "priority" : return "Priority";
      case "error" : return "Error";
      case "payload" : return "Payload";
      case "progress" : return "Progress";
      case "result" : return "Result";
      case "jobsBefore" : return "Jobs Before";
      default: return c;
    }
  }

  render()
  {
    var jobItems = this.state.jobs.map((j) =>   
    {    
      return <Job cols={this.props.cols} possibleCols={this.props.possibleCols} key={j._id} data={j} loadJobs={this.loadJobs} loadCounts={this.props.loadCounts}/>
    });

    var colItems = this.props.possibleCols.map((c, index) =>
      {
        return (this.props.cols.includes(c))  ? <th key={index} >{this.colNameToHeaderString(c)}</th> : null
      });


    return (        
        <div className="container">
          <Table className="jobsListTable" size="sm" responsive hover striped > 
          <thead>
            <tr>
              <th>&nbsp;</th>
              {colItems}
            </tr>
          </thead>  
          <tbody>          
          {jobItems}
          </tbody>
          </Table>
        </div>
    );
  }
}

////////

class Job extends Component {
  constructor(props)
  {

    super(props);    

    if (this.props.cols == null)
      throw new Error("cols is a required property");

    this.state = {
      modal: false
    };
  }


  
  getColumnDef(field,index)
  {
    switch(field)
    {
      case "_id": return (<td key={index} >{this.props.data._id} </td>);
      case "friendlyName": return (<td key={index} >{this.props.data.friendlyName} </td>);
      case "jobFamily" : return (<td key={index} >{this.props.data.jobFamily} </td>);
      case "status" : return (<td key={index} ><JobStatusCol status={this.props.data.status} /></td>);
      case "estCompletion" : return (<td key={index}>{this.calculateDuration()}</td>);
      case "workerid" : return (<td key={index} >{this.props.data.workerid} </td>);
      case "result" : return (<td key={index} >{JSON.stringify(this.props.data.result, null, "\t")} </td>);
      case "startTime" : return (<td key={index}>{this.props.data.startTime}</td>)
      case "endTime" : return (<td key={index}>{this.props.data.endTime}</td>)
      case "priority": return <td key={index}>{this.props.data.priority}</td>      
      case "error" : return <td key={index}>{this.props.data.error}</td>      
      case "payload" : return <td key={index}>{JSON.stringify(this.props.data.payload, null, "\t")}</td>      
      case "progress" : return <td key={index}> <ProgressBar variant='info' label={`${Math.floor(100 * this.props.data.progress)}%`} now={ Math.floor(100 * this.props.data.progress) } /> </td>      
      case "lastLog" : return <td key={index}>{this.props.data.lastLog}</td>
      case "jobsBefore" : return <td key={index}>{this.props.data.jobsBefore}</td>
      default: throw new Error("unknown field " + field);
    }
  }

  calculateDuration()
  {
    var row = this.props.data;
    if (row.status === "running" && typeof(row.progress) !== "undefined" && row.progress > 0.0)
    {                              
      // solve for total time (x):  x = t / p 
      // where t is time so far
      // where p is percent completed (e.g. 0.34)
      var startTime = moment.utc( row.startTime);
      var progress = row.progress;                 
      var runTime = moment.utc().diff(startTime);       
//      var runTimeDuration = moment.duration(runTime);                       
      var total = runTime / progress;
      var remain = total - runTime;
  //    var totalDuration = moment.duration(total);
      var remainDuration = moment.duration(remain);                              
      return remainDuration.humanize();
    }
    else
    {
      return "N/A";
    }
}

  rePrioritize = (newPri) =>
  {
    var fd = new FormData();
    fd.set("priority", newPri);
    axios.put("/jobs/" + this.props.data._id,fd).then(this.closeModal).then(res =>
      {
        this.signalReload();
      });
  }

  delete = () =>
  {
      axios.delete("/jobs/" + this.props.data._id).then(this.closeModal).then( res => { this.signalReload() });
  }

  signalReload() 
  {
    this.props.loadCounts();
    this.props.loadJobs();
  }

  showModal = () => 
  {   
    this.setState({ modal: true});
  }

  closeModal = () =>
  {
    this.setState({ modal: false});
  }

  fail = () =>
  {
    var fd = new FormData();
    fd.set("error", "Marked failed from web UI");
    axios.put("/jobs/" + this.props.data._id,fd).then(this.closeModal).then(res =>
      {
        this.signalReload();
      });
  }

  resubmit = () =>
  {
 

    var fd = new FormData();
    fd.set("payload", JSON.stringify(this.props.data.payload));
    fd.set("friendlyName", this.props.data.friendlyName);
    fd.set("jobFamily", this.props.data.jobFamily);
    axios.post("/jobs", fd).then(this.closeModal).then(res =>
      {
        this.signalReload();
      });

  }

  render()
  {

    var columns = this.props.possibleCols.map((col, index) =>
    {
      return (this.props.cols.includes(col)) ? this.getColumnDef(col,index) : null;
    });

    return (
      
    <tr>
      <td>
        <FontAwesomeIcon icon={faCog} className='clickable' onClick={this.showModal} />    
        { 
          (this.state.modal) ? <JobDetailModal key={this.props.data._id} 
            show={this.state.modal} 
            data={this.props.data} 
            handleClose={this.closeModal} 
            delete={this.delete} 
            rePrioritize={this.rePrioritize}
            fail={this.fail}
            resubmit={this.resubmit}
            /> : ""
        }
      </td>
      {columns} 
      </tr>);
  }
}

class JobStatusCol extends Component {
  render()
  {
    return  GetIconByStatus(this.props.status);
  }
}

/////////////

class HomePage extends Component {
  render() 
  {
    return (
      <div>
        <div><FamilyList /></div>
        <div><WorkerList /></div>
        <FamilyDetailModal />
      </div>
    );  
  }
}

class Family extends Component 
{
  constructor(props)
  {
    super(props);        
    this.state = {};
    this.state.family = cloneDeep(props.family);
    this.state.modal = false;
    this.state.isLoading = true;
    this.state.counts = null;
    this.showModal = this.showModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.updateState = this.updatePaused.bind(this);
    this.togglePause = this.togglePause.bind(this);    
    this.state.icon = this.getIcon(this.state.family.pause);

    axios.get("/jobs/count?family=" + this.props.family._id)
    .then(c => {      
      this.setState({ isLoading: false, counts: c.data});      
    })
  }


  closeModal = () => 
  {
    this.setState({ modal: false });
  }

  showModal = () => 
  {
    this.setState({ modal: true});
  }


  getIcon(pause)
  {
    return pause ? (faPauseCircle) : (faPlayCircle);
  }

  updatePaused = (pause) => {
    this.setState( (prev) => {  
      var newState = cloneDeep(prev);
      newState.family.pause = pause;
      newState.icon = this.getIcon(newState.family.pause);
      newState.modal = false;
      return newState
    });    
  }

  togglePause = () =>
  {
    var fd = new FormData();
    fd.set("pause", !this.state.family.pause);
    axios.put('/jobs/families/' + this.state.family._id,fd)
    .then(res =>
      {
        this.updatePaused(!this.state.family.pause);
      })
  }

  handleDelete = () =>
  {
    if( window.confirm("Are you sure you want to delete the family: " + this.state.family._id + "? This cannot be undone"))
    {
      axios.delete('/jobs/families/' + this.state.family._id)
      .then(res => 
        {
          this.closeModal();
          this.props.refresh();
        })
    }
  }

  
  render()
  {      

    if (this.state.isLoading)
    {  
      return ""
    }
    else
    {
      return (
          
        <Container>
        <Card className='clickable' onClick={this.showModal}> 
        <Card.Header><FontAwesomeIcon icon={this.state.icon} />  {this.state.family._id}</Card.Header>
        <Card.Body>

          <Row >
            <Col style={{ maxWidth: "150px"}}>
              <PieChart data={
                [
                  { title : "pending", value: this.state.counts.pending, color: "#7A8288" },
                  { title : "running", value: this.state.counts.running, color: "#5bc0de" },
                  { title : "completed", value: this.state.counts.completed, color: "#62c462" }, 
                  { title : "failed", value: this.state.counts.failed, color: "#ee5f5b"}
                ]}
              />
            </Col>
            <Col  >          
              <Row className="">
                <Col xs={2} className=""> <FontAwesomeIcon icon={faCircle} />  Total</Col>            
                <Col xs={2} title='Total'>{this.state.counts.total} </Col>
              </Row>
                
              <Row className="text-secondary">
                <Col xs={2} > <FontAwesomeIcon icon={faCircle} mask={['far', 'circle']} /> Pending</Col>
                <Col xs={2} > {this.state.counts.pending} </Col>                  
              </Row>
              
              <Row className="text-info">
                <Col xs={2} > <FontAwesomeIcon icon={faPlayCircle} mask='far'/> Running</Col>
                <Col xs={2} > {this.state.counts.running} </Col>                
              </Row>
              
              <Row className="text-success">
                <Col  xs={2} > <FontAwesomeIcon icon={faCheckCircle} mask='far' /> Completed</Col>
                <Col  xs={2} > {this.state.counts.completed}</Col>                
              </Row>
              
              <Row className="text-danger">
                <Col  xs={2} > <FontAwesomeIcon icon={faExclamationTriangle} /> Failed</Col>
                <Col  xs={2} >{this.state.counts.failed}</Col>             
              </Row>
            
            </Col>

          </Row>

          </Card.Body>
        </Card>

        
        {
          this.state.modal ? ( 
            <FamilyDetailModal show={this.state.modal} data={this.state.family} handleClose={this.closeModal} togglePause={this.togglePause} handleDelete={this.handleDelete} /> 
            ) 
            : ""
          }
      
      </Container>
        
      );
    }
  }
}

class FamilyList extends Component
{
  constructor(props) 
  {
    super(props);
    
    this.state = { 
      families: [],      
      counts: {}
     }
     this.refresh();
  }

  refresh = () => {
    axios.get("/jobs/families")
     .then(res => {                
       this.setState({ families: res.data});            
     });
  }

  render() 
  {    
    const familyItems = this.state.families.map((f) =>   
    {    
      return <Family onClick={ () => this.onClick(f) } key={f._id} family={f} refresh={this.refresh} /> 
    }
    );

    
    return (
    <div className="container">
      <Card >      
      <Card.Header>Job Families</Card.Header>  
        <Card.Body>          
          {familyItems}
        </Card.Body>
      </Card>
    </div>
    );
  }
}

//////////

class Worker extends Component {
  
  constructor(props)
  {
    super(props);
    this.state = {
      modal: false
    };
  }

  showModal = ()   => {
    this.setState({modal: true});
  }

  closeModal = () => {
    this.setState( { modal: false});
  }

  deleteWorker = () => {
    if(window.confirm("Are you sure you wish to delete this worker?"))
    {
        axios.delete("/jobs/workers/" + this.props.worker._id).then(res =>{
          this.props.signalReload();
          this.setState({modal: false});
        });
    };
  }

  render()
  {
    var age = moment().utc().diff(  moment.utc( this.props.worker.updatedTime ));
    var ageDuration = moment.duration(age);

    return (
      <>
      { this.state.modal ?      <Modal show={this.state.modal} onHide={this.closeModal} >
            <Modal.Header closeButton>
              <Modal.Title>Worker: {this.props.worker.name} - {this.props.worker.jobFamily} </Modal.Title>
            </Modal.Header>
            <Modal.Footer>
            <Button onClick={this.closeModal}>Close</Button>
            <Button variant="danger" onClick={this.deleteWorker}>Delete</Button>
            </Modal.Footer>
        </Modal>  : "" }
      <Row className='clickable' onClick={this.showModal}>
        <Col className=''> <FontAwesomeIcon icon={faCog} /> {this.props.worker.name}</Col>
        <Col className='col-sm'><FontAwesomeIcon icon={faTag} /> {this.props.worker.jobFamily}</Col>
        <Col className='col-sm'> <FontAwesomeIcon icon={faClipboard} />  {this.props.worker.status}</Col>
        <Col className='col-sm'><FontAwesomeIcon icon={faHistory} /> {ageDuration.humanize()}</Col>
      </Row>
      </>
    )
  }
}

class WorkerList extends Component {

    constructor(props){
      super(props);
      this.state = {
        workers: []
      }

      this.getWorkers();
    }

    getWorkers = () =>
    {
      axios.get("/jobs/workers")
      .then(res => {                
        this.setState({ workers: res.data});            
      });
    }

    render()
    {
      const workerItems = this.state.workers.map((w) =>   
      {       
        return <Worker  key={w._id} worker={w} signalReload={this.getWorkers}/>
      }
      );

      return (
    
        <div className="container">
        <Card >      
          <Card.Header>Workers</Card.Header>  
          <Card.Body>            
            {workerItems}
          </Card.Body>
        </Card>
      </div>
        );
    }

}

class FamilyDetailModal extends Component {
  constructor(props)
  {
    super(props);
    this.state = { modal: this.props.show};
  }

  render()
  {
    if (this.props.data != null)
  return (
  <Modal show={this.props.show} onHide={this.props.handleClose}>
    <Modal.Header closeButton >
      <Modal.Title>Job Family Detail: {this.props.show ? this.props.data._id : ""}</Modal.Title>
    </Modal.Header>
    <Modal.Body>
      <pre>{ JSON.stringify(this.props.data, undefined, 2)}</pre>
    </Modal.Body>
    <Modal.Footer>
      <Button variant="primary" onClick={this.props.handleClose}>Close</Button>            
      <Button variant="primary" onClick={this.props.togglePause}>{this.props.data.pause ? "Resume" : "Pause"}</Button>      
      <Button variant="danger" onClick={this.props.handleDelete}>Delete</Button>
    </Modal.Footer>
  </Modal>
  );
  else
  return "";
  }


}


class JobDetailModal extends Component
{
  constructor(props)
  {
    super(props);

    this.state = {};
  }

  tryReprioritize()
  {    
    var newPri = prompt("New Priority", this.props.data.priority);
    if (newPri === null) return //cancel button clicked
    newPri = parseInt(newPri);
    if (newPri !== null && newPri !== this.props.data.priority)    
      this.props.rePrioritize(newPri);
    /*else
      console.log("newPri not changed, doing nothing");*/
  }

  tryDelete = () => {  
    if( window.confirm("Really delete this job?"))    
    {
        this.props.delete();
    }
  }

  render()
  {
    if (this.props.data != null)
    {
      return (
      <Modal show={this.props.show} size="lg" onHide={this.props.handleClose} >
        <Modal.Header closeButton>
          <Modal.Title>Job Detail: { this.props.data.friendlyName !== "" ? this.props.data.friendlyName : this.props.data._id }</Modal.Title>
        </Modal.Header>
        <Modal.Body>

          <Form>
            <Form.Group as={Row} controlId='idLabel'>
              <Form.Label column sm="2" > _id</Form.Label>
              <Col>
                <Form.Control plaintext readOnly defaultValue={this.props.data._id} />
              </Col>
            </Form.Group>
          </Form>
          
        <pre>{ JSON.stringify(this.props.data, undefined, 2)}</pre>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={this.props.handleClose}>Close</Button> 
          { this.props.data.status === "pending" ?  <Button variant="secondary" onClick={this.tryReprioritize.bind(this)} >Re-Prioritize</Button> : "" }
          { this.props.data.status === "failed" || this.props.data.status === "completed" ? <Button variant="secondary" onClick={this.props.resubmit} >Resubmit</Button> : "" }
          { this.props.data.status === "running" ? <Button variant="secondary" onClick={this.props.fail} >Fail</Button> : "" }
          <Button variant="danger" onClick={this.tryDelete} >Delete</Button>
        </Modal.Footer>
      </Modal>
      );
    }
    else
    {
      return "";
    }
  }
}

export default App;